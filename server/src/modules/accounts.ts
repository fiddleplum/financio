import { JsonHelper, JsonObject, JsonType, MathHelper } from '@orourley/pine-lib';
import { ItemsModule } from './items';
import { Account, AccountFile, AccountGroup, Items, Role, YMD } from '@orourley/financio-types';
import { FinancioServer } from '../financio_server';

export class AccountsModule extends ItemsModule<Account, AccountGroup> {

	constructor(server: FinancioServer) {
		super('account', 'accounts', true, server);
	}

	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return Role.DashBoard;
		}
		else if (type === 'get') {
			return Role.DashBoard;
		}
		else if (type === 'set') {
			return Role.Accountant;
		}
		return Role.Owner;
	}

	async setItemProperties(account: Account, newItem: boolean, params: JsonObject): Promise<void> {

		// Get the projectId.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the unit.
		const unit = params['unit'];
		if (!JsonHelper.isString(unit)) {
			throw new Error(`params.unit must be a string.`);
		}

		// Get the internal flag.
		const internal = params['internal'];
		if (!JsonHelper.isBoolean(internal)) {
			throw new Error(`params.internal must be a boolean.`);
		}

		// Get the anchor month.
		const anchorMonth = params['anchorMonth'];
		if (!JsonHelper.isString(anchorMonth) || !anchorMonth.match(/^\d{4}-\d{2}$/u)) {
			throw new Error(`params.anchorMonth must be a string matching YYYY-MM.`);
		}

		// Get the anchor value.
		const anchorValue = params['anchorValue'];
		if (!JsonHelper.isNumber(anchorValue)) {
			throw new Error(`params.anchorValue must be a number.`);
		}

		account.unitId = unit;
		account.internal = internal;

		// If this is a new account, set the anchor point and a value entry.
		if (newItem) {
			account.valuesAtMonthStart = [];
			account.anchorPoint = { month: anchorMonth, value: anchorValue };
		}
		else {

			// If the anchor month has changed, we need to update the values at month start.
			if (anchorMonth !== account.anchorPoint.month) {
				const offset = account.anchorPoint.value - Account.getValueAtMonthStart(anchorMonth, account);
				for (let i = 0, l = account.valuesAtMonthStart.length; i < l; i++) {
					const valueAtIndex = account.valuesAtMonthStart[i]!;
					valueAtIndex.value = MathHelper.round(valueAtIndex.value + offset, 10);
				}
				// We also need to switch some of the months if they've changed 'sides' of the anchor point.
				for (let i = 0, l = account.valuesAtMonthStart.length; i < l; i++) {
					const valueAtIndex = account.valuesAtMonthStart[i]!;
					if (valueAtIndex.month >= anchorMonth && valueAtIndex.month < account.anchorPoint.month) {
						const date = new YMD(valueAtIndex.month);
						date.month += 1;
						valueAtIndex.month = date.toYearMonthString();
						if (i < account.valuesAtMonthStart.length - 1) {
							valueAtIndex.value = account.valuesAtMonthStart[i + 1]!.value;
						}
						else {
							valueAtIndex.value = account.anchorPoint.value + offset;
						}
					}
				}
				for (let i = account.valuesAtMonthStart.length - 1; i >= 0; i--) {
					const valueAtIndex = account.valuesAtMonthStart[i]!;
					if (valueAtIndex.month < anchorMonth && valueAtIndex.month >= account.anchorPoint.month) {
						const date = new YMD(valueAtIndex.month);
						date.month -= 1;
						valueAtIndex.month = date.toYearMonthString();
						if (i > 0) {
							valueAtIndex.value = account.valuesAtMonthStart[i - 1]!.value;
						}
						else {
							valueAtIndex.value = account.anchorPoint.value + offset;
						}
					}
				}

				// Set the new anchor month.
				account.anchorPoint.month = anchorMonth;
			}

			// If the anchor value has changed, we need to update the values at month start.
			const offset = anchorValue - account.anchorPoint.value;
			if (offset !== 0) {
				for (let i = 0, l = account.valuesAtMonthStart.length; i < l; i++) {
					account.valuesAtMonthStart[i]!.value = MathHelper.round(account.valuesAtMonthStart[i]!.value + offset, 10);
				}
				account.anchorPoint.value = anchorValue;
			}
		}
	}

	onMessageReceivedExt(command: string, username: string, params: JsonObject): Promise<JsonType | void> | undefined {

		if (command === 'setOfxImportDefaults') {
			return this.setOfxImportDefaults(username, params);
		}
		else if (command === 'setCsvImportDefaults') {
			return this.setCsvImportDefaults(username, params);
		}

		return undefined;
	}

	/** Sets the defaults on OFX imports. */
	async setOfxImportDefaults(username: string, params: JsonObject): Promise<void> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the account id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the reverseDirection flag.
		const reverseDirection = params['reverseDirection'];
		if (!JsonHelper.isBoolean(reverseDirection)) {
			throw new Error(`params.reverseDirection must be a boolean.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the accounts file.
		await this.server.dataStore.modifyJson<AccountFile>(`projects/${projectId}/accounts.json`, async (accountsFile) => {

			// Check that it is valid.
			if (!Items.isFile(accountsFile)) {
				throw new Error(`Account with id ${id} is not found.`);
			}

			// Get the account and verify that it is valid.
			const account = accountsFile.list[id];
			if (!account || Items.isGroup(account)) {
				throw new Error(`Account with id ${id} is not found.`);
			}

			// Set the new defaults.
			account.ofxImportDefaults = {
				reverseDirection
			};

			return accountsFile;
		});
	}

	/** Sets the defaults on CSV imports. */
	async setCsvImportDefaults(username: string, params: JsonObject): Promise<void> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the account id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the delim.
		const delim = params['delim'];
		if (!JsonHelper.isString(delim)) {
			throw new Error(`params.delim must be a string.`);
		}

		// Get the first lines trimming.
		const trimFirstLines = params['trimFirstLines'];
		if (!JsonHelper.isNumber(trimFirstLines)) {
			throw new Error(`params.trimFirstLines must be a number.`);
		}

		// Get the last lines trimming.
		const trimLastLines = params['trimLastLines'];
		if (!JsonHelper.isNumber(trimLastLines)) {
			throw new Error(`params.trimLastLines must be a number.`);
		}

		// Get the headers flag.
		const hasHeaders = params['hasHeaders'];
		if (!JsonHelper.isBoolean(hasHeaders)) {
			throw new Error(`params.hasHeaders must be a boolean.`);
		}

		// Get the date format.
		const dateFormat = params['dateFormat'];
		if (!JsonHelper.isString(dateFormat)) {
			throw new Error(`params.dateFormat must be a string.`);
		}

		// Get the CSV header import defaults.
		const headersToCols = params['headersToCols'];
		if (!JsonHelper.isObjectOfNumbers(headersToCols)) {
			throw new Error(`params.headersToCols must be an object of strings to numbers.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the accounts file.
		await this.server.dataStore.modifyJson<AccountFile>(`projects/${projectId}/accounts.json`, async (accountsFile) => {

			// Check that it is valid.
			if (!Items.isFile(accountsFile)) {
				throw new Error(`Account with id ${id} is not found.`);
			}

			// Get the account and verify that it is valid.
			const account = accountsFile.list[id];
			if (!account || Items.isGroup(account)) {
				throw new Error(`Account with id ${id} is not found.`);
			}

			// Set the new defaults.
			account.csvImportDefaults = {
				delim,
				trimFirstLines,
				trimLastLines,
				hasHeaders,
				dateFormat,
				headersToCols
			};

			return accountsFile;
		});
	}
}
