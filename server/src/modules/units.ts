import { JsonHelper, JsonObject, JsonType, Sort } from '@orourley/pine-lib';
import { Unit, ConversionRates, Role, RelativeDate, YMD, UnitGroup, UnitFile, Items } from '@orourley/financio-types';
import { FinancioServer } from '../financio_server';
import { ItemsModule } from './items';

export class UnitsModule extends ItemsModule<Unit, UnitGroup> {

	constructor(server: FinancioServer) {
		super('unit', 'units', true, server);
	}

	/** Gets the permission given a request type. */
	getMinimumPermission(type: 'list' | 'get' | 'set'): Role | undefined {
		if (type === 'list') {
			return Role.Viewer;
		}
		else if (type === 'get') {
			return Role.Viewer;
		}
		else if (type === 'set') {
			return Role.Accountant;
		}
		return Role.Owner;
	}

	/** Sets the attributes of a particular item. */
	async setItemProperties(unit: Unit, _newItem: boolean, params: JsonObject): Promise<void> {

		// Get the decimals to be displayed.
		const decimals = params['decimals'];
		if (!JsonHelper.isNumber(decimals) && decimals !== undefined) {
			throw new Error(`params.decimals must be a number or undefined.`);
		}

		// Set the decimals.
		if (decimals !== undefined) {
			unit.decimals = decimals;
		}

		// If there is no conversion rates object, add it.
		if (unit.conversionRates === undefined) {
			unit.conversionRates = {};
		}
	}

	/** Runs any additional commands. Returns undefined if no command was processed. */
	onMessageReceivedExt(command: string, username: string, params: JsonObject): Promise<JsonType | void> | undefined {
		if (command === 'listConversionRates') {
			return this.listConversionRates(username, params);
		}
		else if (command === 'getConversionRate') {
			return this.getConversionRate(username, params);
		}
		else if (command === 'setConversionRate') {
			return this.setConversionRate(username, params);
		}
		else if (command === 'deleteConversionRate') {
			return this.deleteConversionRate(username, params);
		}
		return undefined;
	}

	/** Lists the conversion rates.
	 *  The rates will be book-ended by the interpolated rates at the start and end times, if present. */
	async listConversionRates(username: string, params: JsonObject): Promise<ConversionRates> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the unit id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the other unit id.
		const otherId = params['otherId'];
		if (!JsonHelper.isString(otherId) && otherId !== undefined) {
			throw new Error(`params.otherId must be a string or undefined.`);
		}
		if (id === otherId) {
			throw new Error(`params.otherId must be different than params.id.`);
		}

		// Get the start date.
		const startDate = params['startDate'];
		if (!RelativeDate.isValidString(startDate) && startDate !== undefined) {
			throw new Error(`params.startDate must be a RelativeDate string or undefined.`);
		}

		// Get the end date.
		const endDate = params['endDate'];
		if (!RelativeDate.isValidString(endDate) && endDate !== undefined) {
			throw new Error(`params.endDate must be a RelativeDate string or undefined.`);
		}

		// Setup the start and end dates.
		const startDateString = RelativeDate.toYMD(RelativeDate.fromString(startDate ?? ',-1,years,')).toString();
		const endDateString = RelativeDate.toYMD(RelativeDate.fromString(endDate ?? ',,,')).toString();

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Get the unit file.
		const unitFile = await this.server.dataStore.getJson<UnitFile>(`projects/${projectId}/units.json`);
		if (!unitFile) {
			return {};
		}

		// Get the unit.
		const unit = unitFile.list[id];
		if (!unit || Items.isGroup(unit)) {
			return {};
		}

		// Get the list of other units to list.
		const otherUnitIds = otherId !== undefined ? [otherId] : Object.keys(unit.conversionRates);

		// Start a list filtered by the other unit ids and the start and end dates.
		const conversionRatesFiltered: ConversionRates = {};

		// Go through each other unit id.
		for (const otherUnitId of otherUnitIds) {

			// Get conversion rates for the other unit.
			const conversionRates = unit.conversionRates[otherUnitId];
			if (!conversionRates) {
				continue;
			}

			// Gets the index for the start date.
			const indexStart = Sort.getIndex(startDateString, conversionRates, (lhs, rhs) => lhs[0] < rhs);

			// Gets the index for the end date.
			let indexEnd = Sort.getIndex(endDateString, conversionRates, (lhs, rhs) => lhs[0] < rhs);
			if (indexEnd === conversionRates.length) {
				indexEnd -= 1;
			}

			// Add the filtered conversion rates.
			if (indexStart <= indexEnd) {
				conversionRatesFiltered[otherUnitId] = conversionRates.slice(indexStart, indexEnd + 1);
			}
			else {
				conversionRatesFiltered[otherUnitId] = [];
			}

			// Always the rates at the start and end of the range.
			const startRateBookEnd = Unit.getConversionRateAtDate(id, otherUnitId, startDateString, unit.conversionRates);
			if (startRateBookEnd !== undefined) {
				conversionRatesFiltered[otherUnitId].unshift([startDateString, startRateBookEnd]);
			}
			const endRateBookEnd = Unit.getConversionRateAtDate(id, otherUnitId, endDateString, unit.conversionRates);
			if (endRateBookEnd !== undefined) {
				conversionRatesFiltered[otherUnitId].push([endDateString, endRateBookEnd]);
			}
		}

		// Return the conversion rate.
		return conversionRatesFiltered;
	}

	/** Gets a conversion rate. */
	async getConversionRate(username: string, params: JsonObject): Promise<number | undefined> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the unit id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the other unit id.
		const otherId = params['otherId'];
		if (!JsonHelper.isString(otherId)) {
			throw new Error(`params.otherId must be a string.`);
		}
		if (id === otherId) {
			throw new Error(`params.otherId must be different than params.id.`);
		}

		// Get the date.
		const date = params['date'];
		if (!JsonHelper.isString(date) || !YMD.isWellFormatted(date)) {
			throw new Error(`params.date must be a well formatted date string.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Get the units file.
		const unitFile = await this.server.dataStore.getJson<UnitFile>(`projects/${projectId}/units.json`);

		// Get the conversion with the other unit.
		const unit = unitFile?.list[id];
		if (!unit || Items.isGroup(unit)) {
			return undefined;
		}

		const conversionRates = unit.conversionRates[otherId];
		if (!conversionRates) {
			return undefined;
		}

		// Gets the conversion rate for the date.
		const index = Sort.getIndex(date, conversionRates, (lhs, rhs) => lhs[0] < rhs);
		const conversionRate = conversionRates[index];
		if (!conversionRate || conversionRate[0] !== date) {
			return undefined;
		}

		// Return the conversion rate.
		return conversionRate[1];
	}

	/** Sets a conversion rate. */
	async setConversionRate(username: string, params: JsonObject): Promise<void> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the unit id.
		const id = params['id'];
		if (!JsonHelper.isString(id)) {
			throw new Error(`params.id must be a string.`);
		}

		// Get the other unit id.
		const otherId = params['otherId'];
		if (!JsonHelper.isString(otherId)) {
			throw new Error(`params.otherId must be a string.`);
		}
		if (id === otherId) {
			throw new Error(`params.otherId must be different than params.id.`);
		}

		// Get the date.
		const date = params['date'];
		if (!JsonHelper.isString(date) || !YMD.isWellFormatted(date)) {
			throw new Error(`params.date must be a well formatted date string.`);
		}

		// Get the other unit value that equals one of this unit.
		const value = params['value'];
		if (!JsonHelper.isNumber(value)) {
			throw new Error(`params.value must be a number.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the units file.
		await this.server.dataStore.modifyJson<UnitFile>(`projects/${projectId}/units.json`, async (unitFile) => {

			// If there's no unit file yet, error.
			if (!unitFile) {
				throw new Error(`There is no unit file for adding conversion rates.`);
			}

			// Get the unit.
			const unit = unitFile.list[id];
			if (!unit || Items.isGroup(unit)) {
				throw new Error(`The unit with id ${id} is not found.`);
			}

			// Get the other unit.
			const otherUnit = unitFile.list[otherId];
			if (!otherUnit || Items.isGroup(otherUnit)) {
				throw new Error(`The unit with id ${otherId} is not found.`);
			}

			// Add the conversion rate to this unit.
			if (!unit.conversionRates[otherId]) {
				unit.conversionRates[otherId] = [];
			}
			Sort.add([date, value], unit.conversionRates[otherId], (a, b) => a[0] < b[0]);

			// Add the conversion rate to the other unit.
			if (!otherUnit.conversionRates[id]) {
				otherUnit.conversionRates[id] = [];
			}
			Sort.add([date, 1 / value], otherUnit.conversionRates[id], (a, b) => a[0] < b[0]);

			// Return the data.
			return unitFile;
		});
	}

	/** Deletes a conversion rate. */
	async deleteConversionRate(username: string, params: JsonObject): Promise<void> {

		// Get the project id.
		const projectId = params['projectId'];
		if (!JsonHelper.isString(projectId)) {
			throw new Error(`params.projectId must be a string.`);
		}

		// Get the unit id.
		const unitId = params['unitId'];
		if (!JsonHelper.isString(unitId)) {
			throw new Error(`params.unitId must be a string.`);
		}

		// Get the other unit id.
		const otherUnitId = params['otherUnitId'];
		if (!JsonHelper.isString(otherUnitId)) {
			throw new Error(`params.otherUnitId must be a string.`);
		}
		if (unitId === otherUnitId) {
			throw new Error(`params.otherUnitId must be different than params.unitId.`);
		}

		// Get the date.
		const date = params['date'];
		if (!JsonHelper.isString(date) || !YMD.isWellFormatted(date)) {
			throw new Error(`params.date must be a well formatted date string.`);
		}

		// Verify permissions.
		if (!await this.server.hasPermission(username, projectId, Role.Bookkeeper)) {
			throw new Error('You do not have the required role.');
		}

		// Modify the units file.
		await this.server.dataStore.modifyJson<UnitFile>(`projects/${projectId}/units.json`, async (unitFile) => {

			// If there's no unit file yet, set it up.
			if (!unitFile) {
				throw new Error(`There is no unit file for adding conversion rates.`);
			}

			const unit = unitFile.list[unitId];
			if (!unit || Items.isGroup(unit)) {
				throw new Error(`The unit with id ${unitId} is not found.`);
			}

			const otherUnit = unitFile.list[unitId];
			if (!otherUnit || Items.isGroup(otherUnit)) {
				throw new Error(`The unit with id ${otherUnitId} is not found.`);
			}

			// Add the conversion rate to this unit.
			if (!unit.conversionRates[otherUnitId]) {
				throw new Error(`There is no conversion between unit with id ${unitId} and unit with id ${otherUnitId}`);
			}
			if (!Sort.remove(date, unit.conversionRates[otherUnitId], (a, b) => a[0] < b, (a, b) => a[0] === b)) {
				throw new Error(`There is no conversion between unit with id ${unitId} and unit with id ${otherUnitId} at date ${date}`);
			}
			if (unit.conversionRates[otherUnitId].length === 0) {
				delete unit.conversionRates[otherUnitId];
			}

			// Add the conversion rate to the other unit.
			if (!otherUnit.conversionRates[unitId]) {
				throw new Error(`There is no conversion between unit with id ${otherUnitId} and unit with id ${unitId}`);
			}
			if (!Sort.remove(date, otherUnit.conversionRates[unitId], (a, b) => a[0] < b, (a, b) => a[0] === b)) {
				throw new Error(`There is no conversion between unit with id ${otherUnitId} and unit with id ${unitId} at date ${date}`);
			}
			if (otherUnit.conversionRates[unitId].length === 0) {
				delete otherUnit.conversionRates[unitId];
			}

			// Return the data.
			return unitFile;
		});
	}
}
