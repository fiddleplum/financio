import { ConnectionState, FullApp, Router } from '@orourley/elm-app';
import {
	AccountListPage,
	AccountViewPage,
	AdminPage,
	CategoryListPage,
	CategoryViewPage,
	LoginPage,
	ProjectViewPage,
	ProjectListPage,
	ReportListPage,
	ReportViewPage,
	RuleListPage,
	RuleViewPage,
	TransactionsImportPage,
	UnitListPage,
	UnitViewPage,
	UserSettingsPage
} from './internal';

import { componentTypes } from './component-types';

import html from './financio-app.html';
import css from './financio-app.css';

/** The main Financio app. */
export class FinancioApp extends FullApp {

	/** The constructor. */
	constructor() {
		super();

		// Bind functions to this.
		this.toggleNav = this.toggleNav.bind(this);

		// Register the pages.
		this.registerPage('', ProjectViewPage);
		this.registerPage('accountList', AccountListPage);
		this.registerPage('accountView', AccountViewPage);
		this.registerPage('admin', AdminPage);
		this.registerPage('categoryList', CategoryListPage);
		this.registerPage('categoryView', CategoryViewPage);
		this.registerPage('login', LoginPage);
		this.registerPage('projectView', ProjectViewPage);
		this.registerPage('projectList', ProjectListPage);
		this.registerPage('reportView', ReportViewPage);
		this.registerPage('reportList', ReportListPage);
		this.registerPage('ruleList', RuleListPage);
		this.registerPage('ruleView', RuleViewPage);
		this.registerPage('transactionsImport', TransactionsImportPage);
		this.registerPage('unitView', UnitViewPage);
		this.registerPage('unitList', UnitListPage);
		this.registerPage('userSettings', UserSettingsPage);
	}

	/** Processes a query. */
	protected override async processQuery(oldQuery: Router.Query | undefined, newQuery: Router.Query): Promise<void> {

		// Update the project-dependent nav items if there is a project id.
		if (this._projectId !== newQuery['projectId']) {
			this._projectId = newQuery['projectId'];
			this.query('.nav .needsProject', Element).classList.toggle('hidden', this._projectId === undefined);
			const links = this.queryAll('.nav .needsProject a', HTMLAnchorElement);
			links.forEach((a) => {
				a.href = a.href.replaceAll(/&projectId=[^&]*/gu, '');
			});
			if (this._projectId !== undefined) {
				links.forEach((a) => {
					a.href += `&projectId=${this._projectId}`;
				});
			}
		}

		// Call super.
		await super.processQuery(oldQuery, newQuery);
	}

	/** Changes the display to indicate that the user is logged in. */
	onConnectionStateUpdated(prevState: ConnectionState, nextState: ConnectionState): void {
		if (prevState === 'authenticated') {
			this.root.classList.remove('logged-in');
			this.query('.nav .needsAdmin', Element).classList.add('hidden');
		}
		if (nextState === 'connecting') {
			this.showMessage('Connecting...');
		}
		else if (nextState === 'connectingFailed') {
			this.showMessage('Could not connect to the server. Try waiting a bit and refreshing the page.');
		}
		else if (nextState === 'authenticating') {
			this.showMessage('Authenticating...');
		}
		else if (nextState === 'authenticated') {
			this.showMessage('');
			this.root.classList.add('logged-in');
			void this.isAdmin().then((admin) => {
				this.query('.nav .needsAdmin', Element).classList.toggle('hidden', !admin);
			});
		}
		else if (nextState === 'notAuthenticated') {
			this.showMessage('');
			this.setRouterQuery({
				page: 'login',
				prevPage: this.getPageName() !== 'login' ? this.getPageName() : undefined
			}, { mergeOldQuery: true });
		}
	}

	/** Gets the parent element of the page and the child element where the page will be inserted before. */
	protected override getPageParentAndBefore(): [Element, Element] {
		return [this.root, this.query(':scope > .message', Element)];
	}

	/** Gets the project id. */
	get projectId(): string | undefined {
		return this._projectId;
	}

	/** Callback when a new page is shown. */
	protected onNewPage(): void {
	}

	/** Sets the subtitle. */
	setSubtitle(subtitle: string): void {
		const title = (subtitle !== '' ? ' - ' : '') + subtitle;
		document.title = `Financio${title}`;
		this.query('.title .page-title', HTMLElement).innerHTML = title;
	}

	/** Shows a message. */
	showMessage(message: string): void {
		this.query(':scope > .message', HTMLDivElement).innerHTML = message;
	}

	/** Toggles the nav menu off and on. */
	protected toggleNav(event: Event): void {
		const nav = this.query('.nav', Element);
		if (nav.contains(event.target as Node)) {
			return;
		}
		if (nav.classList.contains('on')) {
			window.removeEventListener('click', this.toggleNav);
		}
		else {
			window.addEventListener('click', this.toggleNav);
		}
		this.query('.nav', Element).classList.toggle('on');
		event.stopPropagation();
	}

	private _projectId: string | undefined;

	protected static override html = html;

	protected static override css = css;

	protected static override childComponentTypes = componentTypes;
}

FinancioApp.setAppClass();
