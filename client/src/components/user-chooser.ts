import { Params } from '@orourley/elm-app';
import { FinancioApp } from '../internal';
import { Chooser } from './chooser';
import { UserListing } from '@orourley/financio-types';

export class UserChooser extends Chooser {

	/** Constructor. */
	constructor(params: Params) {
		super('User', params);
	}

	/** Subclasses implement this, which should return html for the list options. */
	protected async addOptions(): Promise<string> {

		// Get the units.
		const users = await (this.app as FinancioApp).send<Record<string, UserListing>>('users', 'listUsers', {});

		// Make the html, excluding some units.
		let html = '';
		for (const username of Object.keys(users)) {
			html += `<option value="${username}">${users[username]!.displayName}</option>`;
		}

		// Return the html.
		return html;
	}
}
