import { FormEasy, FormValues, FormInput, Params } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { EditPopup, FormEntry } from '../internal';

import css from './item-edit-popup.css';

export abstract class ItemEditPopup extends EditPopup {

	/** Constructor. */
	constructor(type: string, module: string, params: Params) {
		super(type, params);

		// Set the module.
		this._module = module;

		// Set the project id.
		this._projectId = params.attributes.get('projectid') ?? this.app.projectId;
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected abstract getExtraFormEntriesHtml(): string;

	/** Populates the form with any default values when we're creating. */
	protected abstract populateExtraFieldDefaults(form: FormEasy): Promise<void>;

	/** Populates the form with any extra values. */
	protected abstract populateExtraFieldValues(id: string, item: Items.Item, form: FormEasy): Promise<void>;

	/** Gets any extra form values for sending to the server. */
	protected abstract getExtraFormValues(values: FormValues): Record<string, any>;

	protected getFormEntriesHtml(): string {
		return `
			<FormEntry name="name" label="Name">
				<FormInput></FormInput>
				<div class="tip">The name of the ${this.type.toLocaleLowerCase()}.</div>
			</FormEntry>
			<FormEntry name="description" label="Description">
				<FormInput></FormInput>
				<div class="tip">The longer description of this ${this.type.toLocaleLowerCase()}. It will appear just under the name of the ${this.type.toLocaleLowerCase()}.</div>
			</FormEntry>
			<FormEntry name="parentId" label="Parent Group">
				<${this.type}Chooser selectGroups="only"></${this.type}Chooser>
				<div class="tip">The new ${this.type.toLocaleLowerCase()} will be placed under this group at the end, or sorted if the group has Sort Children set.</div>
			</FormEntry>
			${this.getExtraFormEntriesHtml()}
		`;
	}

	protected override async populateFieldDefaults(form: FormEasy): Promise<void> {
		await this.populateExtraFieldDefaults(form);
	}

	protected override async populateFieldValues(id: string, form: FormEasy): Promise<string> {

		// Get the item.
		const item = await this.app.send<Items.Item>(this._module, 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Set the options.
		form.getEntries().queryComponent('.name', FormEntry).getInput(FormInput).value = item.name;
		form.getEntries().queryComponent('.description', FormEntry).getInput(FormInput).value = item.description ?? '';

		// Remove the parent id, since we're now editing the item.
		form.getEntries().queryComponent('.parentId', FormEntry).setClass('hidden', true);

		// Populate any extra form values.
		await this.populateExtraFieldValues(id, item, form);

		// Return the name.
		return item.name;
	}

	protected override async save(id: string | undefined, _form: FormEasy, values: FormValues): Promise<string> {

		// Get the name.
		const name = values['name'] as string;
		if (name === '') {
			throw new Error(`Please give the ${this.type.toLocaleLowerCase()} a name.`);
		}

		// Get the description.
		const description = values['description'] as string;

		// Get the parent id.
		const parentId = values['parentId'] as string;

		// Get any extra form values.
		const extraFormValues = this.getExtraFormValues(values);

		// Send the data to the server.
		return this.app.send<string>(this._module, 'setItemOrGroup', {
			projectId: this._projectId,
			id: id,
			name: name,
			description: description,
			...(parentId !== '' && { parentId }),
			...extraFormValues
		});
	}

	// The module for server commands.
	private _module: string;

	/** The projectId. */
	private _projectId: string | undefined;

	protected static override css = css;
}
