import { Component, FormEasy, FormValues, FormInput, FormSelect, Params } from '@orourley/elm-app';
import { Filter } from '@orourley/financio-types';
import { AccountChooser, CategoryChooser, FormEntry } from '../internal';
import { RelativeDateChooser } from './relative-date-chooser';

import html from './filter-form.html';
import css from './filter-form.css';

export class FilterForm extends Component {

	/** The canceled callback. */
	private onCanceled: (() => void) | undefined;

	/** The submitted callback. */
	private onSubmitted: ((transactionFilter: Filter) => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onCanceled = params.eventHandlers.get('canceled');
		this.onSubmitted = params.eventHandlers.get('submitted');
	}

	/** Gets the forced filter that will always be enabled. */
	get forcedFilter(): Filter {
		return this._forcedFilter;
	}

	/** Sets the forced filter that will always be enabled. */
	set forcedFilter(transactionFilter: Filter) {

		// Save the forced filter.
		this._forcedFilter = { ...transactionFilter };

		// Get the form.
		const form = this.queryComponent('.FormEasy', FormEasy);

		// Go through each filter type, disabling input in that entry.
		form.getEntries().queryComponent('.startDate', FormEntry).classList.toggle('disabled', this._forcedFilter.startDate !== undefined);
		form.getEntries().queryComponent('.endDate', FormEntry).classList.toggle('disabled', this._forcedFilter.endDate !== undefined);
		form.getEntries().queryComponent('.minAmount', FormEntry).classList.toggle('disabled', this._forcedFilter.minAmount !== undefined);
		form.getEntries().queryComponent('.maxAmount', FormEntry).classList.toggle('disabled', this._forcedFilter.maxAmount !== undefined);
		form.getEntries().queryComponent('.search', FormEntry).classList.toggle('disabled', this._forcedFilter.search !== undefined);
		form.getEntries().queryComponent('.reviewed', FormEntry).classList.toggle('disabled', this._forcedFilter.reviewed !== undefined);
		form.getEntries().queryComponent('.accountId1', FormEntry).classList.toggle('disabled', this._forcedFilter.accountId1 !== undefined);
		form.getEntries().queryComponent('.accountId2', FormEntry).classList.toggle('disabled', this._forcedFilter.accountId2 !== undefined);
		form.getEntries().queryComponent('.categoryId', FormEntry).classList.toggle('disabled', this._forcedFilter.categoryIds !== undefined);

		// Update the form.
		this.updateForm();
	}

	/** Gets the filter. */
	get filter(): Filter {

		// Make the final filter.
		const filter = { ...this._filter, ...this._forcedFilter };

		// Return the filter.
		return filter;
	}

	/** Sets the filter. */
	set filter(transactionFilter: Filter) {

		// Save the filter.
		this._filter = { ...transactionFilter };

		// Update the form.
		this.updateForm();
	}

	/** Updates the form from filter. */
	private updateForm(): void {

		// Make the final filter.
		const filter = { ...this._filter, ...this._forcedFilter };

		// Get the form.
		const form = this.queryComponent('.FormEasy', FormEasy);

		// Go through each filter type, setting its value.
		if (filter.startDate !== undefined) {
			const input = form.getEntries().queryComponent('.startDate', FormEntry).getInput(RelativeDateChooser);
			input.value = filter.startDate;
		}
		if (filter.endDate !== undefined) {
			const input = form.getEntries().queryComponent('.endDate', FormEntry).getInput(RelativeDateChooser);
			input.value = filter.endDate;
		}
		if (filter.minAmount !== undefined) {
			const input = form.getEntries().queryComponent('.minAmount', FormEntry).getInput(FormInput);
			input.value = filter.minAmount.toString();
		}
		if (filter.maxAmount !== undefined) {
			const input = form.getEntries().queryComponent('.maxAmount', FormEntry).getInput(FormInput);
			input.value = filter.maxAmount.toString();
		}
		if (filter.search !== undefined) {
			const input = form.getEntries().queryComponent('.search', FormEntry).getInput(FormInput);
			input.value = filter.search;
		}
		if (filter.reviewed !== undefined) {
			const input = form.getEntries().queryComponent('.reviewed', FormEntry).getInput(FormSelect);
			input.value = filter.reviewed !== undefined ? (filter.reviewed ? 'yes' : 'no') : '';
		}
		if (filter.accountId1 !== undefined) {
			const input = form.getEntries().queryComponent('.accountId1', FormEntry).getInput(AccountChooser);
			input.value = filter.accountId1;
		}
		if (filter.accountId2 !== undefined) {
			const input = form.getEntries().queryComponent('.accountId2', FormEntry).getInput(AccountChooser);
			input.value = filter.accountId2;
		}
		if (filter.categoryIds !== undefined && filter.categoryIds.length > 0) {
			const input = form.getEntries().queryComponent('.categoryId', FormEntry).getInput(CategoryChooser);
			input.value = filter.categoryIds[0] ?? '';
		}
	}

	/** Called when the filter form is canceled. */
	protected onFormCanceled(): void {
		if (this.onCanceled) {
			this.onCanceled();
		}
	}

	/** Called when the filter form is submitted. */
	protected async onFormSubmitted(form: FormEasy, values: FormValues): Promise<string> {

		const startDate = form.getEntries().queryComponent('.startDate', FormEntry).getInput(RelativeDateChooser).value;
		const endDate = form.getEntries().queryComponent('.endDate', FormEntry).getInput(RelativeDateChooser).value;
		const minAmount = values['minAmount'] as string;
		const maxAmount = values['maxAmount'] as string;
		const search = values['search'] as string;
		const reviewed = values['reviewed'] as string;
		const accountIdFrom = values['accountIdFrom'] as string;
		const accountIdTo = values['accountIdTo'] as string;
		const categoryId = values['categoryId'] as string;

		// Turn the amounts to numbers and get defaults.
		const minAmountNumber = minAmount !== '' ? parseFloat(minAmount) : 0;
		if (isNaN(minAmountNumber)) {
			return 'Please enter a number for min amount or leave it blank.';
		}
		const maxAmountNumber = maxAmount !== '' ? parseFloat(maxAmount) : 0;
		if (isNaN(maxAmountNumber)) {
			return 'Please enter a number for max amount or leave it blank.';
		}

		// Make the accounts list.
		const accountIdsFrom = [];
		if (accountIdFrom !== '') {
			accountIdsFrom.push(accountIdFrom);
		}

		// Make the accounts list.
		const accountIdsTo = [];
		if (accountIdTo !== '') {
			accountIdsTo.push(accountIdTo);
		}

		// Make the categories list.
		const categoryIds = [];
		if (categoryId !== '') {
			categoryIds.push(categoryId);
		}

		// Create the filter.
		this._filter = {
			startDate,
			endDate,
			...(minAmount !== '' && { minAmount: minAmountNumber }),
			...(maxAmount !== '' && { maxAmount: maxAmountNumber }),
			...(search !== '' && { search }),
			...(reviewed !== '' && { reviewed: reviewed === 'yes' }),
			...(accountIdsFrom.length > 0 && { accountIdsFrom }),
			...(accountIdsTo.length > 0 && { accountIdsTo }),
			...(categoryIds.length > 0 && { categoryIds })
		};

		// Call the created callback.
		if (this.onSubmitted) {
			this.onSubmitted(this._filter);
		}

		return '';
	}

	/** A filter that will always be used. */
	private _forcedFilter: Filter = {};

	/** The current filter in use. */
	private _filter: Filter = {};

	protected static override html = html;
	protected static override css = css;
}
