import { Component, Params } from '@orourley/elm-app';
import { Interval, YMD } from '@orourley/financio-types';

import html from './calendar.html';
import css from './calendar.css';

/** The click callback type. */
type ClickCallback = (date: YMD) => void;

/** A calendar component that can select a single day. */
export class Calendar extends Component {

	/** The onClick callback. */
	clickCallback: ClickCallback | undefined;

	constructor(params: Params) {
		super(params);

		// Get the click callback.
		this.clickCallback = params.eventHandlers.get('click');

		// Setup the date objects.
		for (let i = 0; i < 42; i++) {
			this._dates.push(new YMD());
		}

		// Set the shown date to now.
		const now = new Date();
		this._updateShown(now.getFullYear(), now.getMonth() + 1, false);
	}

	/** Selects dates. */
	select(date: YMD | Interval<YMD> | (YMD | Interval<YMD>)[]): void {
		// Add the selected dates.
		const selectedDates = this._getIntervalsFromParam(date);
		this._selectedDateRanges.splice(this._selectedDateRanges.length - 1, 0, ...selectedDates);
		// Union up any ranges.
		for (let i = 0; i < this._selectedDateRanges.length; i++) {
			for (let j = i + 1; j < this._selectedDateRanges.length; j++) {
				const selectedDateRangeLhs = this._selectedDateRanges[i]!;
				const selectedDateRangeRhs = this._selectedDateRanges[j]!;
				if (selectedDateRangeRhs.min <= selectedDateRangeLhs.max && selectedDateRangeLhs.min <= selectedDateRangeRhs.max) {
					if (selectedDateRangeLhs.max < selectedDateRangeRhs.max) {
						selectedDateRangeLhs.max.copy(selectedDateRangeRhs.max);
					}
					if (selectedDateRangeLhs.min > selectedDateRangeRhs.min) {
						selectedDateRangeLhs.min.copy(selectedDateRangeRhs.min);
					}
					this._selectedDateRanges.splice(j, 1);
					j -= 1;
				}
			}
		}
		// Update the calendar.
		const lastSelectedDate = selectedDates[selectedDates.length - 1];
		if (lastSelectedDate) {
			this._updateShown(lastSelectedDate.max.year, lastSelectedDate.max.month, true);
		}
	}

	/** Deselects dates. */
	deselect(date: YMD | Interval<YMD> | (YMD | Interval<YMD>)[]): void {
		// Get the deselected dates.
		const deselectedDates = this._getIntervalsFromParam(date);
		// Set the selected dates.
		for (let i = 0; i < this._selectedDateRanges.length; i++) {
			const dateInterval = this._selectedDateRanges[i]!;
			for (let j = 0; j < deselectedDates.length; j++) {
				const deselectInterval = deselectedDates[j]!;
				if (deselectInterval.min <= dateInterval.max && dateInterval.min <= deselectInterval.max) {
					if (dateInterval.max <= deselectInterval.max) { // Move down the max.
						dateInterval.max.copy(deselectInterval.min);
						dateInterval.max.day -= 1;
					}
					if (dateInterval.min >= deselectInterval.min) { // Move up the min.
						dateInterval.min.copy(deselectInterval.max);
						dateInterval.min.day += 1;
					}
					if (dateInterval.min < deselectInterval.min && deselectInterval.max < dateInterval.max) { // Split the dateInterval in two.
						const splitPoint = new YMD(deselectInterval.min);
						splitPoint.day -= 1;
						this._selectedDateRanges.push(new Interval(dateInterval.min, splitPoint));
						dateInterval.min.copy(deselectInterval.max);
						dateInterval.min.day += 1;
					}
					if (dateInterval.min > dateInterval.max) { // Remove the now invalid interval.
						this._selectedDateRanges.splice(i, 1);
						i -= 1;
					}
				}
			}
		}
		// Update the calendar.
		const lastDeselectedDate = deselectedDates[deselectedDates.length - 1];
		if (lastDeselectedDate) {
			this._updateShown(lastDeselectedDate.max.year, lastDeselectedDate.max.month, true);
		}
	}

	/** Clears all of the selections. */
	clearSelections(): void {
		this._selectedDateRanges = [];
		// Update the calendar.
		this._updateShown(this._shownYear, this._shownMonth, true);
	}

	/** Gets an array of intervals from the date param. */
	_getIntervalsFromParam(date: YMD | Interval<YMD> | (YMD | Interval<YMD>)[]): Interval<YMD>[] {
		const intervals: Interval<YMD>[] = [];
		if (date instanceof YMD) {
			intervals.push(new Interval(date, date));
		}
		else if (date instanceof Interval) {
			intervals.push(new Interval(date.min, date.max));
		}
		else if (date instanceof Array) {
			for (let i = 0; i < date.length; i++) {
				const dateItem = date[i];
				if (dateItem instanceof YMD) {
					intervals.push(new Interval(dateItem, dateItem));
				}
				else if (dateItem instanceof Interval) {
					intervals.push(new Interval(dateItem.min, dateItem.max));
				}
			}
		}
		return intervals;
	}

	/** Updates the shown year and month. */
	private _updateShown(year: number, month: number, forceUpdate: boolean): void {
		let daysNeedUpdate = false;
		if (this._shownYear !== year) {
			this._shownYear = year;
			this.setHtml(this._shownYear.toString(), this.query('.year_current', HTMLElement)!, this);
			daysNeedUpdate = true;
		}
		if (this._shownMonth !== month) {
			this._shownMonth = month;
			this.setHtml(Calendar._monthNames[month - 1]!, this.query('.month_current', HTMLElement)!, this);
			daysNeedUpdate = true;
		}
		if (daysNeedUpdate || forceUpdate) {
			const date = new YMD(this._shownYear, this._shownMonth, 1);
			date.day = -date.dayOfWeek;
			for (let i = 0; i < 42; i++, date.day += 1) {
				const dayElem = this.query(`.day_${i}`, HTMLElement)!;
				if (date.month === this._shownMonth) {
					dayElem.classList.remove('anotherMonth');
				}
				else {
					dayElem.classList.add('anotherMonth');
				}
				if (this._dateBounds.min <= date && date <= this._dateBounds.max) {
					dayElem.innerHTML = date.day.toString();
					dayElem.classList.add('clickable');
					// Check if the date is selected.
					let selected = false;
					let first = false;
					let last = false;
					for (let i = 0; i < this._selectedDateRanges.length; i++) {
						const selectedDateRange = this._selectedDateRanges[i]!;
						if (selectedDateRange.min <= date && date <= selectedDateRange.max) {
							selected = true;
							first = selectedDateRange.min.equals(date);
							last = selectedDateRange.max.equals(date);
						}
					}
					if (selected) {
						dayElem.classList.add('selected');
					}
					else {
						dayElem.classList.remove('selected');
					}
					if (first) {
						dayElem.classList.add('first');
					}
					else {
						dayElem.classList.remove('first');
					}
					if (last) {
						dayElem.classList.add('last');
					}
					else {
						dayElem.classList.remove('last');
					}
				}
				else {
					dayElem.innerHTML = '';
					dayElem.classList.remove('clickable', 'selected');
				}
				this._dates[i]!.copy(date);
			}
		}
	}

	/** Offsets the currently show month by the number. */
	private _offsetDate(years: number, months: number): void {
		const newDate = new YMD(this._shownYear, this._shownMonth, 1);
		newDate.year += years;
		newDate.month += months;
		if (newDate < this._dateBounds.min) {
			newDate.copy(this._dateBounds.min);
		}
		else if (newDate > this._dateBounds.max) {
			newDate.copy(this._dateBounds.max);
		}
		this._updateShown(newDate.year, newDate.month, false);
	}

	/** Triggered when the user clicks a date. */
	protected _onClick(event: MouseEvent): void {
		const elem = event.target;
		if (elem instanceof HTMLDivElement) {
			const index = parseInt(elem.className.substring(4));
			const date = this._dates[index]!;
			if (this._dateBounds.min <= date && date <= this._dateBounds.max) {
				if (this.clickCallback) {
					this.clickCallback(date);
				}
			}
		}
	}

	/** Decrements the date by 10 years. */
	protected _decadeDec(): void {
		this._offsetDate(-10, 0);
	}

	/** Increments the date by 10 years. */
	protected _decadeInc(): void {
		this._offsetDate(+10, 0);
	}

	/** Decrements the date by 1 year. */
	protected _yearDec(): void {
		this._offsetDate(-1, 0);
	}

	/** Increments the date by 1 year. */
	protected _yearInc(): void {
		this._offsetDate(+1, 0);
	}

	/** Decrements the date by 1 month. */
	protected _monthDec(): void {
		this._offsetDate(0, -1);
	}

	/** Increments the date by 1 month. */
	protected _monthInc(): void {
		this._offsetDate(0, +1);
	}

	/** The currently shown year. */
	private _shownYear: number = Number.NaN;

	/** The currently shown month. */
	private _shownMonth: number = Number.NaN;

	/** The list of date objects for each date shown. */
	private _dates: YMD[] = [];

	/** The currently selected date ranges. */
	private _selectedDateRanges: Interval<YMD>[] = [];

	/** The bounds on the date selection. */
	private _dateBounds = new Interval(new YMD(1, 1, 1), new YMD(9999, 12, 31));

	/** The names of the months. */
	private static _monthNames = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'];

	protected static override html = html;
	protected static override css = css;
}
