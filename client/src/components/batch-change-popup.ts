import { FormEasy, FormValues, Params } from '@orourley/elm-app';
import { Transaction } from '@orourley/financio-types';
import { FinancioApp } from '../internal';

import html from './batch-change-popup.html';
import css from './batch-change-popup.css';
import { FinancioPopup } from '../financio-popup';

export class BatchChangePopup extends FinancioPopup {

	/** The submitted callback. */
	private onSubmitted: (() => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onSubmitted = params.eventHandlers.get('submitted');

		// Set the content of the window.
		this.setHtml(html, this.query('.window', Element), this);
	}

	/** Sets the transactions to work on. */
	setTransactions(transactions: Transaction[]): void {
		this._transactions = transactions;
	}

	/** The form was canceled, so call the callback. */
	onFormCanceled(): void {
		this.app.closeOverlay(this);
	}

	protected async onFormSubmitted(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the fields.
		const categoryId = values['categoryId'] as string;
		const accountIdFrom = values['accountIdFrom'] as string;
		const accountIdTo = values['accountIdTo'] as string;
		const reviewed = values['reviewed'] as string;

		// Batch change all transactions.
		for (const transaction of this._transactions) {

			// Category
			if (categoryId === '*Clear*') {
				transaction.categoryId = '';
			}
			else if (categoryId !== '*No Change*') {
				transaction.categoryId = categoryId;
			}

			// Account From
			if (accountIdFrom === '*Clear*') {
				transaction.accountIdFrom = '';
			}
			else if (accountIdFrom !== '*No Change*') {
				transaction.accountIdFrom = accountIdFrom;
			}

			// Account To
			if (accountIdTo === '*Clear*') {
				transaction.accountIdTo = '';
			}
			else if (accountIdTo !== '*No Change*') {
				transaction.accountIdTo = accountIdTo;
			}

			// Reviewed
			if (reviewed !== '*No Change*') {
				transaction.reviewed = reviewed === 'yes';
			}
		}

		// Send the update to the server.
		try {
			await (this.app as FinancioApp).send('transactions', 'update', {
				projectId: (this.app as FinancioApp).projectId,
				transactions: this._transactions.map((transaction) => ({ dateFromOld: transaction.dateFrom, transaction: transaction }))
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Call the created callback.
		this.app.closeOverlay(this);
		if (this.onSubmitted) {
			this.onSubmitted();
		}

		return 'The transactions have been changed.';
	}

	protected async deleteRule(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const verify = values['verify'] as string;

		if (verify !== 'DELETE') {
			return 'Please enter DELETE to confirm.';
		}

		// Send the commands.
		const promises = [];
		for (const transaction of this._transactions) {
			promises.push((this.app as FinancioApp).send('transactions', 'delete', {
				projectId: (this.app as FinancioApp).projectId,
				id: transaction.id,
				dateFrom: transaction.dateFrom
			}));
		}
		try {
			await Promise.all(promises);
		}
		catch (error) {
			return (error as Error).message;
		}

		// Call the created callback.
		if (this.onSubmitted) {
			this.onSubmitted();
		}

		return 'The transactions have been deleted.';
	}

	private _transactions: Transaction[] = [];

	protected static override css = css;
}
