import { FormCheckbox, FormEasy, FormValues, FormInput, Params } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { EditPopup, FormEntry } from '../internal';

import css from './group-edit-popup.css';

export class GroupEditPopup extends EditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Group', params);

		const itemType = params.attributes.get('itemtype');
		if (itemType === undefined) {
			throw new Error('An itemType must be specified.');
		}
		this._itemType = itemType;

		const module = params.attributes.get('module');
		if (module === undefined) {
			throw new Error('A module must be specified.');
		}
		this._module = module;

		this._file = params.attributes.has('file');
	}

	/** Gets the form entries html. */
	protected getFormEntriesHtml(): string {
		return `
			<FormEntry name="name" label="Name"><FormInput></FormInput></FormEntry>
			<FormEntry name="description" label="Description"><FormInput></FormInput></FormEntry>
			<FormEntry name="sortChildren" label="Sort Children"><FormCheckbox></FormCheckbox></FormEntry>
			<FormEntry name="parentId" label="Group">
				<${this._itemType}Chooser selectGroups="only"></${this._itemType}Chooser>
				<div class="tip">The new ${this._itemType.toLocaleLowerCase()} will be placed under this group at the end, or sorted if the group has Sort Children set.</div>
			</FormEntry>`;
	}

	protected override async populateFieldDefaults(form: FormEasy): Promise<void> {

		if (this._file) {
			form.getEntries().removeComponent(form.getEntries().queryComponent('.name', FormEntry));
			form.getEntries().removeComponent(form.getEntries().queryComponent('.description', FormEntry));
		}

		form.getEntries().queryComponent('.sortChildren', FormEntry).getInput(FormCheckbox).value = true;
	}

	protected override async populateFieldValues(id: string | undefined, form: FormEasy): Promise<string | undefined> {

		// Remove the parent id, since we're now editing the item.
		form.getEntries().queryComponent('.parentId', FormEntry).setClass('hidden', true);

		if (this._file) {

			// Get the file list.
			const itemFile = await this.app.send<Omit<Items.File, 'list'>>(this._module, 'getFileSansList', {
				projectId: this.app.projectId
			});

			// Set the values.
			form.getEntries().queryComponent('.sortChildren', FormEntry).getInput(FormCheckbox).value = itemFile.sortChildren;

			// Return the name.
			return '';
		}
		else {

			// Get the group.
			const group = await this.app.send<Items.Group>(this._module, 'getGroup', {
				projectId: this.app.projectId,
				id: id
			});

			// Set the options.
			form.getEntries().queryComponent('.name', FormEntry).getInput(FormInput).value = group.name;
			form.getEntries().queryComponent('.description', FormEntry).getInput(FormInput).value = group.description ?? '';
			form.getEntries().queryComponent('.sortChildren', FormEntry).getInput(FormCheckbox).value = group.sortChildren;

			// Return the name.
			return group.name;
		}
	}

	protected override async save(id: string | undefined, _form: FormEasy, values: FormValues): Promise<string> {

		if (this._file) {

			// Get the sort children flag.
			const sortChildren = values['sortChildren'] as boolean;

			// Send the data to the server.
			return await this.app.send<string>(this._module, 'setFile', {
				projectId: this.app.projectId,
				id: id,
				sortChildren: sortChildren
			});
		}
		else {

			// Get the name.
			const name = values['name'] as string;
			if (name === '') {
				throw new Error('Please give the group a name.');
			}

			// Get the description.
			const description = values['description'] as string;

			// Get the sort children flag.
			const sortChildren = values['sortChildren'] as boolean;

			// Get the parent id.
			const parentId = values['parentId'] as string;

			// Send the data to the server.
			return await this.app.send<string>(this._module, 'setItemOrGroup', {
				projectId: this.app.projectId,
				id: id,
				name: name,
				description: description,
				sortChildren: sortChildren,
				...(parentId !== '' && { parentId })
			});
		}
	}

	/** The item type. */
	private _itemType: string;

	/** The module to use. */
	private _module: string;

	/** Whether or not this is a file (or group). */
	private _file: boolean;

	protected static override css = css;
}
