import { Params } from '@orourley/elm-app';
import { Unit } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class UnitDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Unit', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the unit.
		const unit = await this.app.send<Unit>('units', 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return unit.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('units', 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}
}
