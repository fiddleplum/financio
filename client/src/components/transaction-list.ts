import { Component, CustomPopup, Params, Popup, Router } from '@orourley/elm-app';
import { Filter, Transaction, Rule, AccountFile, CategoryFile, UnitFile, RuleFile, Items } from '@orourley/financio-types';

import { FinancioApp } from '../internal';
import { TransactionView } from './transaction-view';
import { FilterForm } from './filter-form';
import { BatchChangePopup } from './batch-change-popup';

import html from './transaction-list.html';
import css from './transaction-list.css';

export class TransactionList extends Component {

	/** A callback for when the transactions update. */
	onUpdated: (() => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Save the attributes.
		this._editable = params.attributes.has('editable');

		// Save the callbacks.
		this.onUpdated = params.eventHandlers.get('updated');

		// Set a default filter.
		void this.setFilter({
			startDate: ',-4,months,beginning',
			endDate: ',-1,months,ending'
		});
	}

	/** Gets the current list of transactions. */
	getTransactions(): Transaction[] {
		return this._transactions;
	}

	/** Gets the transaction views. */
	getTransactionViews(): TransactionView[] {
		return this.queryAllComponents('.TransactionView', TransactionView);
	}

	/** Gets the filter. */
	getFilter(): Filter {
		return this._filter;
	}

	/** Set the transaction filter that will be applied. */
	async setFilter(filter: Filter): Promise<void> {

		// Set the filter.
		this._filter = structuredClone(filter);

		// Update the list.
		await this.updateList();
	}

	async setFilterFromQuery(query: Router.Query): Promise<void> {
		if (query['startDate'] !== undefined) {
			this._filter.startDate = query['startDate'];
		}
		if (query['endDate'] !== undefined) {
			this._filter.endDate = query['endDate'];
		}
		if (query['minAmount'] !== undefined) {
			const minAmount = parseFloat(query['minAmount']);
			if (!isNaN(minAmount)) {
				this._filter.minAmount = minAmount;
			}
		}
		if (query['maxAmount'] !== undefined) {
			const maxAmount = parseFloat(query['maxAmount']);
			if (!isNaN(maxAmount)) {
				this._filter.maxAmount = maxAmount;
			}
		}
		if (query['search'] !== undefined) {
			this._filter.search = query['search'];
		}
		if (query['reviewed'] !== undefined) {
			if (query['reviewed'] === 'yes') {
				this._filter.reviewed = true;
			}
			else if (query['reviewed'] === 'no') {
				this._filter.reviewed = false;
			}
		}
		if (query['accountId1'] !== undefined) {
			this._filter.accountId1 = query['accountId1'];
		}
		if (query['accountId2'] !== undefined) {
			this._filter.accountId2 = query['accountId2'];
		}
		if (query['categoryIds'] !== undefined) {
			this._filter.categoryIds = query['categoryIds'].split(',');
		}
	}

	/** Set the transaction filter that will always be applied. */
	async setForcedFilter(filter: Filter): Promise<void> {

		// Set the forced filter.
		this._forcedFilter = structuredClone(filter);

		// Update the list.
		await this.updateList();
	}

	/** Called when a transaction view has been saved. */
	transactionSaved(transactionView: TransactionView): void {

		// Get the transaction.
		const transaction = transactionView.getTransaction();
		if (!transaction) {
			return;
		}

		// Get the transaction index.
		const transactionIndex = this._transactions.findIndex((aTransaction) => aTransaction.id === transaction.id);
		if (transactionIndex === -1) {
			return;
		}

		// Update the transactions list entry.
		this._transactions.splice(transactionIndex, 1, transaction);
	}

	/** Called when the transaction view delete button is pressed. */
	transactionDeleted(transactionView: TransactionView): void {

		// Get the transaction.
		const transaction = transactionView.getTransaction();
		if (!transaction) {
			return;
		}

		// Get the transaction index.
		const transactionIndex = this._transactions.findIndex((aTransaction) => aTransaction.id === transaction.id);
		if (transactionIndex === -1) {
			return;
		}

		// Remove it from the transactions list.
		this._transactions.splice(transactionIndex, 1);

		// Remove the transaction view component.
		this.removeComponent(transactionView);
	}

	/** Called when the transaction view join button is pressed. */
	transactionJoined(_updatedTransactionid: string, deletedTransactionId: string): void {

		// Get the transaction index.
		const transactionIndex = this._transactions.findIndex((aTransaction) => aTransaction.id === deletedTransactionId);
		if (transactionIndex === -1) {
			return;
		}

		// Remove it from the transactions list.
		this._transactions.splice(transactionIndex, 1);

		// Remove the transaction view component, if it exists.
		if (this.queryHas(`.TransactionView[transactionId="${deletedTransactionId}"]`)) {
			this.removeNode(this.query(`.TransactionView[transactionId="${deletedTransactionId}"]`, Element));
		}
	}

	/** Called when the transaction view join button is pressed. */
	transactionSplit(newTransactions: Transaction[], deletedTransactionId: string): void {

		// Get the transaction index.
		const transactionIndex = this._transactions.findIndex((aTransaction) => aTransaction.id === deletedTransactionId);
		if (transactionIndex === -1) {
			return;
		}

		// Replace the transactions list entry with the new transactions.
		this._transactions.splice(transactionIndex, 1, ...newTransactions);

		// Remove the transaction view component, if it exists.
		const transactionViewToDelete = this.queryComponent(`.TransactionView[transactionId="${deletedTransactionId}"]`, TransactionView);

		// Make the new transaction views.
		for (const newTransaction of newTransactions) {
			this.insertTransactionView(newTransaction, transactionViewToDelete);
		}

		// Remove the transaction view component.
		this.removeComponent(transactionViewToDelete);
	}

	/** Sets the sort mode. */
	async setSortMode(sortMode: 'reviewed' | 'date' | 'amount'): Promise<void> {

		// Flip the ascending if the same sort button is pressed.
		if (sortMode === this._sortMode) {
			this._sortAscending = !this._sortAscending;
		}

		// Set the sort mode.
		this._sortMode = sortMode;

		// Update the list.
		await this.updateList();
	}

	/** Updates the list of transactions. */
	private async updateList(): Promise<void> {

		// Get the financio app.
		const financioApp = this.app as FinancioApp;

		// Make the final filter.
		const filter = { ...this._filter, ...this._forcedFilter };

		// Get the latest transactions that are associated with this account.
		this._transactions = await financioApp.send<Transaction[]>('transactions', 'list', {
			projectId: financioApp.projectId,
			filter: filter
		});

		// Sort using the mode.
		if (this._sortMode === 'date') {
			this._transactions.sort((a, b) => {
				if (a.dateFrom !== b.dateFrom) {
					return a.dateFrom.localeCompare(b.dateFrom) * (this._sortAscending ? 1 : -1);
				}
				else if (a.amountFrom !== b.amountFrom) {
					return (a.amountFrom - b.amountFrom) * (this._sortAscending ? 1 : -1);
				}
				return a.description.localeCompare(b.description) * (this._sortAscending ? 1 : -1);
			});
		}
		else if (this._sortMode === 'amount') {
			this._transactions.sort((a, b) => (a.amountFrom - b.amountFrom) * (this._sortAscending ? 1 : -1));
		}
		else if (this._sortMode === 'reviewed') {
			this._transactions.sort((a, b) => ((a.reviewed ? 1 : 0) - (b.reviewed ? 1 : 0)) * (this._sortAscending ? 1 : -1));
		}

		// Get the account, category, and unit files for use in the TransactionViews.
		const accountFile = await financioApp.send<AccountFile>('accounts', 'getFile', {
			projectId: financioApp.projectId
		});
		const categoryFile = await financioApp.send<CategoryFile>('categories', 'getFile', {
			projectId: financioApp.projectId
		});
		const unitFile = await financioApp.send<UnitFile>('units', 'getFile', {
			projectId: financioApp.projectId
		});
		this._files = {
			accountFile,
			categoryFile,
			unitFile
		};

		// Get the list element.
		const listElem = this.query(':scope > .list', Element);

		// Clear the existing TransactionViews.
		this.clearNode(listElem);

		// Create a TransactionView for each transaction.
		for (const transaction of this._transactions) {
			this.insertTransactionView(transaction, undefined);
		}

		if (this.onUpdated) {
			this.onUpdated();
		}
	}

	/** Toggles the filter. */
	protected openFilterPopup(): void {
		const popup = this.app.openOverlay<CustomPopup>('<CustomPopup><FilterForm onCanceled="filterCanceled" onSubmitted="filterSubmitted"></FilterForm></CustomPopup>', this);
		const filterForm = popup.getUserContent().queryComponent('.FilterForm', FilterForm);
		filterForm.filter = this._filter;
		filterForm.forcedFilter = this._forcedFilter;
		this._popup = popup;
	}

	/** Cancels any changes to the filter. */
	protected filterCanceled(): void {
		this.app.closeOverlay(this._popup!);
	}

	/** Applies the filter. */
	protected async filterSubmitted(): Promise<void> {
		const filterForm = (this._popup as CustomPopup).getUserContent().queryComponent('.FilterForm', FilterForm);
		this._filter = filterForm.filter;
		this._forcedFilter = filterForm.forcedFilter;
		this.app.closeOverlay(this._popup!);

		// Update the router.
		(this.app as FinancioApp).setRouterQuery({
			...(this._filter.startDate !== undefined && { startDate: this._filter.startDate }),
			...(this._filter.endDate !== undefined && { endDate: this._filter.endDate }),
			...(this._filter.minAmount !== undefined && { minAmount: this._filter.minAmount.toString() }),
			...(this._filter.maxAmount !== undefined && { maxAmount: this._filter.maxAmount.toString() }),
			...(this._filter.search !== undefined && { search: this._filter.search }),
			...(this._filter.reviewed !== undefined && { reviewed: this._filter.reviewed ? 'yes' : 'no' }),
			...(this._filter.accountId1 !== undefined && { accountIdsFrom: this._filter.accountId1 }),
			...(this._filter.accountId2 !== undefined && { accountIdsTo: this._filter.accountId2 }),
			...(this._filter.categoryIds && this._filter.categoryIds.length > 0 && { categoryIds: this._filter.categoryIds.join(',') })
		}, {
			mergeOldQuery: true,
			overwriteHistory: true,
			noCallbacks: true
		});

		// Update the list.
		await this.updateList();
	}

	/** Applies all rules to the transactions. */
	protected async applyAllRules(): Promise<void> {

		// Get the financio app.
		const financioApp = this.app as FinancioApp;

		// Get all of the rules.
		const ruleFile = await financioApp.send<RuleFile>('rules', 'getFile', {
			projectId: financioApp.projectId
		});

		// Apply all of the rules.
		const changedTransactions = [];
		for (const transaction of this._transactions) {
			for (const rule of Object.values(ruleFile.list)) {
				if (Items.isGroup(rule)) {
					continue;
				}
				if (Rule.apply(rule, transaction)) {
					changedTransactions.push(transaction);
				}
			}
		}

		// Send the update to the server.
		await financioApp.send('transactions', 'update', {
			projectId: financioApp.projectId,
			transactions: changedTransactions.map((transaction) => ({ dateFromOld: transaction.dateFrom, transaction: transaction }))
		});

		// Update the list.
		await this.updateList();
	}

	/** Shows the batch change popup. */
	showBatchChangePopup(): void {
		const popup = this.app.openOverlay<BatchChangePopup>(`<BatchChangePopup onSubmitted="batchChangeSubmitted"></BatchChangePopup>`, this);
		popup.setTransactions(this._transactions);
		this._popup = popup;
	}

	/** Called when the batch change popup has saved. */
	async batchChangeSubmitted(): Promise<void> {

		// Update the list.
		await this.updateList();
	}

	/** Shows a new transaction at the top. */
	showNewTransaction(): void {
		if (!this._files) {
			return;
		}
		const firstTransactionView = this.queryComponent(`:scope > .list .TransactionView`, TransactionView);
		this.insertTransactionView(undefined, firstTransactionView);
	}

	/** Duplicates a transaction. */
	async duplicateTransaction(transactionView: TransactionView): Promise<void> {
		if (!this._files) {
			return;
		}
		const transaction = transactionView.getTransaction();
		if (transaction !== undefined) {

			// Create the new transaction.
			const newTransaction = { ...transaction };
			newTransaction.id = '';

			// Send the update to the server. Save the transaction id in case it was a new transaction.
			newTransaction.id = (await (this.app as FinancioApp).send<string[]>('transactions', 'update', {
				projectId: (this.app as FinancioApp).projectId,
				transactions: [{
					dateFromOld: newTransaction.dateFrom,
					transaction: newTransaction
				}]
			}))[0]!;

			// Set the make the new view and add it the transaction to the list.
			this.insertTransactionView(newTransaction, transactionView);
			this._transactions.push(newTransaction);
		}
	}

	/** Inserts a new transaction view before another. */
	private insertTransactionView(transaction: Transaction | undefined, before: TransactionView | undefined): TransactionView {
		if (!this._files) {
			throw new Error('Missing files.');
		}
		const listElem = this.query(':scope > .list', Element);
		const newTranactionViewElem = this.insertHtml(`<TransactionView onSaved="transactionSaved" onDeleted="transactionDeleted" onJoined="transactionJoined" onSplit="transactionSplit" onNeedsDuplication="duplicateTransaction" ${this._editable ? 'editable' : ''}></TransactionView`, listElem, before, this)[0] as Element;
		const newTransactionView = this.queryComponent(newTranactionViewElem, TransactionView);
		newTransactionView.setTransaction(transaction, this._files);
		return newTransactionView;
	}

	/** Whether or not it is editable. */
	private _editable: boolean = true;

	/** The transactions. */
	private _transactions: Transaction[] = [];

	/** The files to pass to TransactionViews. */
	private _files: { accountFile: AccountFile; categoryFile: CategoryFile; unitFile: UnitFile; } | undefined;

	/** The filter. */
	private _filter: Filter = {};

	/** The forced filter. */
	private _forcedFilter: Filter = {};

	/** The popup. */
	private _popup: Popup | undefined;

	/** The fields that can be sorted. */
	private _sortMode: 'reviewed' | 'date' | 'amount' = 'date';

	/** Whether or not that field is sorted ascending or descending. */
	private _sortAscending: boolean = false;

	protected static override html = html;
	protected static override css = css;
}
