import { FormCheckbox, FormEasy, FormValues, FormInput, Params } from '@orourley/elm-app';
import { Account, Project } from '@orourley/financio-types';
import { FormEntry, UnitChooser } from '../internal';
import { ItemEditPopup } from './item-edit-popup';

import css from './account-edit-popup.css';

export class AccountEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Account', 'accounts', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return `
			<FormEntry name="unit" label="Unit">
				<UnitChooser showAdd></UnitChooser>
				<div class="tip">The unit will be placed after the amount in any transaction that contains this account, such as 1024.00 USD or 23.45 shares FORD.</div>
			</FormEntry>
			<FormEntry name="internal" label="Internal">
				<FormCheckbox></FormCheckbox>
				<div class="tip">Internal means that you have control over this account, such as a bank account, real estate, stocks, etc that you control. External is for accounts that you don't have control over.</div>
			</FormEntry>
			<FormEntry name="anchorMonth" label="Anchor Month">
				<FormInput value="0000-01" pattern="\\d{4}-\\d{2}"></FormInput>
				<div class="tip">
					<p>This month in the format YYYY-MM, combined with the Anchor Value below, is a known value of the account at known date. Combined with the transactions to and from the account, it is used to figure out the value of the account at any other date.</p>
					<p>Example 1: If this is a newly opened account, you can just choose the date the account was opened and its opening balance.</p>
					<p>Example 2: If this is a 20 year old bank account, but your first transaction is last year, you can set this to the balance of the account at the beginning of the month of the first transaction* so that later transactions show the right balance. You could even later put in transactions before the Anchor Date, and their balances will show correctly. *Make sure that your first transaction is the first transaction of that particular month, or else any other added transactions input between the anchor month and the first transaction will make the balance incorrect.</p>
					<p>Example 3: If this is an asset such as a house, you can just enter a purchase date with a value of 1. Since there are no transactions on the house until you sell it, the Unit Conversion Rates will be where you change the value of the house with respect to a currency, such as USD.</p>
				</div>
			</FormEntry>
			<FormEntry name="anchorValue" label="Anchor Value">
				<FormInput value="0" pattern="[+\\-]?\\d+(\\.\\d*)?|\\.\\d+"></FormInput>
				<div class="tip">See the Anchor Month tip above for more information.</div>
			</FormEntry>`;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(form: FormEasy): Promise<void> {

		// Populate the unit field with the pr		// Get the project.
		const project = await this.app.send<Project>('projects', 'getItem', {
			id: this.app.projectId
		});

		// Populate the unit field with the project's default unit.
		form.getEntries().queryComponent('.unit', FormEntry).getInput(UnitChooser).value = project.defaultUnitId;
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(_id: string, account: Account, form: FormEasy): Promise<void> {
		form.getEntries().queryComponent('.unit', FormEntry).getInput(UnitChooser).value = account.unitId;
		form.getEntries().queryComponent('.internal', FormEntry).getInput(FormCheckbox).value = account.internal;
		form.getEntries().queryComponent('.anchorMonth', FormEntry).getInput(FormInput).value = account.anchorPoint.month;
		form.getEntries().queryComponent('.anchorValue', FormEntry).getInput(FormInput).value = account.anchorPoint.value.toString();
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(values: FormValues): Record<string, any> {

		// Get the unit.
		const unit = values['unit'] as string;

		// Get the internal flag.
		const internal = values['internal'] as boolean;

		// Get the anchor month.
		const anchorMonth = values['anchorMonth'] as string;

		// Get the starting value.
		const anchorValueAsString = values['anchorValue'] as string;

		// Parse the starting value as a number.
		const anchorValue = Number.parseFloat(anchorValueAsString);
		if (isNaN(anchorValue)) {
			throw new Error('Please make the anchor value a number, without units.');
		}

		// Return the values.
		return {
			unit,
			internal,
			anchorMonth,
			anchorValue
		};
	}

	protected static override css = css;
}
