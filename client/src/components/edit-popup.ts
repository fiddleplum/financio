import { FormEasy, FormValues, Params } from '@orourley/elm-app';
import { FinancioPopup } from '../financio-popup';

import html from './edit-popup.html';
import css from './edit-popup.css';

export abstract class EditPopup extends FinancioPopup {

	/** Constructor. */
	constructor(type: string, params: Params) {
		super(params);

		// Save the type.
		this.type = type;

		// Are we editing? (False if creating).
		this._editing = params.attributes.has('edit');

		// Get the id.
		this._typeId = params.attributes.get('typeid');

		// Get the event handlers.
		this._onSaved = params.eventHandlers.get('saved');

		// Set the content of the window.
		this.setHtml(html, this.getWindow(), this);

		// Set the title.
		if (this._editing) {
			this.query('.window > h1 > .editCreate', Element).innerHTML = 'Edit';
		}
		else {
			this.query('.window > h1 > .editCreate', Element).innerHTML = 'Create';
		}
		this.query('.window > h1 > .type', Element).innerHTML = type;

		// Wait one 'frame' to initialize. This is so a sub class construct can complete.
		void Promise.resolve().then(() => {
			this.queryComponent('.window > .FormEasy', FormEasy).getEntries().setHtml(this.getFormEntriesHtml(), undefined, params.innerHtmlContext);
			this.initialize();
		});
	}

	/** Gets the form entries html. */
	protected abstract getFormEntriesHtml(): string;

	/** Initialize any defaults.
	 *  * Rejects with an Error if something went wrong. */
	protected abstract populateFieldDefaults(form: FormEasy): Promise<void>;

	/** For subclasses to implement, to populate the fields.
	 *  * Resolves with the name for setting the h1.
	 *  * Rejects with an Error if something went wrong. */
	protected abstract populateFieldValues(typeId: string | undefined, form: FormEasy): Promise<string | undefined>;

	/** When the edit form is submitted, this is called to save it.
	 *  * Resolves with the id of the thing created or edited.
	 *  * Rejects with an Error if something went wrong, which will be shown in the form. */
	protected abstract save(typeId: string | undefined, form: FormEasy, values: FormValues): Promise<string | undefined>;

	/** Initializes the popup. It never rejects. */
	protected async initialize(): Promise<void> {

		// Get the form.
		const form = this.queryComponent('.window > .FormEasy', FormEasy);

		// Disable it.
		form.setEnabled(false);

		// Populate the field defaults.
		try {
			await this.populateFieldDefaults(form);
		}
		catch (error) {
			this.insertHtml(`<p>${(error as Error).message}</p>`, this.query('.window', Element), this.query('h1', Element));
			throw error;
		}

		// If there is a typeId, then it is in edit mode, so populate the form with the existing values.
		if (this._editing) {

			// Populate the fields & get the name.
			let name;
			try {
				name = await this.populateFieldValues(this._typeId, form);
			}
			catch (error) {
				this.insertHtml(`<p>${(error as Error).message}</p>`, this.query('.window', Element), this.query('h1', Element));
				throw error;
			}

			// Set the name in the title.
			if (name !== undefined) {
				this.query('.window > h1 > .name', Element).innerHTML = name;
			}
		}

		// Make sure to scroll to the top.
		this.root.scrollTop = 0;

		// Enable the form.
		form.setEnabled(true);
	}

	/** When the form is submitted. */
	protected async onSubmitted(form: FormEasy, values: FormValues): Promise<string> {

		// Disable the form.
		form.setEnabled(false);

		// Do the save.
		let typeId;
		try {
			typeId = await this.save(this._typeId, form, values);
		}
		catch (error) {
			form.setEnabled(true);
			return (error as Error).message;
		}

		// Call the saved callback.
		this._onSaved?.(typeId, this._typeId !== undefined);

		// Remove the popup.
		this.app.closeOverlay(this);

		return '';
	}

	/** The form was canceled, so remove this window. */
	onCanceled(): void {

		// Remove the popup.
		this.app.closeOverlay(this);
	}

	/** The type. */
	protected readonly type: string;

	/** Are we editing? (False if we're creating). */
	private _editing: boolean;

	/** The id. */
	private _typeId: string | undefined;

	/** The saved callback. */
	private _onSaved: ((id: string | undefined, wasEditing: boolean) => void) | undefined;

	protected static override css = css;
}
