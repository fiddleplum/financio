import { FormCheckbox, FormEasy, FormValues, FormInput, FormSelect, Params } from '@orourley/elm-app';
import { Rule } from '@orourley/financio-types';
import { AccountChooser, CategoryChooser, FormEntry } from '../internal';
import { ItemEditPopup } from './item-edit-popup';

import css from './rule-edit-popup.css';

export class RuleEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Rule', 'rules', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return `
			<h2>Match</h2>
			<FormEntry name="reviewedMatch" label="Reviewed Match"><FormSelect value="no">
				<option value="">Yes or No</option>
				<option value="no">No</option>
				<option value="yes">Yes</option>
			</FormSelect></FormEntry>
			<FormEntry name="searchMatch" label="Search Match"><FormInput></FormInput></FormEntry>
			<FormEntry name="amountMatch" label="Amount Match"><FormInput></FormInput></FormEntry>
			<h2>Apply</h2>
			<FormEntry name="categoryId" label="Category to Apply"><CategoryChooser showAdd></CategoryChooser></FormEntry>
			<FormEntry name="accountId" label="Account to Apply"><AccountChooser showAdd></AccountChooser></FormEntry>
			<FormEntry name="notes" label="Notes to Append"><FormInput></FormInput></FormEntry>
			<FormEntry name="reviewed" label="Mark as Reviewed"><FormSelect value="yes">
				<option value=""></option>
				<option value="no">No</option>
				<option value="yes">Yes</option>
			</FormSelect></FormEntry>
			<FormEntry name="onlyOnAccountId" label="Only on Account"><AccountChooser value="">
				<option value="">*Any*</option>
			</AccountChooser></FormEntry>
			<FormEntry name="enabled" label="Enabled"><FormCheckbox checked></FormCheckbox></FormEntry>`;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(_form: FormEasy): Promise<void> {
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(_id: string, rule: Rule, form: FormEasy): Promise<void> {

		// Set the values.
		form.getEntries().queryComponent('.reviewedMatch', FormEntry).getInput(FormSelect).value = rule.reviewedMatch !== undefined ? (rule.reviewedMatch ? 'yes' : 'no') : '';
		form.getEntries().queryComponent('.searchMatch', FormEntry).getInput(FormInput).value = rule.searchMatch ?? '';
		form.getEntries().queryComponent('.amountMatch', FormEntry).getInput(FormInput).value = rule.amountMatch?.toString() ?? '';
		form.getEntries().queryComponent('.categoryId', FormEntry).getInput(CategoryChooser).value = rule.categoryId ?? '';
		form.getEntries().queryComponent('.accountId', FormEntry).getInput(AccountChooser).value = rule.accountId ?? '';
		form.getEntries().queryComponent('.notes', FormEntry).getInput(FormInput).value = rule.notes ?? '';
		form.getEntries().queryComponent('.reviewed', FormEntry).getInput(FormSelect).value = rule.reviewed !== undefined ? (rule.reviewed ? 'yes' : 'no') : '';
		form.getEntries().queryComponent('.onlyOnAccountId', FormEntry).getInput(AccountChooser).value = rule.onlyOnAccountId ?? '';
		form.getEntries().queryComponent('.enabled', FormEntry).getInput(FormCheckbox).value = rule.enabled;
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(values: FormValues): Record<string, any> {

		// Get the fields.
		const reviewedMatchString = values['reviewedMatch'] as string;
		const searchMatch = values['searchMatch'] as string;
		const amountMatch = values['amountMatch'] as string;
		const categoryId = values['categoryId'] as string;
		const accountId = values['accountId'] as string;
		const notes = values['notes'] as string;
		const reviewedString = values['reviewed'] as string;
		const onlyOnAccountId = values['onlyOnAccountId'] as string;
		const enabled = values['enabled'] as boolean;

		// Parse the reviewed match value as a boolean.
		const reviewedMatch = reviewedMatchString === '' ? undefined : (reviewedMatchString === 'yes');

		// Parse the amount match value as a number.
		const amountMatchNumber = amountMatch !== '' ? Number.parseFloat(amountMatch.replace(/[^0-9.+-]+/gu, '')) : undefined;
		if (amountMatchNumber !== undefined && isNaN(amountMatchNumber)) {
			throw new Error('Please make the amount from field a number, without units');
		}

		// Parse the reviewed match value as a boolean.
		const reviewed = reviewedString === '' ? undefined : (reviewedString === 'yes');

		// Return the values.
		return {
			reviewedMatch: reviewedMatch,
			searchMatch: searchMatch !== '' ? searchMatch : undefined,
			amountMatch: amountMatchNumber,
			categoryId: categoryId !== '' ? categoryId : undefined,
			accountId: accountId !== '' ? accountId : undefined,
			notes: notes !== '' ? notes : undefined,
			reviewed: reviewed,
			onlyOnAccountId: onlyOnAccountId !== '' ? onlyOnAccountId : undefined,
			enabled: enabled
		};
	}

	protected static override css = css;
}
