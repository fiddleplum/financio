import { DropDown, FormBaseInput, Params } from '@orourley/elm-app';
import { YMD } from '@orourley/financio-types';
import { Calendar } from '../internal';

import html from './date-chooser.html';
import css from './date-chooser.css';

/** A generic date chooser. */
export class DateChooser extends FormBaseInput<string> {

	/** The changed callback. */
	onChanged: ((dateChooser: DateChooser) => void) | undefined;

	constructor(params: Params) {
		super(params);

		// Get the elements.
		this._inputElem = this.query(':scope > input', HTMLInputElement);

		// Set the name and id.
		this._inputElem.name = params.attributes.get('name') ?? '';
		this._inputElem.id = params.attributes.get('id') ?? '';

		// Get the value attribute, if any.
		const value = params.attributes.get('value');
		if (value !== undefined && value !== '') {
			this._inputElem.value = value;
			this._date = new YMD(value);
		}

		// Get the event callbacks.
		this.onChanged = params.eventHandlers.get('changed');
	}

	/** Gets the name. */
	get name(): string {
		return this._inputElem.name;
	}

	/** Sets the name. */
	set name(name: string) {
		this._inputElem.name = name;
	}

	/** Gets the id. */
	get id(): string {
		return this._inputElem.id;
	}

	/** Sets the id. */
	set id(id: string) {
		this._inputElem.id = id;
	}

	/** Gets the value. */
	get value(): string {
		return this._inputElem.value;
	}

	/** Sets the value. */
	set value(value: string) {
		this._inputElem.value = value;
		this._date = new YMD(value);
	}

	/** Focuses the input. */
	focus(): void {
		this._inputElem.focus();
	}

	/** Gets the date. */
	get date(): YMD {
		return this._date;
	}

	/** Sets the date. */
	set date(date: YMD) {
		this._date.copy(date);
		this._inputElem.value = this._date.toString();
	}

	protected _onDateSelected(date: YMD): void {
		this.date = date;
		this.app.closeOverlay(this._calendarDropDown!);
		this._calendarDropDown = undefined;
		if (this.onChanged) {
			this.onChanged(this);
		}
	}

	protected _toggleCalendar(): void {
		if (this._calendarDropDown) {
			this.app.closeOverlay(this._calendarDropDown);
			this._calendarDropDown = undefined;
		}
		else {
			this._calendarDropDown = this.app.openOverlay<DropDown>('<DropDown onClosed="_calendarDropDownClosed"><Calendar onclick="_onDateSelected"/></DropDown>', this);
			this._calendarDropDown.setRelativeTo(this._inputElem);
			const calendar = this._calendarDropDown.queryComponent('.Calendar', Calendar);
			calendar.select(this._date);
		}
	}

	protected _calendarDropDownClosed(): void {
		this._calendarDropDown = undefined;
	}

	/** On the date input key down event. */
	protected _onDateInputKeyDown(event: KeyboardEvent): void {
		const input = event.target as HTMLInputElement;
		if (input.selectionStart === null) {
			return;
		}
		const cursor = input.selectionStart;
		if (input.selectionEnd !== cursor) {
			return;
		}
		if (event.key === 'Delete') {
			if (input.value[cursor] === '-') {
				input.selectionStart++;
			}
		}
		else if (event.key === 'Backspace') {
			if (cursor > 0 && input.value[cursor - 1] === '-') {
				input.selectionEnd--;
			}
		}
		else if (event.key === 'ArrowRight') {
			if (cursor === 3 || cursor === 6) {
				input.selectionStart++;
			}
		}
		else if (event.key === 'ArrowLeft') {
			if (cursor === 5 || cursor === 8) {
				input.selectionEnd--;
			}
		}
		this._onDateInputInput(event);
	}

	/** On the date input changing content. */
	private _onDateInputInput(event: InputEvent | KeyboardEvent): void {
		const input = event.target as HTMLInputElement;
		if (input.selectionStart === null) {
			return;
		}

		// Adjust the value and selection to be formatted correctly.
		let cursor = input.selectionStart;
		const value = [...input.value];
		for (let i = 0; i < value.length; i++) {
			if ((0 <= i && i <= 3) || (5 <= i && i <= 6) || (8 <= i && i <= 9)) {
				const c = value[i]!;
				if (c < '0' || '9' < c) {
					value.splice(i, 1);
					if (cursor > i) {
						cursor--;
					}
					i--;
				}
			}
			else if (i === 4 || i === 7) {
				if (value[i] === '-' && i === value.length - 1) { // If there is a dash on the last one, remove it.
					value.splice(i, 1);
					if (cursor > i) {
						cursor--;
					}
					i--;
				}
				else if (value[i] !== '-') {
					value.splice(i, 0, '-');
					if (cursor >= i) {
						cursor++;
					}
					i++;
				}
			}
		}
		input.value = value.join('');
		input.selectionEnd = cursor;

		// Update the date value.
		try {
			const date = new YMD(input.value);
			this._date.copy(date);
		}
		catch {
			this._date.year = Number.NaN;
			this._date.month = Number.NaN;
			this._date.day = Number.NaN;
		}

		// Update the drop down, if needed.
		if (this._calendarDropDown) {
			const calendar = this._calendarDropDown.queryComponent('.Calendar', Calendar);
			calendar.select(this._date);
		}

		// Do the callback.
		if (this.onChanged) {
			this.onChanged(this);
		}
	}

	/** The displayed date. */
	private _date = new YMD();

	/** The input element. */
	private _inputElem: HTMLInputElement;

	/** The calendar drop down. */
	private _calendarDropDown: DropDown | undefined;

	protected static override html = html;
	protected static override css = css;
}
