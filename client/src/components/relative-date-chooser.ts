import { FormBaseInput, FormCheckbox, FormInput, FormSelect, Params } from '@orourley/elm-app';
import { DateChooser } from '../internal';

import html from './relative-date-chooser.html';
import css from './relative-date-chooser.css';
import { RelativeDate } from '@orourley/financio-types';

/** A generic date chooser. */
export class RelativeDateChooser extends FormBaseInput<string> {

	constructor(params: Params) {
		super(params);

		// Get the elements.
		this._hiddenInput = this.queryComponent('.hiddenInput.FormInput', FormInput);
		this._listFormSelect = this.queryComponent('.list.FormSelect', FormSelect);

		// Set the name and id.
		this._hiddenInput.name = params.attributes.get('name') ?? '';
		this._listFormSelect.id = params.attributes.get('id') ?? '';

		// Get the value attribute, if any.
		this._hiddenInput.value = params.attributes.get('value') ?? ',,,';
		this.updateFieldsFromValue();

		// Set the predefined options.
		let html = `<option value="custom">Custom</option>`;
		for (const entry of RelativeDateChooser.predefinedOptions) {
			html += `<option value="${entry.value}">${entry.label}</option>`;
		}
		this._listFormSelect.setOptionsHtml(html, this);
	}

	/** Gets the name. */
	get name(): string {
		return this._hiddenInput.name;
	}

	/** Sets the name. */
	set name(name: string) {
		this._hiddenInput.name = name;
	}

	/** Gets the id. */
	get id(): string {
		return this._listFormSelect.id;
	}

	/** Sets the id. */
	set id(id: string) {
		this._listFormSelect.id = id;
	}

	/** Gets the value. */
	get value(): string {
		return this._hiddenInput.value;
	}

	/** Sets the value. */
	set value(value: string) {
		this._hiddenInput.value = value;
		this.updateFieldsFromValue();
	}

	/** Focuses the input. */
	focus(): void {
		this._listFormSelect.focus();
	}

	/** Updates all of the fields based on the value of the input. */
	private updateFieldsFromValue(): void {

		// See if it's in the list already. If so, select it and hide the custom fields.
		const listComponent = this.queryComponent('.list', FormSelect);
		if (RelativeDateChooser.predefinedOptions.find((entry) => entry.value === this._hiddenInput.value)) {
			listComponent.value = this._hiddenInput.value;
			this.setCustomFieldsVisibility(false);
			return;
		}

		// Show the custom fields.
		this.setCustomFieldsVisibility(true);

		// Get the relative date from the input.
		const relativeDate = RelativeDate.fromString(this.value);

		// Set the absolute part.
		this.queryComponent('.relativeToToday .FormCheckbox', FormCheckbox).value = relativeDate.absolute === undefined;
		if (relativeDate.absolute !== undefined) {
			this.queryComponent('.absolute .DateChooser', DateChooser).value = relativeDate.absolute;
		}

		// Set the offset part.
		this.queryComponent('.offset .FormInput', FormInput).value = relativeDate.offset !== undefined ? relativeDate.offset.toFixed(0) : '';

		// Set the period part.
		const periodComponent = this.queryComponent('.period .FormSelect', FormSelect);
		periodComponent.value = relativeDate.period ?? 'days';

		// Set the align part.
		const alignComponent = this.queryComponent('.align .FormSelect', FormSelect);
		alignComponent.value = relativeDate.align ?? '';

		// Otherwise, fill in the custom fields.
		listComponent.value = 'custom';
	}

	/** When the list is selected, updates the value and opens/closes the custom fields. */
	updateValueFromFields(): void {
		const listComponent = this.queryComponent('.list', FormSelect);

		// If it's an item in the list (not custom), make that the value, and hide the custom fields.
		if (listComponent.value !== 'custom') {
			this._hiddenInput.value = listComponent.value;
			this.setCustomFieldsVisibility(false);
			return;
		}

		// Set the custom fields to true.
		this.setCustomFieldsVisibility(true);

		// Get the absolute checked value.
		const relativeToToday = this.queryComponent('.relativeToToday .FormCheckbox', FormCheckbox).value;

		// Get the different parts of the relative date.
		const absolute = relativeToToday ? '' : this.queryComponent('.absolute .DateChooser', DateChooser).value;
		const offset = this.queryComponent('.offset .FormInput', FormInput).value;
		const period = this.queryComponent('.period .FormSelect', FormSelect).value;
		const align = this.queryComponent('.align .FormSelect', FormSelect).value;

		// Disable the absolute date if it isn't checked.
		this.query('.entry.absolute', Element).classList.toggle('disabled', relativeToToday);

		// Set the input.
		this._hiddenInput.value = `${absolute},${offset},${period},${align}`;
	}

	/** Sets the visibility of the custom fields. */
	private setCustomFieldsVisibility(visible: boolean): void {
		this.query('.custom', Element).classList.toggle('hidden', !visible);
	}

	/** The hidden input. */
	private _hiddenInput: FormInput;

	/** The list form select component. */
	private _listFormSelect: FormSelect;

	/** The predefined options. */
	private static predefinedOptions = [
		{ value: ',,,', label: 'Today' },
		{ value: ',-1,weeks,', label: '1 week ago' },
		{ value: ',-2,weeks,', label: '2 weeks ago' },
		{ value: ',-1,months,', label: '1 month ago' },
		{ value: ',-1,quarters,', label: '1 quarter ago' },
		{ value: ',-1,years,', label: '1 year ago' },
		{ value: ',0,weeks,beginning', label: 'Beginning of this week' },
		{ value: ',0,weeks,ending', label: 'Ending of this week' },
		{ value: ',0,months,beginning', label: 'Beginning of this month' },
		{ value: ',0,months,ending', label: 'Ending of this month' },
		{ value: ',0,quarters,beginning', label: 'Beginning of this quarter' },
		{ value: ',0,quarters,ending', label: 'Ending of this quarter' },
		{ value: ',0,years,beginning', label: 'Beginning of this year' },
		{ value: ',0,years,ending', label: 'Ending of this year' },
		{ value: ',-1,days,', label: 'Yesterday' },
		{ value: ',-1,weeks,beginning', label: 'Beginning of last week' },
		{ value: ',-1,weeks,ending', label: 'Ending of last week' },
		{ value: ',-1,months,beginning', label: 'Beginning of last month' },
		{ value: ',-1,months,ending', label: 'Ending of last month' },
		{ value: ',-1,quarters,beginning', label: 'Beginning of last quarter' },
		{ value: ',-1,quarters,ending', label: 'Ending of last quarter' },
		{ value: ',-1,years,beginning', label: 'Beginning of last year' },
		{ value: ',-1,years,ending', label: 'Ending of last year' },
		{ value: ',+1,days,', label: 'Tomorrow' },
		{ value: ',+1,weeks,beginning', label: 'Beginning of next week' },
		{ value: ',+1,weeks,ending', label: 'Ending of next week' },
		{ value: ',+1,months,beginning', label: 'Beginning of next month' },
		{ value: ',+1,months,ending', label: 'Ending of next month' },
		{ value: ',+1,quarters,beginning', label: 'Beginning of next quarter' },
		{ value: ',+1,quarters,ending', label: 'Ending of next quarter' },
		{ value: ',+1,years,beginning', label: 'Beginning of next year' },
		{ value: ',+1,years,ending', label: 'Ending of next year' }
	];

	protected static override html = html;
	protected static override css = css;
}
