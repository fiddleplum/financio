import { FormEasy, FormValues, Params, FormList, FormSelect } from '@orourley/elm-app';
import { Project } from '@orourley/financio-types';
import { FormEntry } from '../internal';
import { UserChooser } from './user-chooser';
import { ItemEditPopup } from './item-edit-popup';

import css from './project-edit-popup.css';

export class ProjectEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Project', 'projects', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return `
			<FormEntry name="defaultUnitName" label="Default Unit">
				<FormInput pattern=".+"></FormInput>
				<article class="tip">
					<p>The unit will be placed after the amount in any transaction that contains this account, such as 1024.00 USD or 23.45 shares FORD.</p>
					<p>You will be able to choose other units for specific accounts, but this will be the default selection when making new accounts.</p>
				</article>
			</FormEntry>
			<FormEntry class="roles" label="Members">
				<FormList nograb>
					<UserChooser name="roles[+].username"></UserChooser>
					<FormSelect name="roles[].role" value="1">
						<option value="1">Dashboard</option>
						<option value="2">Viewer</option>
						<option value="3">Bookkeeper</option>
						<option value="4">Accountant</option>
						<option value="5">Owner</option>
					</FormSelect>
				</FormList>
				<article class="tip">
					<p>This is a list of members for the project. You can add and remove members or change their roles.</p>
					<p>The roles are as follows:</p>
					<ul>
						<li>Dashboard - The user can view the main project dashboard, but nothing else.</li>
						<li>Viewer - The user can view everything, but cannot change anything.</li>
						<li>Bookkeeper - The user can only update transactions and rules.</li>
						<li>Accountant - The user can update everything except the main project settings.</li>
						<li>Owner - The user can change anything within the project.</li>
					</ul>
					<p>Note that only an admin can create or delete a project, but the owner can still add and remove members and set their roles.</p>
				</article>
			</FormEntry>`;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(form: FormEasy): Promise<void> {

		// Set up an initial default of this user with Owner status.
		if (this.app.username !== undefined) {
			const formListEntry = form.getEntries().queryComponent('.roles', FormEntry).getInput(FormList).add();
			formListEntry.queryComponent('.UserChooser', UserChooser).value = this.app.username;
			formListEntry.queryComponent('.FormSelect', FormSelect).value = '5';
		}
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(id: string, project: Project, form: FormEasy): Promise<void> {

		// Add in a default unit chooser since we have a projectId.
		form.getEntries().insertHtml(`
			<FormEntry name="defaultUnitId" label="Default Unit">
				<UnitChooser value="${project.defaultUnitId}" projectId="${id}"></UnitChooser>
				<article class="tip">
					<p>The unit will be placed after the amount in any transaction that contains this account, such as 1024.00 USD or 23.45 shares FORD.</p>
					<p>You will be able to choose other units for specific accounts, but this will be the default selection when making new accounts.</p>
				</article>
			</FormEntry>`, undefined, form.getEntries().queryComponent('.defaultUnitName', FormEntry));

		// Remove the defaultUnitName FormEntry, since we'll be using a chooser.
		form.getEntries().removeComponent(form.getEntries().queryComponent('.defaultUnitName', FormEntry));

		// Set the roles.
		const formList = form.getEntries().queryComponent('.roles', FormEntry).getInput(FormList);
		formList.remove(formList.getFirstItem()!); // Removes the single default role.
		for (const [username, role] of Object.entries(project.roles)) {
			const formListEntry = formList.add();
			formListEntry.queryComponent('.UserChooser', UserChooser).value = username;
			formListEntry.queryComponent('.FormSelect', FormSelect).value = `${role}`;
		}
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(values: FormValues): Record<string, any> {

		// Get the default unit name.
		const defaultUnitName = values['defaultUnitName'] as string | undefined;

		// Get the default unit id.
		const defaultUnitId = values['defaultUnitId'] as string | undefined;

		// Convert the roles on the form to a roles object.
		const roles = Object.fromEntries((values['roles'] as { username: string; role: string; }[])
			.map((entry) => [entry.username, parseInt(entry.role)]));

		return {
			...(defaultUnitName !== undefined && { defaultUnitName }),
			...(defaultUnitId !== undefined && { defaultUnitId }),
			roles
		};
	}

	protected static override css = css;
}
