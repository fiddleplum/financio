import { FormEasy, FormValues, FormSelect, Params, FormCheckbox } from '@orourley/elm-app';
import { Report } from '@orourley/financio-types';
import { AccountChooser, CategoryChooser, FormEntry, RelativeDateChooser, UnitChooser } from '../internal';
import { ItemEditPopup } from './item-edit-popup';

import css from './report-edit-popup.css';

export class ReportEditPopup extends ItemEditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Report', 'reports', params);
	}

	/** Gets any other form entries to put in after the base item elements. */
	protected getExtraFormEntriesHtml(): string {
		return `
			<FormEntry name="style" label="Style"><FormSelect value="income-statement">
				<option value="incomeStatement">Income Statement</option>
				<option value="balanceSheet">Balance Sheet</option>
			</FormSelect></FormEntry>
			<FormEntry name="startDate" label="Start Date"><RelativeDateChooser value=",-1,years,"></RelativeDateChooser></FormEntry>
			<FormEntry name="endDate" label="End Date"><RelativeDateChooser value=",-1,months,ending"></RelativeDateChooser></FormEntry>
			<FormEntry name="periodIntervals" label="Column Intervals"><FormSelect value="months">
				<option value="days">Days</option>
				<option value="weeks">Weeks</option>
				<option value="months">Months</option>
				<option value="quarters">Quarters</option>
				<option value="years">Years</option>
			</FormSelect></FormEntry>
			<FormEntry name="categoryIds" label="Categories"><CategoryChooser selectGroups="also"></CategoryChooser></FormEntry>
			<FormEntry name="accountIds" label="Accounts"><AccountChooser selectGroups="also"></AccountChooser></FormEntry>
			<FormEntry name="unitId" label="Unit"><UnitChooser></UnitChooser></FormEntry>
			<FormEntry name="useEndOfDay" label="Use End Of Day"><FormCheckbox value="false"></FormCheckbox></FormEntry>`;
	}

	/** Populates the form with any default values when we're creating. */
	protected async populateExtraFieldDefaults(_form: FormEasy): Promise<void> {
	}

	/** Populates the form with any extra values. */
	protected async populateExtraFieldValues(_id: string, report: Report, form: FormEasy): Promise<void> {

		// Set the values.
		form.getEntries().queryComponent('.startDate', FormEntry).getInput(RelativeDateChooser).value = report.startDate;
		form.getEntries().queryComponent('.endDate', FormEntry).getInput(RelativeDateChooser).value = report.endDate;
		form.getEntries().queryComponent('.style', FormEntry).getInput(FormSelect).value = report.style;
		form.getEntries().queryComponent('.periodIntervals', FormEntry).getInput(FormSelect).value = report.periodIntervals;

		// Select the category options.
		if (report.style === 'incomeStatement') {
			if (report.categoryIds) {
				const categoryChooser = form.getEntries().queryComponent('.categoryIds', FormEntry).getInput(CategoryChooser);
				for (const categoryId of report.categoryIds) {
					categoryChooser.value = categoryId;
				}
			}
		}

		// Select the account options.
		if (report.style === 'balanceSheet') {
			if (report.accountIds) {
				const accountChooser = form.getEntries().queryComponent('.accountIds', FormEntry).getInput(AccountChooser);
				for (const accountId of report.accountIds) {
					accountChooser.value = accountId;
				}
			}
			form.getEntries().queryComponent('.useEndOfDay', FormEntry).getInput(FormCheckbox).value = report.useEndOfDay;
		}

		if (report.unitId !== undefined) {
			form.getEntries().queryComponent('.unitId', FormEntry).getInput(UnitChooser).value = report.unitId;
		}
	}

	/** Gets any extra form values for sending to the server. */
	protected getExtraFormValues(values: FormValues): Record<string, any> {

		// Get the fields.
		const style = values['style'] as string;
		const startDate = values['startDate'] as string;
		const endDate = values['endDate'] as string;
		const periodIntervals = values['periodIntervals'] as 'days' | 'weeks' | 'months' | 'quarters' | 'years';
		let categoryIds: string[] | undefined = (values['categoryIds'] as string).split(';;;');
		let accountIds: string[] | undefined = (values['accountIds'] as string).split(';;;');
		const unitId = values['unitId'] !== '' ? values['unitId'] as string : undefined;
		const useEndOfDay = values['useEndOfDay'] as boolean;

		// If there are no categories selected, select them all.
		if (categoryIds.length === 1 && categoryIds[0] === '') {
			categoryIds = undefined;
		}

		// If there are no accounts selected, select them all.
		if (accountIds.length === 1 && accountIds[0] === '') {
			accountIds = undefined;
		}

		// Return the values.
		return {
			startDate,
			endDate,
			periodIntervals,
			categoryIds,
			accountIds,
			style,
			unitId,
			useEndOfDay
		};
	}

	protected static override css = css;
}
