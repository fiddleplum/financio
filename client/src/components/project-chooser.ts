import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class ProjectChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Project', 'projects', params);
	}
}
