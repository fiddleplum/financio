import { Params } from '@orourley/elm-app';
import { DeletePopup } from './delete-popup';
import { Project } from '@orourley/financio-types';

export class ProjectDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Project', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the project.
		const project = await this.app.send<Project>('projects', 'getItem', {
			id
		});

		// Return the name.
		return project.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('projects', 'deleteItemOrGroup', {
			id
		});
	}
}
