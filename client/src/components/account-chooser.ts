import { Params } from '@orourley/elm-app';
import { ItemChooser } from './item-chooser';

export class AccountChooser extends ItemChooser {

	/** Constructor. */
	constructor(params: Params) {
		super('Account', 'accounts', params);
	}
}
