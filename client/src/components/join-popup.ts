import { Transaction } from '@orourley/financio-types';
import { FormEasy, Params } from '@orourley/elm-app';

import html from './join-popup.html';
import css from './join-popup.css';
import { TransactionList } from './transaction-list';
import { FinancioPopup } from '../financio-popup';
import { TransactionView } from './transaction-view';

export class JoinPopup extends FinancioPopup {

	/** The created callback. */
	private onSubmitted: ((updatedTransactionid: string, deletedTransactionId: string) => void) | undefined;

	/** Constructor. */
	constructor(params: Params) {
		super(params);

		// Get the event handlers.
		this.onSubmitted = params.eventHandlers.get('submitted');

		// Set the content of the window.
		this.setHtml(html, this.query('.window', Element), this);
	}

	/** Sets the transaction to be merged into. */
	async setTransaction(transaction: Transaction): Promise<void> {

		// Save the transaction.
		this._transaction = transaction;

		// Get the form.
		const form = this.queryComponent('.FormEasy', FormEasy);

		// Disable it.
		form.setEnabled(false);

		// Set the transaction filter.
		const transactionList = form.getEntries().queryComponent('.TransactionList', TransactionList);
		await transactionList.setFilter({
			startDate: `${this._transaction.dateFrom},${this._transaction.accountIdTo !== '' ? -14 : 0},,`,
			endDate: `${this._transaction.dateTo},${this._transaction.accountIdFrom !== '' ? +14 : 0},,`,
			reviewed: false,
			minAmount: Math.min(this._transaction.amountFrom, this._transaction.amountTo),
			maxAmount: Math.max(this._transaction.amountFrom, this._transaction.amountTo),
			accountId1: this._transaction.accountIdFrom,
			accountId2: this._transaction.accountIdTo,
			excludeAccounts: true
		});

		// Enable the form.
		form.setEnabled(true);
	}

	/** Called when the transaction list has been updated. */
	onTransactionsUpdated(): void {

		// Add a click handler to every transaction view.
		const form = this.queryComponent('.FormEasy', FormEasy);
		const transactionList = form.getEntries().queryComponent('.TransactionList', TransactionList);
		const transactionViews = transactionList.getTransactionViews();
		for (const transactionView of transactionViews) {
			transactionView.setClickable(this.transactionSelected.bind(this, transactionView));
		}
	}

	/** When the user seleects a transaction. */
	transactionSelected(transactionView: TransactionView): void {

		// Update the selected class.
		this._selectedTransactionView?.setClass('selected', false);
		this._selectedTransactionView = transactionView;
		this._selectedTransactionView.setClass('selected', true);
	}

	/** The form was canceled, so call the callback. */
	onFormCanceled(): void {
		this.app.closeOverlay(this);
	}

	protected async onFormSubmitted(form: FormEasy): Promise<string> {

		// Check that a transaction was set.
		if (!this._transaction) {
			return '';
		}

		// Check that a transaction was selected.
		if (!this._selectedTransactionView) {
			return 'Please select a transaction.';
		}

		try {

			const transactionList = form.getEntries().queryComponent('.TransactionList', TransactionList);

			// Update this transaction with the other transaction..
			const dateFromOld = this._transaction.dateFrom;
			const otherTransactionId = this._selectedTransactionView.getTransaction()?.id;
			const otherTransaction = transactionList.getTransactions().find((transaction) => transaction.id === otherTransactionId)!;
			if (this._transaction.accountIdFrom === '') {
				this._transaction.accountIdFrom = otherTransaction.accountIdFrom;
				this._transaction.dateFrom = otherTransaction.dateFrom;
				this._transaction.amountFrom = otherTransaction.amountFrom;
				this._transaction.institutionIdFrom = otherTransaction.institutionIdFrom;
			}
			else if (this._transaction.accountIdTo === '') {
				this._transaction.accountIdTo = otherTransaction.accountIdTo;
				this._transaction.dateTo = otherTransaction.dateTo;
				this._transaction.amountTo = otherTransaction.amountTo;
				this._transaction.institutionIdTo = otherTransaction.institutionIdTo;
			}
			this._transaction.description += `, ${otherTransaction.description}`;
			await this.app.send('transactions', 'update', {
				projectId: this.app.projectId,
				transactions: [{
					dateFromOld: dateFromOld,
					transaction: this._transaction
				}]
			});

			// Delete the other transaction.
			await this.app.send('transactions', 'delete', {
				projectId: this.app.projectId,
				id: otherTransactionId,
				dateFrom: otherTransaction.dateFrom
			});

			// Close this popup.
			this.app.closeOverlay(this);

			// Call the created callback.
			if (this.onSubmitted) {
				this.onSubmitted(this._transaction.id, otherTransaction.id);
			}
		}
		catch (error) {
			return (error as Error).message;
		}

		return '';
	}

	private _transaction: Transaction | undefined;

	private _selectedTransactionView: TransactionView | undefined;

	protected static override css = css;
}
