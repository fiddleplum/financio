import { FormEasy, FormValues, Params } from '@orourley/elm-app';
import { FinancioPopup } from '../financio-popup';

import html from './delete-popup.html';
import css from './delete-popup.css';

/** A delete popup.
 *
 *  Attributes:
 *  * type - The type of object to delete. It will appear in the header.
 *  * typeId - The id of the object to delete. It is passed into the delete callback.
 *  * name - The name of the object to delete, used during verification.
 *  * onDeleted - An optional callback to call when the deleting is done, after this popup has closed. */
export abstract class DeletePopup extends FinancioPopup {

	/** Constructor. */
	constructor(type: string, params: Params) {
		super(params);

		// Get the id.
		const typeId = params.attributes.get('typeid');
		if (typeId === undefined) {
			throw new Error('You must specify a typeId for the delete popup.');
		}
		this._typeId = typeId;

		// Get the deleted callback.
		this._onDeleted = params.eventHandlers.get('deleted');

		// Set the content of the window.
		this.setHtml(html, this.query('.window', Element), this);

		// Set the type in the h1.
		const form = this.queryComponent('.window > .FormEasy', FormEasy);
		form.getEntries().query('h1 > .type', Element).innerHTML = type;

		// Wait one 'frame' to initialize. This is so a sub class construct can complete.
		void Promise.resolve().then(() => this.initialize());
	}

	/** For subclasses to implement, to get the name of the type.
	 *  * Resolves with the name for setting the h1 and verification.
	 *  * Rejects with an Error if something went wrong. */
	protected abstract getName(typeId: string): Promise<string>;

	/** When the delete form is submitted, this is called to delete it.
	 *  * Rejects with an Error if something went wrong, which will be shown in the form. */
	protected abstract delete(typeId: string): Promise<void>;

	/** Initializes the form. */
	private async initialize(): Promise<void> {

		// Get the form.
		const form = this.queryComponent('.window > .FormEasy', FormEasy);

		// Disable it.
		form.setEnabled(false);

		// Get the name.
		try {
			this._name = await this.getName(this._typeId);
		}
		catch (error) {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.getWindow());
			return;
		}

		// Set the name in the h1.
		form.getEntries().query('h1 > .name', Element).innerHTML = this._name;

		// Enable the form.
		form.setEnabled(true);
	}

	/** For form was submitted. Check the values and then
	 *  call the callback to actually delete it. */
	protected async onSubmitted(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const name = values['name'] as string;
		const verify = values['verify'] as string;

		if (name !== this._name) {
			return 'You typed the wrong name.';
		}

		if (verify !== 'DELETE') {
			return 'Please enter DELETE to confirm.';
		}

		// Do the delete.
		try {
			await this.delete(this._typeId);
		}
		catch (error) {
			form.setEnabled(true);
			return (error as Error).message;
		}

		// Remove the popup.
		this.app.closeOverlay(this);

		// Call the deleted callback.
		this._onDeleted?.(this._typeId);

		return '';
	}

	/** The form was canceled, so remove this window. */
	onCanceled(): void {

		// Remove the popup.
		this.app.closeOverlay(this);
	}

	/** The id. */
	private _typeId: string;

	/** The name. */
	private _name: string | undefined;

	/** The deleted callback. */
	private _onDeleted: ((id: string) => void) | undefined;

	protected static override css = css;
}
