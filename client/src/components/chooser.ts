import { FormBaseInput, FormSelect, Params } from '@orourley/elm-app';

import html from './chooser.html';
import css from './chooser.css';

export abstract class Chooser extends FormBaseInput<string> {

	/** Called when the user chooses and option. Takes the id and whether or not it is an added option. */
	onChosen: ((id: string, added: boolean) => void) | undefined;

	/** Subclasses implement this, which should return html for the list options. */
	protected abstract addOptions(): Promise<string>;

	/** Constructor. */
	constructor(type: string, params: Params) {
		super(params);

		// Save the type and show add option.
		this._type = type;
		this._showAddOption = params.attributes.has('showadd');
		this._showBlankOption = params.attributes.has('showblank');

		// Save the FormSelect.
		this._formSelect = this.queryComponent(':scope .FormSelect', FormSelect);

		// Save the attributes.
		this._formSelect.name = params.attributes.get('name') ?? '';
		this._formSelect.id = params.attributes.get('id') ?? '';
		this._formSelect.value = params.attributes.get('value') ?? '';

		// Get the event handlers.
		this.onChosen = params.eventHandlers.get('chosen');

		// Save the inner HTMl.
		this._extraOptions = params.innerHtml;

		// Wait one 'frame' to update the display. This is so a sub class construct can complete.
		void Promise.resolve().then(() => this.updateDisplay());
	}

	/** Gets the name. */
	get name(): string {
		return this._formSelect.name;
	}

	/** Sets the name. */
	set name(name: string) {
		this._formSelect.name = name;
	}

	/** Gets the id. */
	get id(): string {
		return this._formSelect.id;
	}

	/** Sets the id. */
	set id(id: string) {
		this._formSelect.id = id;
	}

	/** Focuses the input. */
	focus(): void {
		this._formSelect.focus();
	}

	/** Gets the value. */
	get value(): string {
		return this._formSelect.value;
	}

	/** Sets the value. */
	set value(value: string) {
		this._formSelect.value = value;
	}

	protected setType(type: string) {
		this._type = type;
		this.updateDisplay();
	}

	/** Updates the display. Can be called by subclasses to refresh. */
	protected async updateDisplay(): Promise<void> {

		// Setup the accounts html.
		let html = '';
		if (this._showAddOption) {
			html += `<a href="javascript:;" class="add button" onclick="createPopupShow">Add New ${this._type}</a>`;
		}
		if (this._showBlankOption) {
			html += `<option value="">*Blank*</option>`;
		}
		html += this._extraOptions;
		html += await this.addOptions();

		// Add the options.
		this._formSelect.setOptionsHtml(html, this);
	}

	protected optionChanged(formSelect: FormSelect): void {
		const value = formSelect.value;
		if (this.onChosen) {
			this.onChosen(value, this._added);
		}
	}

	/** Shows the create popup. */
	protected createPopupShow(): void {
		this.app.openOverlay(`<${this._type}EditPopup onSaved="onCreatePopupSaved"></${this._type}EditPopup>`, this);
	}

	/** Called when the create popup has submitted. */
	protected async onCreatePopupSaved(typeId: string): Promise<void> {

		// Refresh the list.
		await this.updateDisplay();

		// Set to added.
		this._added = true;

		// Set the value of the select to the newly created account.
		this._formSelect.value = typeId;
	}

	/** The type of chooser. */
	private _type: string;

	/** The FormSelect. */
	private _formSelect: FormSelect;

	/** Whether or not to show the add option. */
	private _showAddOption: boolean;

	/** Whether or not to show the blank option. */
	private _showBlankOption: boolean;

	/** Inner HTML to prepend to the options. */
	private _extraOptions: string;

	/** Whether or not an option was added. */
	private _added: boolean = false;

	protected static override html = html;
	protected static override css = css;
}
