import { Params } from '@orourley/elm-app';
import { Rule } from '@orourley/financio-types';
import { DeletePopup } from './delete-popup';

export class RuleDeletePopup extends DeletePopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Rule', params);
	}

	protected override async getName(id: string): Promise<string> {

		// Get the rule.
		const rule = await this.app.send<Rule>('rules', 'getItem', {
			projectId: this.app.projectId,
			id: id
		});

		// Return the name.
		return rule.name;
	}

	protected override async delete(id: string): Promise<void> {

		// Send the command.
		await this.app.send('rules', 'deleteItemOrGroup', {
			projectId: this.app.projectId,
			id: id
		});
	}
}
