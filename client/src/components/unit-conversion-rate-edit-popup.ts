import { FormEasy, FormValues, FormInput, Params } from '@orourley/elm-app';
import { DateChooser, EditPopup, FormEntry, UnitChooser } from '../internal';

import css from './unit-conversion-rate-edit-popup.css';

export class UnitConversionRateEditPopup extends EditPopup {

	/** Constructor. */
	constructor(params: Params) {
		super('Unit Conversion Entry', params);

		// Make sure it has an id.
		if (params.attributes.get('typeid') === undefined) {
			throw new Error('The typeid attribute must be present on UnitConversionRateEditPopup.');
		}
	}

	/** Gets the form entries html. */
	protected getFormEntriesHtml(): string {
		return `
			<FormEntry name="otherUnitId" label="Other Unit"><UnitChooser></UnitChooser></FormEntry>
			<FormEntry name="date" label="Date"><DateChooser></DateChooser></FormEntry>
			<FormEntry name="value" label="Conversion Rate"><FormNumberInput></FormNumberInput></FormEntry>
			<p>The conversion rate is the number of other units equal to this unit on the given date.</p>`;
	}

	protected override async populateFieldDefaults(_form: FormEasy): Promise<void> {
	}

	protected override async populateFieldValues(id: string, form: FormEasy): Promise<undefined> {

		// Convert the id to a unitId and possibly otherUnitId and date.
		const idTokens = id.split(',');
		const unitId = idTokens[0]!;

		// If there is an otherUnitId and date, then we're editing an existing entry, so fill things out.
		if (idTokens.length === 3) {
			const otherId = idTokens[1]!;
			const date = idTokens[2]!;

			// Get the conversion rate of the entry.
			const conversionRate = await this.app.send<number | undefined>('units', 'getConversionRate', {
				projectId: this.app.projectId,
				id: unitId,
				otherId: otherId,
				date: date
			});
			if (conversionRate === undefined) {
				throw new Error('The conversion rate was not found.');
			}

			// Set the field values.
			form.getEntries().queryComponent('.date', FormEntry).getInput(DateChooser).value = date;
			form.getEntries().queryComponent('.value', FormEntry).getInput(FormInput).value = `${conversionRate}`;
		}

		// Exclude the unitId from the otherUnitId list.
		await form.getEntries().queryComponent('.otherUnitId', FormEntry).getInput(UnitChooser).setExcludedItem(id, true);
	}

	protected override async save(id: string | undefined, _form: FormEasy, values: FormValues): Promise<string> {

		// Get the values.
		const otherId = values['otherUnitId'] as string;
		const date = values['date'] as string;
		const valueAsString = values['value'] as string;

		// Parse the starting value as a number.
		const value = Number.parseFloat(valueAsString.replace(/[^0-9.+-]+/gu, ''));
		if (isNaN(value)) {
			throw new Error('Please make the value a number, without units.');
		}

		// Send the data to the server.
		try {
			return await this.app.send<string>('units', 'setConversionRate', {
				projectId: this.app.projectId,
				id: id,
				otherId: otherId,
				date: date,
				value: value
			});
		}
		catch (error) {
			throw (error as Error).message;
		}
	}

	protected static override css = css;
}
