/** You must include this in your own project directly, but you can copy it from here. */

declare module '*.html' {
	const content: string;
	export default content;
}

declare module '*.css' {
	const content: string;
	export default content;
}

declare module '*.svg' {
	const content: string;
	export default content;
}
