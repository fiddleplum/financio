import { ConfirmPopup, CustomPopup, DropDown, FormCheckbox, FormEasy, FormFiles, FormInput, FormList, FormMultiSelect, FormNumberInput, FormRadioButton, FormSelect, Icon, OkPopup, UserContent } from '@orourley/elm-app';
import { UserChooser } from './components/user-chooser';
import { FormEntry } from './components/form-entry';
import { AccountChooser } from './components/account-chooser';
import { CategoryChooser } from './components/category-chooser';
import { DateChooser } from './components/date-chooser';
import { RelativeDateChooser } from './components/relative-date-chooser';
import { UnitChooser } from './components/unit-chooser';
import { HierarchyList } from './components/hierarchy-list';
import { AccountDeletePopup } from './components/account-delete-popup';
import { AccountEditPopup } from './components/account-edit-popup';
import { BatchChangePopup } from './components/batch-change-popup';
import { Calendar } from './components/calendar';
import { CategoryDeletePopup } from './components/category-delete-popup';
import { CategoryEditPopup } from './components/category-edit-popup';
import { FilterForm } from './components/filter-form';
import { GroupDeletePopup } from './components/group-delete-popup';
import { GroupEditPopup } from './components/group-edit-popup';
import { JoinPopup } from './components/join-popup';
import { ProjectDeletePopup } from './components/project-delete-popup';
import { ProjectEditPopup } from './components/project-edit-popup';
import { ReportDeletePopup } from './components/report-delete-popup';
import { ReportEditPopup } from './components/report-edit-popup';
import { RuleDeletePopup } from './components/rule-delete-popup';
import { RuleEditPopup } from './components/rule-edit-popup';
import { TransactionList } from './components/transaction-list';
import { TransactionView } from './components/transaction-view';
import { UnitConversionRateDeletePopup } from './components/unit-conversion-rate-delete-popup';
import { UnitConversionRateEditPopup } from './components/unit-conversion-rate-edit-popup';
import { UnitDeletePopup } from './components/unit-delete-popup';
import { UnitEditPopup } from './components/unit-edit-popup';
import { ProjectChooser } from './components/project-chooser';
import { RuleChooser } from './components/rule-chooser';
import { ReportChooser } from './components/report-chooser';
import { SplitPopup } from './components/split-popup';

export const componentTypes = [

	ConfirmPopup,
	CustomPopup,
	DropDown,
	FormCheckbox,
	FormEasy,
	FormFiles,
	FormInput,
	FormList,
	FormMultiSelect,
	FormNumberInput,
	FormRadioButton,
	FormSelect,
	Icon,
	OkPopup,
	UserContent,

	Calendar,
	FormEntry,
	HierarchyList,

	AccountChooser,
	CategoryChooser,
	DateChooser,
	ProjectChooser,
	RelativeDateChooser,
	ReportChooser,
	RuleChooser,
	UnitChooser,
	UserChooser,

	FilterForm,
	TransactionList,
	TransactionView,

	AccountDeletePopup,
	AccountEditPopup,
	CategoryDeletePopup,
	CategoryEditPopup,
	GroupDeletePopup,
	GroupEditPopup,
	ProjectDeletePopup,
	ProjectEditPopup,
	ReportDeletePopup,
	ReportEditPopup,
	RuleDeletePopup,
	RuleEditPopup,
	UnitDeletePopup,
	UnitEditPopup,

	BatchChangePopup,
	JoinPopup,
	SplitPopup,
	UnitConversionRateDeletePopup,
	UnitConversionRateEditPopup
];
