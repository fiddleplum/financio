import { FormCheckbox, FormEasy, FormValues, FormInput, FormSelect, Router } from '@orourley/elm-app';
import { Csv } from '@orourley/pine-lib';
import { Account, Items, Transaction, UnitFile } from '@orourley/financio-types';
import { FinancioPage, FormEntry } from '../internal';
import { displayAmount } from '../utils/display_amount';
import { OFX } from '../utils/ofx';

import html from './transactions-import-page.html';
import css from './transactions-import-page.css';

/** The transactions import page. */
export class TransactionsImportPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Set the subtitle.
		this.app.setSubtitle('Import Transactions');

		// Save the account id.
		const accountId = query['id'];
		if (accountId === undefined) {
			this.setHtml('Account not found.', this.root);
			return;
		}
		this._accountId = accountId;

		// Update the display.
		void this.updateDisplay();
	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the account.
		const account = await this.app.send<Account>('accounts', 'getItem', {
			projectId: this.app.projectId,
			id: this._accountId
		});
		this.account = account;

		// Set the name of the account.
		this.query('.accountName', Element).innerHTML = this.account.name;

		// Update the returnLink href.
		this.query(':scope > .heading > p > .returnLink', HTMLAnchorElement).href += `&projectId=${this.app.projectId}&id=${this.account.id}`;
	}

	protected async chooseFiles(_form: FormEasy, values: FormValues): Promise<string> {

		this.fileList = values['files'] as FileList;

		// Get the first file for determining the extension.
		const firstFile = this.fileList[0];
		if (!firstFile) {
			return 'Please select at least one file.';
		}

		this.extension = firstFile.name.substring(firstFile.name.lastIndexOf('.') + 1).toLowerCase();
		if (this.extension !== 'csv' && this.extension !== 'qfx' && this.extension !== 'ofx') {
			return 'Only .qfx, .ofx, and .csv files are currently supported.';
		}
		for (const file of this.fileList) {
			const fileExtension = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
			if (fileExtension !== this.extension) {
				return 'All files do not have the same extension.';
			}
		}

		// Show the next form.
		this.query('.chooseFiles', Element).classList.add('hidden');
		if (this.extension === 'csv') {
			this.query('.configureCSV', Element).classList.remove('hidden');
			if (this.account.csvImportDefaults) {
				const form = this.queryComponent('.configureCSV .FormEasy', FormEasy);
				form.getEntries().queryComponent('.delim', FormEntry).getInput(FormSelect).value = this.account.csvImportDefaults.delim;
				form.getEntries().queryComponent('.trimFirstLines', FormEntry).getInput(FormInput).value = this.account.csvImportDefaults.trimFirstLines.toString();
				form.getEntries().queryComponent('.trimLastLines', FormEntry).getInput(FormInput).value = this.account.csvImportDefaults.trimLastLines.toString();
				form.getEntries().queryComponent('.hasHeaders', FormEntry).getInput(FormCheckbox).value = this.account.csvImportDefaults.hasHeaders;
			}
		}
		else if (this.extension === 'qfx' || this.extension === 'ofx') {
			this.query('.configureOFX', Element).classList.remove('hidden');
			if (this.account.ofxImportDefaults) {
				const form = this.queryComponent('.configureOFX .FormEasy', FormEasy);
				form.getEntries().queryComponent('.reverseDirection', FormEntry).getInput(FormCheckbox).value = this.account.ofxImportDefaults.reverseDirection;
			}
		}

		return '';
	}

	protected async configureOFX(_form: FormEasy, values: FormValues): Promise<string> {

		const reverseDirection = values['reverseDirection'] as boolean;
		this.account.ofxImportDefaults = {
			reverseDirection
		};

		// Save the CSV import defaults.
		await this.app.send('accounts', 'setOfxImportDefaults', {
			projectId: this.app.projectId,
			id: this.account.id,
			reverseDirection: this.account.ofxImportDefaults.reverseDirection
		});

		this.doOfxImport();

		// Hide this form and show the review form.
		this.query('.configureOFX', Element).classList.add('hidden');
		await this.reviewTransactions();

		return '';
	}

	protected async doOfxImport(): Promise<void> {

		// Go through each file and extract the transactions from the OFX format.
		for (const file of this.fileList!) {
			const text = await file.text();
			this.transactions.push(...OFX.parse(text, this.account.id));
		}

		// Reverse the direction of each transaction if needed.
		if (this.account.ofxImportDefaults?.reverseDirection === true) {
			for (const transaction of this.transactions) {
				const accountIdFrom = transaction.accountIdFrom;
				const institutionIdFrom = transaction.institutionIdFrom;
				transaction.accountIdFrom = transaction.accountIdTo;
				transaction.institutionIdFrom = transaction.institutionIdTo;
				transaction.accountIdTo = accountIdFrom;
				transaction.institutionIdTo = institutionIdFrom;
			}
		}

		// Show the review form.
		await this.reviewTransactions();
	}

	protected async configureCSV(_form: FormEasy, values: FormValues): Promise<string> {

		this.delim = values['delim'] as string;
		const hasHeaders = values['hasHeaders'] as boolean;

		const delimChar = this.delimToChar(this.delim);
		if (delimChar === undefined) {
			return 'Please choose a valid delimiter.';
		}

		// Get the first lines trimming.
		const trimFirstLines = values['trimFirstLines'] as string;
		this.trimFirstLines = parseInt(trimFirstLines);
		if (isNaN(this.trimFirstLines) || this.trimFirstLines < 0) {
			return 'The trim first lines field must be a non-negative integer.';
		}

		// Get the last lines trimming.
		const trimLastLines = values['trimLastLines'] as string;
		this.trimLastLines = parseInt(trimLastLines);
		if (isNaN(this.trimLastLines) || this.trimLastLines < 0) {
			return 'The trim last lines field must be a non-negative integer.';
		}

		// Get the first file for determining the headers.
		const firstFile = this.fileList?.[0];
		if (!firstFile) {
			return 'Please select at least one file.';
		}

		// Get the headers.
		let text;
		try {
			text = await firstFile.text();
		}
		catch {
			return `The file ${firstFile.name} is not a valid text file.`;
		}
		const rows = Csv.parse(text, delimChar);
		rows.splice(0, this.trimFirstLines);
		if (rows.length === 0) {
			return `The file ${firstFile.name} is empty after trimming the first ${this.trimFirstLines} lines.`;
		}
		const firstRow = rows[0]!;
		if (hasHeaders) {
			this.headers = firstRow;
		}

		// Create the FormSelect option HTML.
		let formSelectHtml = '';
		if (this.headers) {
			for (const header of this.headers) {
				formSelectHtml += `<option value="${header}">${header}</option>`;
			}
		}
		else {
			for (let i = 0; i < firstRow.length; i++) {
				formSelectHtml += `<option value="${i}}">${i}</option>`;
			}
		}

		// Populate the configure CSV headers form.
		const configureCSVForm = this.queryComponent('.configureCSVHeaders .FormEasy', FormEasy);
		for (const name of ['institutionId', 'description', 'description2', 'amount', 'amountOther', 'date']) {
			const formSelect = configureCSVForm.getEntries().queryComponent(`.${name}.FormEntry`, FormEntry).getInput(FormSelect);
			formSelect.setOptionsHtml(formSelectHtml, this);
		}

		// Show the next form.
		this.query('.configureCSV', Element).classList.add('hidden');
		this.query('.configureCSVHeaders', Element).classList.remove('hidden');

		// Set the CSV import defaults.
		if (this.account.csvImportDefaults) {
			const form = this.queryComponent('.configureCSVHeaders .FormEasy', FormEasy);
			if (this.headers) {
				for (const name of ['institutionId', 'description', 'description2', 'amount', 'amountOther', 'date']) {
					const col = this.account.csvImportDefaults.headersToCols[name];
					if (col !== undefined && col < this.headers.length) {
						form.getEntries().queryComponent(`.${name}`, FormEntry).getInput(FormSelect).value = this.headers[col]!;
					}
				}
			}
			form.getEntries().queryComponent('.dateFormat', FormEntry).getInput(FormSelect).value = this.account.csvImportDefaults.dateFormat;
		}

		return '';
	}

	protected async configureCSVHeaders(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the mapping of fields to columns.
		for (const name of ['institutionId', 'description', 'description2', 'amount', 'amountOther', 'date']) {
			const headerName = values[name] as string;
			if (headerName !== '*Not used*') {
				let col;
				if (this.headers) {
					col = this.headers.indexOf(headerName);
					if (col === -1) {
						return `The header ${headerName} is not a valid header in the CSV file.`;
					}
				}
				else {
					col = parseInt(headerName);
				}
				this.headersToCols[name] = col;
			}
		}

		// Get the date format.
		this.dateFormat = values['dateFormat'] as string;

		if (this.headersToCols['date'] === undefined) {
			return 'The date field must be present.';
		}
		if (this.headersToCols['amount'] === undefined) {
			return 'The amount field must be present.';
		}

		// Save the CSV import defaults.
		await this.app.send('accounts', 'setCsvImportDefaults', {
			projectId: this.app.projectId,
			id: this.account.id,
			delim: this.delim,
			trimFirstLines: this.trimFirstLines,
			trimLastLines: this.trimLastLines,
			hasHeaders: this.headers !== undefined,
			headersToCols: this.headersToCols,
			dateFormat: this.dateFormat
		});

		// Get the delim character for parsing.
		const delimChar = this.delimToChar(this.delim);

		// Go through each file and make transactions.
		for (const file of this.fileList!) {
			let text;
			try {
				text = await file.text();
			}
			catch {
				return `The file ${file.name} is not a valid text file.`;
			}
			const rows = Csv.parse(text, delimChar);

			// If there are trimming of first lines or headers, pop off the first row.
			let rowI = this.trimFirstLines;
			if (this.headers) {
				rowI += 1;
			}
			// Go through each row and create a transaction.
			for (const l = rows.length; rowI < l - this.trimLastLines; rowI++) {
				const row = rows[rowI]!;

				// Empty row.
				if (row.length === 1) {
					continue;
				}

				// Get the date.
				let date: string;
				try {
					let dateString: string | undefined = row[this.headersToCols['date']]!;
					let year: string | undefined;
					let month: string | undefined;
					let day: string | undefined;
					let time: string | undefined;
					if (this.dateFormat === 'mm?[-/]dd?[-/]yyyy') {
						[dateString, month, day, year, time] = dateString.match(/(\d\d?)[-/](\d\d?)[-/](\d\d\d\d)( \d\d:\d\d:\d\d(\.\d+)?)?/u) ?? [];
						if (dateString === undefined) {
							return `The file ${file.name} row ${rowI + 1} date is not a valid date format of 'mm?[-/]dd?[-/]yyyy[ hh:mm:ss[.s+]]'.`;
						}
					}
					else if (this.dateFormat === 'dd?[-/]mm?[-/]yyyy') {
						[dateString, day, month, year, time] = dateString.match(/(\d\d?)[-/](\d\d?)[-/](\d\d\d\d)( \d\d:\d\d:\d\d(\.\d+)?)?/u) ?? [];
						if (dateString === undefined) {
							return `The file ${file.name} row ${rowI + 1} date is not a valid date format of 'dd?[-/]mm?[-/]yyyy[ hh:mm:ss[.s+]]'.`;
						}
					}
					else if (this.dateFormat === 'yyyy-mm-dd') {
						[dateString, year, month, day, time] = dateString.match(/(\d\d\d\d)-(\d\d)-(\d\d)( \d\d:\d\d:\d\d(\.\d+)?)?/u) ?? [];
						if (dateString === undefined) {
							return `The file ${file.name} row ${rowI + 1} date is not a valid date format of 'yyyy-dd-mm[ hh:mm:ss[.s+]]'.`;
						}
					}
					date = `${year}-${month!.padStart(2, '0')}-${day!.padStart(2, '0')}${time ?? ''}`;
				}
				catch {
					return `The file ${file.name} row ${rowI + 1} date is not a valid date.`;
				}
				const dateFrom = date;
				const dateTo = date;

				// Get the amount.
				let amount: number = 0;
				const amountString = row[this.headersToCols['amount']]!;
				if (amountString !== '') {
					amount = parseFloat(amountString.replace(/[^0-9.+-]/gu, ''));
					if (isNaN(amount)) {
						return `The file ${file.name} row ${rowI + 1} amount ${amountString} is not a number.`;
					}
				}

				// Get the other amount.
				let amountOther: number = 0;
				if (this.headersToCols['amountOther'] !== undefined) {
					const amountOtherString = row[this.headersToCols['amountOther']]!;
					if (amountOtherString !== '') {
						amountOther = parseFloat(amountOtherString.replace(/[^0-9.+-]/gu, ''));
						if (isNaN(amountOther)) {
							return `The file ${file.name} row ${rowI + 1} other amount ${amountOtherString} is not a number.`;
						}
					}
				}
				if (amount === 0 && amountOther === 0) {
					continue;
				}
				if (amount === 0) {
					amount = amountOther;
				}
				if (amountOther === 0) {
					amountOther = amount;
				}

				// Set the instituion id.
				const institutionId = this.headersToCols['institutionId'] !== undefined ? row[this.headersToCols['institutionId']]! : '';

				// Set the from and to properties.
				let accountFrom: Account | undefined;
				let accountTo: Account | undefined;
				let amountFrom: number;
				let institutionIdFrom: string = '';
				let institutionIdTo: string = '';
				let amountTo: number;
				if (amount > 0) {
					amountTo = amount;
					amountFrom = Math.abs(amountOther);
					accountTo = this.account;
					institutionIdTo = institutionId;
				}
				else {
					amountFrom = -amount;
					amountTo = Math.abs(amountOther);
					accountFrom = this.account;
					institutionIdFrom = institutionId;
				}

				// Set the description.
				let description = this.headersToCols['description'] !== undefined ? row[this.headersToCols['description']]! : '';
				description += this.headersToCols['description2'] !== undefined ? ` ${row[this.headersToCols['description2']]}` : '';

				const transaction: Transaction = {
					id: '',
					institutionIdFrom: institutionIdFrom,
					institutionIdTo: institutionIdTo,
					accountIdFrom: accountFrom?.id ?? '',
					accountIdTo: accountTo?.id ?? '',
					amountFrom: amountFrom,
					amountTo: amountTo,
					dateFrom: dateFrom,
					dateTo: dateTo,
					description: description,
					categoryId: '',
					reviewed: false,
					note: ''
				};

				this.transactions.push(transaction);
			}
		}

		// Hide this form and show the review form.
		this.query('.configureCSVHeaders', Element).classList.add('hidden');
		await this.reviewTransactions();

		return '';
	}

	/** Setup the page for reviewing the transactions. */
	protected async reviewTransactions(): Promise<void> {

		// Check for duplicates.
		const { newTransactions, duplicateTransactions } = await this.app.send<{ newTransactions: Transaction[]; duplicateTransactions: Transaction[]; }>('transactions', 'checkForDuplicates', {
			projectId: this.app.projectId,
			accountId: this.account.id,
			transactions: this.transactions
		});
		newTransactions.sort((a, b) => TransactionsImportPage.sortTransaction(a, b));
		duplicateTransactions.sort((a, b) => TransactionsImportPage.sortTransaction(a, b));

		// Set the new transactions.
		this.transactions = newTransactions;

		// Get the unit file.
		const unitFile = await this.app.send<UnitFile>('units', 'getFile', {
			projectId: this.app.projectId
		});

		// Populate the new transactions table.
		let newTransactionsHtml = '';
		for (const transaction of newTransactions) {
			const accountFrom = transaction.accountIdFrom === this.account.id ? this.account : undefined;
			const accountTo = transaction.accountIdTo === this.account.id ? this.account : undefined;
			let unitFrom = accountFrom ? unitFile.list[accountFrom.unitId] : undefined;
			let unitTo = accountTo ? unitFile.list[accountTo.unitId] : undefined;
			if (Items.isGroup(unitFrom)) {
				unitFrom = undefined;
			}
			if (Items.isGroup(unitTo)) {
				unitTo = undefined;
			}

			newTransactionsHtml += `
				<div class="row">
					<span class="from">From</span>
					<span class="dateFrom">${transaction.dateFrom}</span>
					<span class="amountFrom">${displayAmount(transaction.amountFrom, unitFrom, true)}</span>
					<span class="institutionIdFrom">${transaction.institutionIdFrom}</span>
					<span class="to">To</span>
					<span class="dateTo">${transaction.dateTo}</span>
					<span class="amountTo">${displayAmount(transaction.amountTo, unitTo, true)}</span>
					<span class="accountFrom">${accountFrom ? accountFrom.name : ''}</span>
					<span class="accountTo">${accountTo ? accountTo.name : ''}</span>
					<span class="description">${transaction.description}</span>
					<span class="institutionIdTo">${transaction.institutionIdTo}</span>
				</div>`;
		}
		const form = this.queryComponent('.reviewTransactions .FormEasy', FormEasy);
		form.getEntries().setHtml(newTransactionsHtml, form.getEntries().query('.new.transactions', Element), this);

		// Populate the duplicate transactions table.
		let duplicateTransactionsHtml = '';
		for (const transaction of duplicateTransactions) {
			const accountFrom = transaction.accountIdFrom === this.account.id ? this.account : undefined;
			const accountTo = transaction.accountIdTo === this.account.id ? this.account : undefined;
			let unitFrom = accountFrom ? unitFile.list[accountFrom.unitId] : undefined;
			let unitTo = accountTo ? unitFile.list[accountTo.unitId] : undefined;
			if (Items.isGroup(unitFrom)) {
				unitFrom = undefined;
			}
			if (Items.isGroup(unitTo)) {
				unitTo = undefined;
			}

			duplicateTransactionsHtml += `
				<div class="row">
					<span class="from">From</span>
					<span class="dateFrom">${transaction.dateFrom}</span>
					<span class="amountFrom">${displayAmount(transaction.amountFrom, unitFrom, true)}</span>
					<span class="institutionIdFrom">${transaction.institutionIdFrom}</span>
					<span class="to">To</span>
					<span class="dateTo">${transaction.dateTo}</span>
					<span class="amountTo">${displayAmount(transaction.amountTo, unitTo, true)}</span>
					<span class="accountFrom">${accountFrom ? accountFrom.name : ''}</span>
					<span class="accountTo">${accountTo ? accountTo.name : ''}</span>
					<span class="description">${transaction.description}</span>
					<span class="institutionIdTo">${transaction.institutionIdTo}</span>
				</div>`;
		}
		form.getEntries().setHtml(duplicateTransactionsHtml, form.getEntries().query('.duplicate.transactions', Element), this);

		// Show the next form.
		this.query('.reviewTransactions', Element).classList.remove('hidden');
	}

	protected async importTransactions(_form: FormEasy, _values: FormValues): Promise<string> {

		await this.app.send('transactions', 'update', {
			projectId: this.app.projectId,
			transactions: this.transactions.map((transaction) => ({
				dateFromOld: transaction.dateFrom,
				transaction: transaction
			}))
		});

		// Go to the account view page.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'accountView',
			id: this.account.id
		});

		return '';
	}

	/** Converts the delim value to the actual character for the CSV parsing. */
	delimToChar(delim: string): string | undefined {
		if (delim === 'Comma') {
			return ',';
		}
		else if (delim === 'Tab') {
			return '\t';
		}
		else if (delim === 'Space') {
			return ' ';
		}
		else if (delim === 'Semicolon') {
			return ';';
		}
		return undefined;
	}

	static sortTransaction(a: Transaction, b: Transaction): number {
		if (a.dateFrom < b.dateFrom) {
			return -1;
		}
		else if (a.dateFrom > b.dateFrom) {
			return +1;
		}
		else {
			if (a.amountFrom < b.amountFrom) {
				return -1;
			}
			else if (a.amountFrom > b.amountFrom) {
				return +1;
			}
			else {
				return 0;
			}
		}
	}

	/** The id of the account. */
	private _accountId!: string;

	/** The account we're working on. */
	private account!: Account;

	/** The list of files. */
	private fileList: FileList | undefined;

	/** The extension of the imported files. */
	private extension: string = '';

	/** The new transactions. */
	private transactions: Transaction[] = [];

	// CSV defaults.
	private headers: string[] | undefined;
	private headersToCols: Record<string, number> = {};
	private delim: string = 'Comma';
	private dateFormat: string = '';
	private trimFirstLines: number = 0;
	private trimLastLines: number = 0;

	protected static override html = html;
	protected static override css = css;
}
