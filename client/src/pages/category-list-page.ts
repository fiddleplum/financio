import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The category list page. */
export class CategoryListPage extends ListPage {

	/** Constructor. */
	constructor(params: Params) {
		super('Category', 'categories', params);
	}
}
