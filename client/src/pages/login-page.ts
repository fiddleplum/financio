import { FormEasy, FormValues, Router } from '@orourley/elm-app';
import { FinancioPage } from '../internal';

import html from './login-page.html';
import css from './login-page.css';

/** The login page. */
export class LoginPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(oldQuery: Router.Query | undefined, _query: Router.Query): Promise<void> {

		// Save the old page so we can go back to it after login.
		this.prevPage = oldQuery?.['page'];
		if (this.prevPage === 'login') {
			this.prevPage = undefined;
		}

		// Set the subtitle.
		this.app.setSubtitle('Login');
	}

	/** Logs in. */
	protected async login(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the values.
		const username = values['username'] as string;
		const password = values['password'] as string;

		// Login.
		try {
			await this.app.login(username, password);
		}
		catch (error) {
			return (error as Error).message;
		}

		// Update the route.
		this.app.setRouterQuery({
			page: this.prevPage,
			prevPage: undefined
		}, { mergeOldQuery: true });

		return '';
	}

	/** The previous page. */
	private prevPage: string | undefined;

	protected static override html = html;
	protected static override css = css;
}
