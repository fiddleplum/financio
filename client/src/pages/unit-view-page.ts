import { FormEasy, FormValues, Router } from '@orourley/elm-app';
import { FinancioPage, FormEntry, RelativeDateChooser, UnitChooser } from '../internal';
import { ConversionRates, Unit, Project } from '@orourley/financio-types';

import html from './unit-view-page.html';
import css from './unit-view-page.css';

/** The unit view page. */
export class UnitViewPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the unit id.
		const unitId = query['id'];
		if (unitId === undefined) {
			this.setHtml('No unit specified.', this.root);
			return;
		}
		this._unitId = unitId;

		// Get the other unit id. If none is specified, use the default unit for the project.
		let otherUnitId = query['otherId'];
		if (otherUnitId === undefined) {
			const project = await this.app.send<Project>('projects', 'getItem', {
				id: this.app.projectId
			});
			otherUnitId = project.defaultUnitId;
		}
		if (otherUnitId !== undefined && otherUnitId !== unitId) {
			this._otherUnitId = otherUnitId;
		}
		const form = this.queryComponent('.FormEasy', FormEasy);
		const unitChooser = form.getEntries().queryComponent('.otherUnitId', FormEntry).getInput(UnitChooser);
		unitChooser.setExcludedItem(this._unitId, true);
		unitChooser.value = this._otherUnitId;

		// Get the start and end dates.
		this._startDate = query['startDate'] ?? ',-1,years,';
		this._endDate = query['endDate'] ?? ',,,';

		// Update the display.
		await this.updateDisplay();
	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the unit.
		const unit = await this.app.send<Unit>('units', 'getItem', {
			projectId: this.app.projectId,
			id: this._unitId
		});

		// Populate the otherUnitIds drop down.
		const form = this.queryComponent('.FormEasy', FormEasy);
		const otherUnitIdsInput = form.getEntries().queryComponent('.otherUnitId', FormEntry).getInput(UnitChooser);
		otherUnitIdsInput.value = this._otherUnitId;

		// Set the start date.
		form.getEntries().queryComponent('.startDate', FormEntry).getInput(RelativeDateChooser).value = this._startDate;
		form.getEntries().queryComponent('.endDate', FormEntry).getInput(RelativeDateChooser).value = this._endDate;

		// Set the subtitle.
		this.app.setSubtitle(`Unit - ${unit.name}`);

		// Update the transaction list.
		await this.updateList();
	}

	/** Updates the list. */
	private async updateList(): Promise<void> {

		// Get the conversion rates.
		const unitConversionRates = await this.app.send<ConversionRates>('units', 'listConversionRates', {
			projectId: this.app.projectId,
			id: this._unitId,
			otherId: this._otherUnitId,
			startDate: this._startDate,
			endDate: this._endDate
		});

		// Get the other unit.
		const otherUnit = await this.app.send<Unit>('units', 'getItem', {
			projectId: this.app.projectId,
			id: this._otherUnitId
		});

		// Get the conversion rates for the other unit.
		const conversionRates = unitConversionRates[this._otherUnitId] ?? [];

		// Remove the two book-end rates.
		conversionRates.shift();
		conversionRates.pop();

		// Set the html.
		let html = '';
		for (const conversionRate of conversionRates) {
			html += `<div class="entry"><div class="date">${conversionRate[0]}</div><div class="rate">${conversionRate[1].toFixed(otherUnit.decimals)} ${otherUnit.name}</div></div>`;
		}
		this.setHtml(html, this.query('.conversionRates', Element), this);
	}

	/** Updates the filter. */
	async filterSubmitted(_form: FormEasy, values: FormValues): Promise<void> {

		// Save the form values.
		this._otherUnitId = values['otherUnitId'] as string;
		this._startDate = values['startDate'] as string;
		this._endDate = values['endDate'] as string;

		// Update the list.
		await this.updateList();
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<UnitEditPopup typeId="${this._unitId}" edit onSaved="onSaved"></UnitEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<UnitDeletePopup typeId="${this._unitId}" onDeleted="onDeleted"></UnitDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(_unitId: string): void {

		// Update the display.
		void this.updateDisplay();
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the list.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'unitsList'
		});
	}

	/** Shows the new conversion rate popup. */
	newConversionRatePopupShow(): void {
		this.app.openOverlay(`<UnitConversionRateEditPopup typeId="${this._unitId}" onSubmitted="newUnitConversionRatePopupSubmitted"></UnitConversionRateEditPopup>`, this);
	}

	/** Called when the new unit conversion entry popup has submitted. */
	async newUnitConversionRatePopupSubmitted(): Promise<void> {

		// Update the display.
		await this.updateDisplay();
	}

	/** The id of the unit. */
	private _unitId!: string;

	private _otherUnitId: string = '';
	private _startDate!: string;
	private _endDate!: string;

	protected static override html = html;
	protected static override css = css;
}
