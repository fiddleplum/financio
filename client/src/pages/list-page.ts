import { Params, Sanitizer } from '@orourley/elm-app';
import { Items } from '@orourley/financio-types';
import { FinancioPage } from '../internal';
import { HierarchyList } from '../components/hierarchy-list';

import html from './list-page.html';
import css from './list-page.css';

/** The list page. */
export abstract class ListPage extends FinancioPage {

	/** Constructor. */
	constructor(type: string, module: string, params: Params) {
		super(params);

		// Set the type.
		this._type = type;

		// Save the module.
		this._module = module;

		// Set the subtitle.
		this.app.setSubtitle(`${this._type} List`);

		// Set the type in the different places.
		this.queryAll('.type', Element).forEach((elem) => {
			elem.innerHTML = this._type;
		});
	}

	override async processQuery(): Promise<void> {

		// Update the display.
		await this.populateContent();
	}

	protected async populateContent(): Promise<void> {

		// Get the content element.
		const contentElem = this.query('.content', Element);

		// Get the list of items.
		const itemFile = await this.app.send<Items.File>(this._module, 'list', {
			projectId: this.app.projectId
		});

		// Populate the HTML.
		let html = ``;
		if (itemFile.childIds.length > 0) {
			html += `<HierarchyList onItemMoved="onItemMoved" onFilterElem="onFilterElem" nograb>`;
			html += this.populateList(itemFile);
			html += `</HierarchyList>`;
		}
		else {
			html += `<p class="first">There are currently no items.</p>`;
		}
		this.setHtml(html, contentElem, this);
	}

	/** Gets html for the list. */
	private populateList(itemFile: Items.File): string {
		let html = ``;
		Items.iterateItems(itemFile, itemFile, (entry) => {
			if (Items.isFile(entry)) {
				html += `
					<span class="edit ${this._editing ? '' : 'hidden'}">
						${entry.sortChildren ? `<span>(sorted)</span>` : ``}
						<Icon src="assets/icons/edit.svg" alt="edit" onclick="openGroupEditPopup||file|edit"></Icon>
					</span>`;
			}
			else if (Items.isGroup(entry)) {
				html += `
					<li class="group" data-id="${entry.id}" data-name="${Sanitizer.sanitizeForAttribute(entry.name)}">
						<div class="entry">
							<Icon class="grab" src="assets/icons/grab.svg" alt="drag"></Icon>
							<span class="name">${Sanitizer.sanitizeForHtml(entry.name)}</span>
							<span class="edit ${this._editing ? '' : 'hidden'}">
								${entry.sortChildren ? `<span>(sorted)</span>` : ``}
								<Icon src="assets/icons/edit.svg" alt="edit" onclick="openGroupEditPopup|${entry.id}||edit"></Icon>
								${entry.childIds.length === 0 ? `<Icon src="assets/icons/delete.svg" alt="delete" onclick="openGroupDeletePopup|${entry.id}"></Icon>` : ``}
							</span>
						</div>
					`;
			}
			else {
				html += `
					<li class="item" data-id="${entry.id}" data-name="${Sanitizer.sanitizeForAttribute(entry.name)}">
						<div class="entry">
							<Icon class="grab" src="assets/icons/grab.svg" alt="drag"></Icon>
							<a class="name" href="#page=${this._type.toLowerCase()}View&${this._type === 'Project' ? `projectId=${entry.id}` : `projectId=${this.app.projectId}&id=${entry.id}`}">${Sanitizer.sanitizeForHtml(entry.name)}</a>
							<span class="edit ${this._editing ? '' : 'hidden'}">
								<Icon src="assets/icons/edit.svg" alt="edit" onclick="openItemEditPopup|${entry.id}"></Icon>
								<Icon src="assets/icons/delete.svg" alt="delete" onclick="openItemDeletePopup|${entry.id}"></Icon>
							</span>
						</div>
					</li>`;
			}
		}, (entry) => {
			html += `<ul class="group${entry.sortChildren ? ' sortChildren' : ''}">`;
			return true;
		}, () => {
			html += `</ul></li>`;
		});
		return html;
	}

	/** A callback to filter out which drag lines are visible. */
	protected onFilterElem(elem: HTMLElement, before: boolean, draggedItem: HTMLLIElement): boolean {
		if (elem instanceof HTMLLIElement) {
			if (elem === draggedItem && before) {
				return true;
			}
			if (elem.parentElement!.classList.contains('sortChildren')) {
				const draggedName = draggedItem.dataset['name']!;
				const name = Sanitizer.desanitizeFromAttribute(elem.dataset['name']!);
				if (before) {
					if (elem.previousElementSibling instanceof HTMLLIElement) {
						const beforeName = Sanitizer.desanitizeFromAttribute(elem.previousElementSibling.dataset['name']!);
						return beforeName <= draggedName && draggedName < name;
					}
					else {
						return draggedName < name;
					}
				}
				else {
					if (elem.nextElementSibling instanceof HTMLLIElement) {
						const nextName = Sanitizer.desanitizeFromAttribute(elem.nextElementSibling.dataset['name']!);
						return name <= draggedName && draggedName < nextName;
					}
					else {
						return name <= draggedName;
					}
				}
			}
		}
		return true;
	}

	/** Toggles the edit mode on and off. */
	protected toggleEdit(): void {
		this._editing = !this._editing;

		// Toggle the grab icons.
		const hierarchyList = this.queryComponent('.HierarchyList', HierarchyList);
		hierarchyList.setGrabButtonVisible(this._editing);

		// Toggle the edit elems.
		const editElems = hierarchyList.getContent().queryAll('.edit', HTMLElement);
		editElems.forEach((editElem) => {
			editElem.classList.toggle('hidden', !this._editing);
		});

		// Toggle the instructions.
		this.query('.editInstructions', Element).classList.toggle('hidden', !this._editing);
	}

	/** When an item is moved in the hierarchy list. */
	protected onItemMoved(item: HTMLLIElement, parentItem: HTMLLIElement | undefined, beforeItem: HTMLLIElement | undefined): void {
		void this.app.send(this._module, 'moveItemOrGroup', {
			projectId: this.app.projectId,
			id: item.dataset['id'],
			parentId: parentItem?.dataset['id'],
			beforeId: beforeItem?.dataset['id']
		});
	}

	/** Shows the item create popup. */
	openItemCreatePopup(): void {
		this.app.openOverlay(`<${this._type}EditPopup onSaved="populateContent"></${this._type}EditPopup>`, this);
	}

	/** Shows the item edit popup. */
	openItemEditPopup(id: string): void {
		this.app.openOverlay(`<${this._type}EditPopup edit onSaved="populateContent" typeId="${id}"></${this._type}EditPopup>`, this);
	}

	/** Shows the item delete popup. */
	openItemDeletePopup(id: string): void {
		this.app.openOverlay(`<${this._type}DeletePopup onDeleted="populateContent" typeId="${id}"></${this._type}DeletePopup>`, this);
	}

	/** Opens the group create popup. */
	protected openGroupCreatePopup(): void {
		this.app.openOverlay(`<GroupEditPopup onSaved="populateContent" itemType="${this._type}" module="${this._module}"></GroupEditPopup>`, this);
	}

	/** Opens the group edit popup. */
	protected openGroupEditPopup(id: string, file: string): void {
		this.app.openOverlay(`<GroupEditPopup edit onSaved="populateContent" ${id !== '' ? `typeId="${id}"` : ''} itemType="${this._type}" module="${this._module}" ${file}></GroupEditPopup>`, this);
	}

	/** Opens the group delete popup. */
	protected openGroupDeletePopup(id: string): void {
		this.app.openOverlay(`<GroupDeletePopup onDeleted="populateContent" typeId="${id}" module="${this._module}"></GroupDeletePopup>`, this);
	}

	/** The type. */
	private _type: string;

	/** The server module we use for the commands. */
	private _module: string;

	/** Whether or not we are in editing mode for rearranging the items and groups. */
	private _editing: boolean = false;

	protected static override html = html;
	protected static override css = css;
}
