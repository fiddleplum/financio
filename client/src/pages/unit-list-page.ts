import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The unit list page. */
export class UnitListPage extends ListPage {

	/** Constructor. */
	constructor(params: Params) {
		super('Unit', 'units', params);
	}
}
