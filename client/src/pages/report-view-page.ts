import { Router } from '@orourley/elm-app';
import { Account, ConversionRates, Unit, Filter, Transaction, RelativeDate, Report, YMD, Items, AccountFile, Project, UnitFile, AccountGroup, CategoryGroup, Category } from '@orourley/financio-types';
import { FinancioApp, FinancioPage } from '../internal';

import html from './report-view-page.html';
import css from './report-view-page.css';
import { displayAmount } from '../utils/display_amount';

/** The report view page. */
export class ReportViewPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the report id.
		const reportId = query['id'];
		if (reportId === undefined) {
			this.setHtml('No report specified.', this.root);
			return;
		}
		this._reportId = reportId;

		// Update the display.
		await this.updateDisplay();
	}

	/** Updates the display. */
	async updateDisplay(): Promise<void> {

		// Get the report.
		const report = await this.app.send<Report>('reports', 'getItem', {
			projectId: this.app.projectId,
			id: this._reportId
		});

		// Set the subtitle.
		this.app.setSubtitle(`Report - ${report.name}`);

		// Update the transaction list.
		await this.updateReport(report);
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<ReportEditPopup typeId="${this._reportId}" edit onSaved="onSaved"></ReportEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<ReportDeletePopup typeId="${this._reportId}" onDeleted="onDeleted"></ReportDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(): void {

		// Update the display.
		void this.updateDisplay();
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the list.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'reportsList'
		});
	}

	/** Updates the transaction list. */
	private async updateReport(report: Report): Promise<void> {

		// Get the financio app.
		const financioApp = this.app as FinancioApp;

		// Adjust the start date to be at the beginning of its month so that the transactions can be used with valuesAtMonthStart.
		const startYmd = RelativeDate.toYMD(RelativeDate.fromString(report.startDate));
		startYmd.day = 1;
		const startDate: RelativeDate = {
			absolute: startYmd.toString()
		};

		// Setup the transaction filter.
		const filter: Filter = {
			startDate: RelativeDate.toString(startDate),
			endDate: report.endDate,
			...(report.style === 'incomeStatement' && report.categoryIds && { categoryIds: report.categoryIds }),
			...(report.style === 'balanceSheet' && report.accountIds && {
				accountIdsFrom: report.accountIds,
				accountIdsTo: report.accountIds })
		};

		// Get the latest transactions that are associated with this account.
		const transactions = await financioApp.send<Transaction[]>('transactions', 'list', {
			projectId: financioApp.projectId,
			filter: filter
		});
		transactions.sort((a, b) => a.dateFrom.localeCompare(b.dateFrom));

		// Set up the HTML for the report.
		let html = '';
		if (report.style === 'incomeStatement') {
			html = await this.doIncomeStatement(report, transactions);
		}
		else if (report.style === 'balanceSheet') {
			html = await this.doBalanceSheet(report, transactions);
		}
		this.setHtml(html, this.query('.report', Element), this);
	}

	/** Makes the HTML for an income statement. */
	private async doIncomeStatement(report: Report & { style: 'incomeStatement'; }, transactions: Transaction[]): Promise<string> {

		// Start the html.
		let html = '';

		// Get the list of categories.
		const categoryList = await this.app.send<Items.File>('categories', 'list', {
			projectId: this.app.projectId
		});

		// Get the account file.
		const accountFile = await this.app.send<AccountFile | undefined>('accounts', 'getFile', {
			projectId: this.app.projectId
		});
		if (!accountFile) {
			throw new Error('No accounts.');
		}

		// Get the report unit.
		const project = await this.app.send<Project>('projects', 'getItem', {
			id: this.app.projectId
		});
		const unitFile = await this.app.send<UnitFile>('units', 'getFile', {
			projectId: this.app.projectId
		});
		const reportUnitId = report.unitId ?? project.defaultUnitId;
		const reportUnit = unitFile.list[reportUnitId];
		if (!reportUnit || Items.isGroup(reportUnit)) {
			throw new Error('No unit found.');
		}

		// Get the start and end dates.
		const startDate = RelativeDate.toYMD(RelativeDate.fromString(report.startDate));
		const endDate = RelativeDate.toYMD(RelativeDate.fromString(report.endDate));
		const periodIntervals = report.periodIntervals;

		// Title
		html += '<h2>Income Statement</h2>';
		html += `<h3>For the period from ${startDate.toString()} to ${endDate.toString()}</h3>`;
		html += '<table>';

		// Setup header.
		html += '<tr class="header"><td class="category"></td>';
		let numColumns = 0;
		const totalsPerPeriod: number[] = [];
		for (let currentDate = new YMD(startDate); currentDate <= endDate; currentDate = this.getNextPeriod(currentDate, periodIntervals)) {
			html += '<td>';
			if (periodIntervals === 'days' || periodIntervals === 'weeks') {
				html += `${currentDate.toString()}`;
			}
			else if (periodIntervals === 'months') {
				html += `${currentDate.toYearMonthString()}`;
			}
			else if (periodIntervals === 'quarters') {
				html += `${currentDate.year.toFixed(0)} Q${((currentDate.month - 1) / 3).toFixed(0)}`;
			}
			else if (periodIntervals === 'years') {
				html += `${currentDate.year.toFixed(0)}`;
			}
			html += '</td>';
			numColumns += 1;
			totalsPerPeriod.push(0);
		}
		html += '<td class="total">Total</td>';
		html += '<td class="average">Average</td>';
		html += '</tr>';
		Items.iterateItems(categoryList, categoryList, (categoryOrGroup, depth) => {

			// Ignore the file.
			if (Items.isFile(categoryOrGroup)) {
				return;
			}

			html += this.doIncomeStatementRow(categoryOrGroup, depth, report, startDate, endDate,
				transactions, categoryList, accountFile, reportUnit, totalsPerPeriod, false);
		}, undefined, (groupOrFile, depth) => {

			// Ignore the file.
			if (Items.isFile(groupOrFile)) {
				return;
			}

			html += this.doIncomeStatementRow(groupOrFile, depth, report, startDate, endDate,
				transactions, categoryList, accountFile, reportUnit, totalsPerPeriod, true);
		});

		// The total per period.
		html += '<tr class="total"><td class="category">Total Income</td>';
		numColumns = 0;
		let totalAmount = 0;
		for (let currentDate = new YMD(startDate); currentDate <= endDate; currentDate = this.getNextPeriod(currentDate, periodIntervals)) {
			html += `<td>${displayAmount(totalsPerPeriod[numColumns]!, reportUnit, false)}</td>`;
			totalAmount += totalsPerPeriod[numColumns]!;
			numColumns += 1;
		}
		html += `<td class="total">${displayAmount(totalAmount, reportUnit, false)}</td>`;
		html += `<td class="average">${displayAmount(totalAmount / numColumns, reportUnit, false)}</td>`;
		html += '</tr>';

		html += '</table>';

		return html;
	}

	private doIncomeStatementRow(categoryOrGroup: Category | CategoryGroup, depth: number, report: Report & { style: 'incomeStatement'; }, startDate: YMD, endDate: YMD, transactions: Transaction[], categoryList: Items.File, accountFile: AccountFile, unit: Unit, totalsPerPeriod: number[], isSubtotal: boolean): string {

		// Filter out any categories not in the report.
		if (report.categoryIds) {
			let categoryInReport = false;
			for (const reportCategoryId of report.categoryIds) {
				const reportCategory = categoryList.list[reportCategoryId];
				if (!reportCategory) {
					continue;
				}
				if (Items.contains(categoryOrGroup, reportCategory, categoryList)
					|| Items.contains(reportCategory, categoryOrGroup, categoryList)) {
					categoryInReport = true;
				}
			}
			if (!categoryInReport) {
				return '';
			}
		}

		// The html that will be filled out.
		let html = '';

		// If it's a group, but not a subtotal row, just do a blank line.
		if (Items.isGroup(categoryOrGroup) && !isSubtotal) {
			html += `<td class="category" style="padding-left: ${0.5 + 0.5 * depth}em"><a href="#page=categoryView&projectId=${this.app.projectId}&id=${categoryOrGroup.id}">${categoryOrGroup.name}</a></td>`;
			for (let beginningOfPeriod = new YMD(startDate); beginningOfPeriod <= endDate; beginningOfPeriod = this.getNextPeriod(beginningOfPeriod, report.periodIntervals)) {
				html += `<td></td>`;
			}
			html += `<td class="total"></td>`;
			html += `<td class="average"></td>`;
			html += '</tr>';
			return html;
		}

		// It's a category or a subtotal.
		if (Items.isGroup(categoryOrGroup)) {
			html += '<tr class="subtotal">';
			html += `<td class="category" style="padding-left: ${0.5 + 0.5 * depth}em">Total ${categoryOrGroup.name}</td>`;
		}
		else {
			html += '<tr>';
			html += `<td class="category" style="padding-left: ${0.5 + 0.5 * depth}em"><a href="#page=categoryView&projectId=${this.app.projectId}&id=${categoryOrGroup.id}">${categoryOrGroup.name}</a></td>`;
		}
		let numColumns = 0;
		let transactionIndex = 0;
		let totalAmount = 0;
		for (let beginningOfPeriod = new YMD(startDate); beginningOfPeriod <= endDate; beginningOfPeriod = this.getNextPeriod(beginningOfPeriod, report.periodIntervals)) {
			const endingOfPeriod = this.getEndingOfPeriod(beginningOfPeriod, report.periodIntervals);
			let amountThisPeriod = 0;
			for (; transactionIndex < transactions.length; transactionIndex += 1) {
				const transaction = transactions[transactionIndex]!;
				const transactionCategory = categoryList.list[transaction.categoryId];
				if (!transactionCategory) {
					continue;
				}
				if (!Items.contains(categoryOrGroup, transactionCategory, categoryList)) {
					continue;
				}
				const accountFrom = accountFile.list[transaction.accountIdFrom];
				const accountTo = accountFile.list[transaction.accountIdTo];
				if (!accountFrom || !accountTo || Items.isGroup(accountFrom) || Items.isGroup(accountTo)) {
					continue;
				}
				if (!accountFrom.internal && accountTo.internal) {
					if (new YMD(transaction.dateTo) < beginningOfPeriod) {
						continue;
					}
					if (new YMD(transaction.dateTo) > endingOfPeriod) {
						break;
					}
					let conversionRate = 1;
					if (unit.id !== accountTo.unitId) {
						conversionRate = Unit.getConversionRateAtDate(unit.id, accountTo.unitId, transaction.dateTo, unit.conversionRates) ?? NaN;
					}
					amountThisPeriod += transaction.amountTo / conversionRate;
				}
				else if (accountFrom.internal && !accountTo.internal) {
					if (new YMD(transaction.dateFrom) < beginningOfPeriod) {
						continue;
					}
					if (new YMD(transaction.dateFrom) > endingOfPeriod) {
						break;
					}
					let conversionRate = 1;
					if (unit.id !== accountTo.unitId) {
						conversionRate = Unit.getConversionRateAtDate(unit.id, accountTo.unitId, transaction.dateTo, unit.conversionRates) ?? NaN;
					}
					amountThisPeriod -= transaction.amountFrom / conversionRate;
				}
			}
			totalAmount += amountThisPeriod;
			if (Items.isGroup(categoryOrGroup)) {
				html += `<td>${displayAmount(amountThisPeriod, unit, false)}</td>`;
			}
			else {
				html += `<td><a href="#page=categoryView&projectId=${this.app.projectId}&id=${categoryOrGroup.id}&startDate=${beginningOfPeriod.toString()},,${report.periodIntervals},beginning&endDate=${beginningOfPeriod.toString()},,${report.periodIntervals},ending">${displayAmount(amountThisPeriod, unit, false)}</a></td>`;
			}
			if (!Items.isGroup(categoryOrGroup)) {
				totalsPerPeriod[numColumns]! += amountThisPeriod;
			}
			numColumns += 1;
		}

		// The total per category.
		html += `<td class="total">${displayAmount(totalAmount, unit, false)}</td>`;
		html += `<td class="average">${displayAmount(totalAmount / numColumns, unit, false)}</td>`;
		html += '</tr>';

		return html;
	}

	/** Makes the HTML for a balance sheet. */
	private async doBalanceSheet(report: Report & { style: 'balanceSheet'; }, transactions: Transaction[]): Promise<string> {

		// Start the html.
		let html = '';

		// Get the account file.
		const accountFile = await this.app.send<AccountFile>('accounts', 'getFile', {
			projectId: this.app.projectId
		});

		// Get the unit file.
		const unitFile = await this.app.send<UnitFile>('units', 'getFile', {
			projectId: this.app.projectId
		});

		// Get the default unit id as the report id.
		const project = await this.app.send<Project>('projects', 'getItem', {
			id: this.app.projectId
		});
		const reportUnitId = report.unitId ?? project.defaultUnitId;
		const reportUnit = unitFile.list[reportUnitId];
		if (!reportUnit || Items.isGroup(reportUnit)) {
			throw new Error('No unit found.');
		}

		// Get the start and end dates.
		const startDate = RelativeDate.toYMD(RelativeDate.fromString(report.startDate));
		const endDate = RelativeDate.toYMD(RelativeDate.fromString(report.endDate));
		const periodInterval = report.periodIntervals;

		// Get the conversion rates.
		const unitConversionRates = await this.app.send<ConversionRates>('units', 'listConversionRates', {
			projectId: this.app.projectId,
			id: reportUnitId,
			startDate: report.startDate,
			endDate: report.endDate
		});

		// Title
		html += '<h2>Balance Sheet</h2>';
		html += `<h3>For the period from ${startDate.toString()} to ${endDate.toString()}</h3>`;
		html += `<p>All amounts at start of day in ${reportUnit.name}.</p>`;
		html += '<table>';

		// Get the periods.
		const periods: YMD[] = [];
		let date = this.getBeginningOfPeriod(startDate, periodInterval);
		while (true) {
			periods.push(date);
			if (endDate < date) {
				break;
			}
			date = this.getNextPeriod(date, periodInterval);
		}

		// Get the accounts that we'll be displaying.
		const promises: Promise<void>[] = [];
		const accountsInReport: Record<string, Account | AccountGroup> = {};
		const accountIdsInReport = new Set<string>();
		Items.iterateItems(accountFile, accountFile, (accountOrGroup) => {

			// Don't do the file itself.
			if (Items.isFile(accountOrGroup)) {
				return;
			}

			// Filter out any accounts not in the report.
			if (report.accountIds !== undefined) {
				let accountInReport = false;
				for (const reportAccountId of report.accountIds) {
					const reportAccount = accountFile.list[reportAccountId];
					if (!reportAccount) {
						continue;
					}
					if (Items.contains(accountOrGroup, reportAccount, accountFile) || Items.contains(reportAccount, accountOrGroup, accountFile)) {
						accountInReport = true;
					}
				}
				if (!accountInReport) {
					return;
				}
			}

			// Push the promises.
			promises.push(this.app.send<Account | AccountGroup>('accounts', Items.isGroup(accountOrGroup) ? 'getGroup' : 'getItem', {
				projectId: this.app.projectId,
				id: accountOrGroup.id
			}).then((account) => {
				accountsInReport[accountOrGroup.id] = account;
				accountIdsInReport.add(account.id);
			}));
		});
		await Promise.all(promises);

		// Build the table cells.
		const accountTotals: Record<string, { start: number; periods: number[]; }> = {};
		Items.iterateItems(accountFile, accountFile, (accountOrGroup) => {

			// Don't do the file.
			if (Items.isFile(accountOrGroup)) {
				return;
			}

			// If this isn't in the report, just move on.
			if (accountsInReport[accountOrGroup.id] === undefined) {
				return;
			}

			// Get month containing this date as a number.
			let valueAtStartOfFirstPeriodMonth = 0;
			if (!Items.isGroup(accountOrGroup)) {
				const firstPeriod = periods[0]!.toYearMonthString();
				valueAtStartOfFirstPeriodMonth = Account.getValueAtMonthStart(firstPeriod, accountOrGroup, accountFile, accountIdsInReport);
			}

			// Set the account totals to be the start of the first period.
			// Transactions will fill in the rest.
			accountTotals[accountOrGroup.id] = { start: valueAtStartOfFirstPeriodMonth, periods: new Array<number>(periods.length).fill(0) };
		});

		// Populate the account totals for each period.
		for (let transactionI = 0; transactionI < transactions.length; transactionI += 1) {
			const transaction = transactions[transactionI]!;

			// Add the amountFrom to the appropriate account and period.
			if (accountTotals[transaction.accountIdFrom] !== undefined) {
				const dateFromYmd = new YMD(transaction.dateFrom);
				let periodI = 0;
				for (; periodI < periods.length; periodI++) {
					if (dateFromYmd < periods[periodI]!) {
						break;
					}
				}
				if (periodI < periods.length) {
					const accountFrom = accountFile.list[transaction.accountIdFrom];
					if (!accountFrom || Items.isGroup(accountFrom)) {
						throw new Error(`Transaction with id ${transaction.id} has an invalid accountIdFrom.`);
					}
					accountTotals[transaction.accountIdFrom]!.periods[periodI]! -= transaction.amountFrom;
				}
			}

			// Add the amountTo to the appropriate account and period.
			if (accountTotals[transaction.accountIdTo] !== undefined) {
				const dateToYmd = new YMD(transaction.dateTo);
				let periodI = 0;
				for (; periodI < periods.length; periodI++) {
					if (dateToYmd < periods[periodI]!) {
						break;
					}
				}
				if (periodI < periods.length) {
					const accountTo = accountFile.list[transaction.accountIdTo];
					if (!accountTo || Items.isGroup(accountTo)) {
						throw new Error(`Transaction with id ${transaction.id} has an invalid accountIdTo.`);
					}
					accountTotals[transaction.accountIdTo]!.periods[periodI]! += transaction.amountTo;
				}
			}
		}

		// Accumulate the previous periods into the later periods.
		for (const accountTotal of Object.values(accountTotals)) {
			for (let i = 1; i < accountTotal.periods.length; i++) {
				accountTotal.periods[i]! += accountTotal.periods[i - 1]!;
				accountTotal.periods[i - 1]! += accountTotal.start;
			}
			if (accountTotal.periods.length > 0) {
				accountTotal.periods[accountTotal.periods.length - 1]! += accountTotal.start;
			}
		}

		// Setup header and totals.
		html += '<tr class="header"><td class="account"></td>';
		for (let periodI = 0; periodI < periods.length; periodI++) {
			let period = periods[periodI]!;

			// Set the header HTML for this column.
			if (report.useEndOfDay) {
				period = new YMD(period);
				period.day -= 1;
			}
			html += `<td>${period.toString()}</td>`;
		}
		if (periods.length > 1) {
			html += '<td class="change">Change</td>';
			html += '<td class="percentage">%</td>';
		}
		html += '</tr>';

		// Go through each account as a row.
		const totalsPerPeriod: number[][] = [];
		Items.iterateItems(accountFile, accountFile, (accountOrGroup, depth) => {

			// Ignore the file.
			if (Items.isFile(accountOrGroup)) {
				return;
			}

			// Don't do anything if it's not in the list.
			if (!accountTotals[accountOrGroup.id]) {
				return;
			}

			html += '<tr>';
			html += `<td class="account" style="padding-left: ${depth}rem"><a href="#page=accountView&projectId=${this.app.projectId}&id=${accountOrGroup.id}">${accountOrGroup.name}</a></td>`;
			for (let periodI = 0; periodI < periods.length; periodI++) {

				// Show nothing for a parent account.
				if (Items.hasChildren(accountOrGroup)) {
					html += `<td></td>`;
					continue;
				}

				const period = periods[periodI]!;
				const totalForPeriod = accountTotals[accountOrGroup.id]!.periods[periodI]!;

				const conversionRate = Unit.getConversionRateAtDate(reportUnitId, accountOrGroup.unitId, period.toString(), unitConversionRates) ?? NaN;
				totalsPerPeriod[totalsPerPeriod.length - 1]![periodI]! += totalForPeriod / conversionRate;

				html += `<td><a href="#page=accountView&projectId=${this.app.projectId}&id=${accountOrGroup.id}&startDate=${period.toString()},,${report.periodIntervals},beginning&endDate=${period.toString()},,${report.periodIntervals},ending">${displayAmount(totalForPeriod / conversionRate, reportUnit, false)}</a></td>`;
			}

			// The total per account.
			if (periods.length > 1) {
				if (Items.isGroup(accountOrGroup)) {
					html += `<td></td><td></td></tr>`;
					return;
				}
				const valueAtStart = accountTotals[accountOrGroup.id]!.periods[0]!;
				const valueAtEnd = accountTotals[accountOrGroup.id]!.periods[accountTotals[accountOrGroup.id]!.periods.length - 1]!;
				const conversionRate = Unit.getConversionRateAtDate(reportUnitId, accountOrGroup.unitId, periods[0]!.toString(), unitConversionRates) ?? NaN;
				html += `<td class="change">${displayAmount((valueAtEnd - valueAtStart) / conversionRate, reportUnit, false)}</td>`;
				const percentage = (valueAtEnd - valueAtStart) / valueAtStart;
				if (isFinite(percentage)) {
					html += `<td class="percentage">${(percentage * 100).toFixed(2)}</td>`;
				}
				else {
					html += `<td class="percentage"></td>`;
				}
			}
			html += '</tr>';
		}, (groupOrFile) => {

			// Don't do anything not in the list.
			if (Items.isGroup(groupOrFile) && !accountTotals[groupOrFile.id]) {
				return false;
			}

			// Push a new set of zeros to the stack.
			totalsPerPeriod.push(new Array<number>(periods.length).fill(0));

			// Return true, meaning we'll go into the children.
			return true;

		}, (groupOrFile, depth) => {

			// Ignore the file.
			if (Items.isFile(groupOrFile)) {
				return;
			}

			// Don't do anything not in the list.
			const accountTotal = accountTotals[groupOrFile.id];
			if (!accountTotal) {
				return;
			}

			// Write the total HTML.
			let valueAtStart = NaN;
			let valueAtEnd = NaN;
			html += '<tr class="subtotal">';
			html += `<td class="account" style="padding-left: ${depth}rem">Total ${groupOrFile.name}</td>`;
			for (let i = 0, l = totalsPerPeriod[totalsPerPeriod.length - 1]!.length; i < l; i++) {
				const value = totalsPerPeriod[totalsPerPeriod.length - 1]![i]!;
				html += `<td>${displayAmount(value, reportUnit, false)}</td>`;
				if (isNaN(valueAtStart)) {
					valueAtStart = value;
				}
				valueAtEnd = value;
			}
			html += '</td>';

			// The total per account.
			if (periods.length > 1) {
				html += `<td class="change">${displayAmount(valueAtEnd - valueAtStart, reportUnit, false)}</td>`;
				const percentage = (valueAtEnd - valueAtStart) / valueAtStart;
				if (isFinite(percentage)) {
					html += `<td class="percentage">${(percentage * 100).toFixed(2)}</td>`;
				}
				else {
					html += `<td class="percentage"></td>`;
				}
			}
			html += '</tr>';

			// Accumulate the previous from this level, and pop off this list of totals.
			for (let periodI = 0; periodI < periods.length; periodI++) {
				totalsPerPeriod[totalsPerPeriod.length - 2]![periodI]! += totalsPerPeriod[totalsPerPeriod.length - 1]![periodI]!;
			}
			totalsPerPeriod.pop();
		});

		// The total per period.
		html += '<tr class="total"><td class="account">Total Balance</td>';
		for (let periodI = 0; periodI < periods.length; periodI++) {
			html += `<td>${displayAmount(totalsPerPeriod[0]![periodI]!, reportUnit, false)}</td>`;
		}
		if (periods.length > 1) {
			html += `<td class="change">${displayAmount(totalsPerPeriod[0]![totalsPerPeriod[0]!.length - 1]! - totalsPerPeriod[0]![0]!, reportUnit, false)}</td>`;
			const percentage = (totalsPerPeriod[0]![totalsPerPeriod[0]!.length - 1]! - totalsPerPeriod[0]![0]!) / totalsPerPeriod[0]![0]!;
			if (isFinite(percentage)) {
				html += `<td class="percentage">${(percentage * 100).toFixed(2)}</td>`;
			}
			else {
				html += `<td class="percentage"></td>`;
			}
		}
		html += '</tr>';

		html += '</table>';

		return html;
	}

	/** Increments a date by one period. */
	private getNextPeriod(date: YMD, period: PeriodInterval): YMD {
		const nextPeriod = new YMD(date);
		if (period === 'days') {
			nextPeriod.day += 1;
		}
		else if (period === 'weeks') {
			nextPeriod.day += 7;
		}
		else if (period === 'months') {
			nextPeriod.day = 1;
			nextPeriod.month += 1;
		}
		else if (period === 'quarters') {
			nextPeriod.day = 1;
			nextPeriod.month += 3;
		}
		else if (period === 'years') {
			nextPeriod.year += 1;
		}
		return nextPeriod;
	}

	/** Returns the beginning of the period given the date. */
	private getBeginningOfPeriod(date: YMD, period: PeriodInterval): YMD {
		const beginningOfPeriod = new YMD(date);
		if (period === 'weeks') {
			beginningOfPeriod.day -= 1 - date.dayOfWeek;
		}
		else if (period === 'months') {
			beginningOfPeriod.day = 1;
		}
		else if (period === 'quarters') {
			beginningOfPeriod.day = 1;
			beginningOfPeriod.month -= (date.month - 1) % 3;
		}
		else if (period === 'years') {
			beginningOfPeriod.day = 1;
			beginningOfPeriod.month = 1;
		}
		return beginningOfPeriod;
	}

	/** Returns the end of the period given the date. */
	private getEndingOfPeriod(date: YMD, period: PeriodInterval): YMD {
		const endingOfPeriod = new YMD(date);
		if (period === 'weeks') {
			endingOfPeriod.day += 7 - date.dayOfWeek;
		}
		else if (period === 'months') {
			endingOfPeriod.day = date.daysInMonth;
		}
		else if (period === 'quarters') {
			endingOfPeriod.day = 1;
			endingOfPeriod.month += 2 - ((date.month - 1) % 3);
			endingOfPeriod.day = date.daysInMonth;
		}
		else if (period === 'years') {
			endingOfPeriod.day = date.isLeapYear ? 366 : 365;
		}
		return endingOfPeriod;
	}

	/** The id of the report. */
	private _reportId!: string;

	protected static override html = html;
	protected static override css = css;
}

/** The possible period intervals. */
type PeriodInterval = 'days' | 'weeks' | 'months' | 'quarters' | 'years';
