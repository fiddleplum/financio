import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The report list page. */
export class ReportListPage extends ListPage {

	/** Constructor. */
	constructor(params: Params) {
		super('Report', 'reports', params);
	}
}
