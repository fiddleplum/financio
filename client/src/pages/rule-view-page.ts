import { Router } from '@orourley/elm-app';
import { FinancioApp, FinancioPage, TransactionList } from '../internal';
import { Rule } from '@orourley/financio-types';

import html from './rule-view-page.html';
import css from './rule-view-page.css';

/** The rule view page. */
export class RuleViewPage extends FinancioPage {

	/** Process the query. */
	override async processQuery(_oldQuery: Router.Query | undefined, query: Router.Query): Promise<void> {

		// Get the rule id.
		const ruleId = query['id'];
		if (ruleId === undefined) {
			this.setHtml('No rule specified.', this.root);
			return;
		}
		this._ruleId = ruleId;

		// Get the filter from the query.
		const transactionList = this.queryComponent('.TransactionList', TransactionList);
		transactionList.setFilterFromQuery(query);

		// Update the display.
		await this.updateDisplay();
	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the rule.
		const rule = await this.app.send<Rule>('rules', 'getItem', {
			projectId: this.app.projectId,
			id: this._ruleId
		});

		// Set the subtitle.
		this.app.setSubtitle(`Rule - ${rule.name}`);

		// Update the transaction list.
		await this.updateTransactionList(rule);
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<RuleEditPopup typeId="${this._ruleId}" edit onSaved="onSaved"></RuleEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<RuleDeletePopup typeId="${this._ruleId}" onDeleted="onDeleted"></RuleDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(): void {

		// Update the display.
		void this.updateDisplay();
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the list.
		this.app.setRouterQuery({
			projectId: this.app.projectId,
			page: 'ruleList'
		});
	}

	/** Applies the rule to whatever is in the transaction list. */
	async applyRule(): Promise<void> {

		// Get the rule.
		const rule = await this.app.send<Rule>('rules', 'getItem', {
			projectId: this.app.projectId,
			id: this._ruleId
		});

		// Get the transactions of the list.
		const transactions = this.queryComponent('.TransactionList', TransactionList).getTransactions();
		for (const transaction of transactions) {
			Rule.apply(rule, transaction);
		}

		// Send the update to the server.
		await (this.app as FinancioApp).send('transactions', 'update', {
			projectId: (this.app as FinancioApp).projectId,
			transactions: transactions.map((transaction) => ({ dateFromOld: transaction.dateFrom, transaction: transaction }))
		});

		// Update the transaction list.
		await this.updateTransactionList(rule);
	}

	/** Updates the transaction list. */
	private async updateTransactionList(rule: Rule): Promise<void> {

		// Update the transaction list forced filter.
		await this.queryComponent('.TransactionList', TransactionList).setForcedFilter({
			...(rule.reviewedMatch !== undefined && { reviewed: rule.reviewedMatch }),
			...(rule.amountMatch !== undefined && { minAmount: rule.amountMatch }),
			...(rule.amountMatch !== undefined && { maxAmount: rule.amountMatch }),
			...(rule.searchMatch !== undefined && { search: rule.searchMatch }),
			...(rule.onlyOnAccountId !== undefined && { accountIds: [rule.onlyOnAccountId] })
		});

		// Update the transaction list filter.
		await this.queryComponent('.TransactionList', TransactionList).setFilter({
			startDate: ',-1,years,',
			endDate: ',+1,years,'
		});
	}

	/** The id of the rule. */
	private _ruleId!: string;

	protected static override html = html;
	protected static override css = css;
}
