import { FormEasy, FormInput, FormValues } from '@orourley/elm-app';
import { FinancioPage, FormEntry } from '../internal';

import html from './user-settings-page.html';
import css from './user-settings-page.css';

export class UserSettingsPage extends FinancioPage {

	override async initialize(): Promise<void> {

		// Set the title.
		this.app.setSubtitle('User Settings');

		const displayName = await this.app.send<string>('users', 'getDisplayName', {
			username: this.app.username
		});
		const formEasy = this.queryComponent('.displayName .FormEasy', FormEasy);
		formEasy.getEntries().queryComponent('.FormEntry', FormEntry).getInput(FormInput).value = displayName;
	}

	protected async changeDisplayName(_form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const displayName = values['displayName'] as string;

		// Send the command.
		try {
			await this.app.send('users', 'changeDisplayName', {
				displayName
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		return 'The display name has been changed.';
	}

	protected async changePassword(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const oldPassword = values['oldPassword'] as string;
		const newPassword = values['newPassword'] as string;

		// Send the command.
		try {
			await this.app.send('users', 'changePassword', {
				oldPassword,
				newPassword
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		return 'The password has been changed.';
	}

	protected async deleteUser(form: FormEasy, values: FormValues): Promise<string> {

		// Get the inputs.
		const username = values['username'] as string;
		const password = values['password'] as string;
		const verify = values['verify'] as string;

		if (username !== this.app.username) {
			return 'You typed the wrong username.';
		}

		if (verify !== 'DELETE') {
			return 'Please enter DELETE to confirm.';
		}

		// Send the command.
		try {
			await this.app.send('users', 'deleteUser', {
				password
			});
		}
		catch (error) {
			return (error as Error).message;
		}

		// Clear the form.
		form.clear();

		await this.app.logout();

		return 'The user has been deleted.';
	}

	static override html = html;
	static override css = css;
}
