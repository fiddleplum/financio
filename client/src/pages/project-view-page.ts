import { Project } from '@orourley/financio-types';
import { FinancioPage } from '../internal';

import html from './project-view-page.html';
import css from './project-view-page.css';

/** The project dashboard page. */
export class ProjectViewPage extends FinancioPage {

	/** Process the query. */
	override async initialize(): Promise<void> {

		if (this.app.projectId === undefined) {
			this.setHtml('<p>Please choose a project in the projects list.</p>', this.root);
			return;
		}

		// Update the display.
		await this.updateDisplay().catch((error) => {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.root);
		});
	}

	/** Updates the display. */
	private async updateDisplay(): Promise<void> {

		// Get the project name and set the input.
		const project = await this.app.send<Project>('projects', 'getItem', {
			id: this.app.projectId
		});

		// Set the subtitle.
		this.app.setSubtitle(project.name);
	}

	/** Shows the edit popup. */
	openEditPopup(): void {
		this.app.openOverlay(`<ProjectEditPopup typeId="${this.app.projectId}" edit onSaved="onSaved"></ProjectEditPopup>`, this);
	}

	/** Shows the delete popup. */
	openDeletePopup(): void {
		this.app.openOverlay(`<ProjectDeletePopup typeId="${this.app.projectId}" onDeleted="onDeleted"></ProjectDeletePopup>`, this);
	}

	/** Called when this has been saved. */
	onSaved(): void {

		// Update the display.
		this.updateDisplay().catch((error) => {
			this.setHtml(`<p>${(error as Error).message}</p>`, this.root);
		});
	}

	/** Called when this has been deleted. */
	onDeleted(): void {

		// Go to the main page.
		this.app.setRouterQuery({});
	}

	protected static override html = html;
	protected static override css = css;
}
