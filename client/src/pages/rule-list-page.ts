import { Params } from '@orourley/elm-app';
import { ListPage } from './list-page';

/** The rule list page. */
export class RuleListPage extends ListPage {

	/** Constructor. */
	constructor(params: Params) {
		super('Rule', 'rules', params);
	}
}
