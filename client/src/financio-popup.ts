import { Popup } from '@orourley/elm-app';
import { FinancioApp } from './internal';

export abstract class FinancioPopup extends Popup {
	override get app(): FinancioApp {
		return super.parent as FinancioApp;
	}
}
