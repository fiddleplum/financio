import { Unit } from '@orourley/financio-types';

export function displayAmount(amount: number, unit: Unit | undefined, showUnit: boolean): string {
	if (unit) {
		return `${amount.toFixed(unit.decimals)}${showUnit ? ` ${unit.name}` : ``}`;
	}
	else {
		return `${amount}`;
	}
}
