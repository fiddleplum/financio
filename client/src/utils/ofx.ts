import { Transaction } from '@orourley/financio-types';

/** An OFX parsed into a JS object. */
type OFX = (string | [string, string | OFX])[];

export namespace OFX {

	/** Parses an OFX into transactions. */
	export function parse(text: string, accountId: string): Transaction[] {

		/** The transactions that we'll return. */
		const transactions: Transaction[] = [];

		// Get the XML.
		const ofx = convertToValidJsObject(text);

		// Pop off the headers.
		ofx.shift();

		// Drill down into the transactions.
		const OFX = getValueObject('OFX', ofx);

		// Bank messages.
		if (hasKey('BANKMSGSRSV1', OFX) || hasKey('CREDITCARDMSGSRSV1', OFX)) {

			// Get the base nodes.
			let BANKTRANLIST;
			if (hasKey('BANKMSGSRSV1', OFX)) {
				const BANKMSGSRSV1 = getValueObject('BANKMSGSRSV1', OFX);
				const STMTTRNRS = getValueObject('STMTTRNRS', BANKMSGSRSV1);
				const STMTRS = getValueObject('STMTRS', STMTTRNRS);
				BANKTRANLIST = getValueObject('BANKTRANLIST', STMTRS);
			}
			else {
				const CREDITCARDMSGSRSV1 = getValueObject('CREDITCARDMSGSRSV1', OFX);
				const CCSTMTTRNRS = getValueObject('CCSTMTTRNRS', CREDITCARDMSGSRSV1);
				const CCSTMTRS = getValueObject('CCSTMTRS', CCSTMTTRNRS);
				BANKTRANLIST = getValueObject('BANKTRANLIST', CCSTMTRS);
			}

			// STMTTRN
			const STMTTRNs = getValueObjectArray('STMTTRN', BANKTRANLIST);
			for (const STMTTRN of STMTTRNs) {

				// Get the tag values.
				const DTPOSTED = getValueString('DTPOSTED', STMTTRN);
				const TRNAMT = getValueString('TRNAMT', STMTTRN);
				const NAME = hasKey('NAME', STMTTRN) ? getValueString('NAME', STMTTRN) : '';
				const MEMO = hasKey('MEMO', STMTTRN) ? getValueString('MEMO', STMTTRN) : '';
				const FITID = hasKey('FITID', STMTTRN) ? getValueString('FITID', STMTTRN) : '';

				// Convert the tag values to transaction values.
				const date = `${DTPOSTED.substring(0, 4)}-${DTPOSTED.substring(4, 6)}-${DTPOSTED.substring(6, 8)}`;
				const amount = Number.parseFloat(TRNAMT);

				// Make the transaction and push it.
				const transaction: Transaction = {
					id: '',
					accountIdFrom: amount >= 0 ? '' : accountId,
					accountIdTo: amount < 0 ? '' : accountId,
					dateFrom: date,
					dateTo: date,
					amountFrom: Math.abs(amount),
					amountTo: Math.abs(amount),
					description: `${NAME} ${MEMO}`,
					institutionIdFrom: amount >= 0 ? '' : FITID,
					institutionIdTo: amount < 0 ? '' : FITID,
					categoryId: '',
					reviewed: false,
					note: ''
				};
				transactions.push(transaction);
			}
		}

		// Investment Statement Messages.
		if (hasKey('INVSTMTMSGSRSV1', OFX)) {

			// Get the security id to name mapping.
			const securityIdsToNames: Record<string, string> = {};
			if (hasKey('SECLISTMSGSRSV1', OFX)) {
				const SECLISTMSGSRSV1 = getValueObject('SECLISTMSGSRSV1', OFX);
				const SECLIST = getValueObject('SECLIST', SECLISTMSGSRSV1);
				const MFINFOs = getValueObjectArray('MFINFO', SECLIST);
				for (const MGINFO of MFINFOs) {
					const SECINFO = getValueObject('SECINFO', MGINFO);
					const SECID = getValueObject('SECID', SECINFO);
					const UNIQUEID = getValueString('UNIQUEID', SECID);
					const SECNAME = getValueString('SECNAME', SECINFO);
					securityIdsToNames[UNIQUEID] = SECNAME;
				}
				const STOCKINFOs = getValueObjectArray('STOCKINFO', SECLIST);
				for (const STOCKINFO of STOCKINFOs) {
					const SECINFO = getValueObject('SECINFO', STOCKINFO);
					const SECID = getValueObject('SECID', SECINFO);
					const UNIQUEID = getValueString('UNIQUEID', SECID);
					const SECNAME = getValueString('SECNAME', SECINFO);
					securityIdsToNames[UNIQUEID] = SECNAME;
				}
			}

			// Get the base nodes.
			const INVSTMTMSGSRSV1 = getValueObject('INVSTMTMSGSRSV1', OFX);
			const INVSTMTTRNRS = getValueObject('INVSTMTTRNRS', INVSTMTMSGSRSV1);
			const INVSTMTRS = getValueObject('INVSTMTRS', INVSTMTTRNRS);
			const INVTRANLIST = getValueObject('INVTRANLIST', INVSTMTRS);

			// REINVEST
			const REINVESTs = getValueObjectArray('REINVEST', INVTRANLIST);
			for (const REINVEST of REINVESTs) {

				// Get the tag values.
				const INVTRAN = getValueObject('INVTRAN', REINVEST);
				const DTTRADE = getValueString('DTTRADE', INVTRAN);
				const FITID = getValueString('FITID', INVTRAN);
				const TOTAL = getValueString('TOTAL', REINVEST);
				const UNITS = getValueString('UNITS', REINVEST);
				const SECID = getValueObject('SECID', REINVEST);
				const UNIQUEID = getValueString('UNIQUEID', SECID);

				const date = `${DTTRADE.substring(0, 4)}-${DTTRADE.substring(4, 6)}-${DTTRADE.substring(6, 8)}`;
				const amount = Number.parseFloat(TOTAL);
				const amount2 = Number.parseFloat(UNITS);
				const securityName = securityIdsToNames[UNIQUEID];
				if (securityName === undefined) {
					throw new Error(`Malformed OFX: Security with id ${UNIQUEID} does not have a corresponding name.`);
				}

				// Make the transaction and push it.
				const transaction: Transaction = {
					id: '',
					accountIdFrom: accountId,
					accountIdTo: '',
					dateFrom: date,
					dateTo: date,
					amountFrom: amount,
					amountTo: amount2,
					description: `REINVEST INTO ${securityName}`,
					institutionIdFrom: FITID,
					institutionIdTo: '',
					categoryId: '',
					reviewed: false,
					note: ''
				};
				transactions.push(transaction);
			}

			// BUYMF
			const BUYMFs = getValueObjectArray('BUYMF', INVTRANLIST);
			for (const BUYMF of BUYMFs) {

				// Get the tag values.
				const INVBUY = getValueObject('INVBUY', BUYMF);
				const INVTRAN = getValueObject('INVTRAN', INVBUY);
				const DTTRADE = getValueString('DTTRADE', INVTRAN);
				const FITID = getValueString('FITID', INVTRAN);
				const TOTAL = getValueString('TOTAL', INVBUY);
				const UNITS = getValueString('UNITS', INVBUY);
				const SECID = getValueObject('SECID', INVBUY);
				const UNIQUEID = getValueString('UNIQUEID', SECID);

				// Convert the tag values to transaction values.
				const date = `${DTTRADE.substring(0, 4)}-${DTTRADE.substring(4, 6)}-${DTTRADE.substring(6, 8)}`;
				const amount = Number.parseFloat(TOTAL);
				const amount2 = Number.parseFloat(UNITS);
				const securityName = securityIdsToNames[UNIQUEID];
				if (securityName === undefined) {
					throw new Error(`Malformed OFX: Security with id ${UNIQUEID} does not have a corresponding name.`);
				}

				// Make the transaction and push it.
				const transaction: Transaction = {
					id: '',
					accountIdFrom: accountId,
					accountIdTo: '',
					dateFrom: date,
					dateTo: date,
					amountFrom: -amount,
					amountTo: amount2,
					description: `BUYMF ${securityName}`,
					institutionIdFrom: FITID,
					institutionIdTo: '',
					categoryId: '',
					reviewed: false,
					note: ''
				};
				transactions.push(transaction);
			}

			// SELLMF
			const SELLMFs = getValueObjectArray('SELLMF', INVTRANLIST);
			for (const SELLMF of SELLMFs) {

				// Get the tag values.
				const INVSELL = getValueObject('INVSELL', SELLMF);
				const INVTRAN = getValueObject('INVTRAN', INVSELL);
				const DTTRADE = getValueString('DTTRADE', INVTRAN);
				const FITID = getValueString('FITID', INVTRAN);
				const TOTAL = getValueString('TOTAL', INVSELL);
				const UNITS = getValueString('UNITS', INVSELL);
				const SECID = getValueObject('SECID', INVSELL);
				const UNIQUEID = getValueString('UNIQUEID', SECID);

				// Convert the tag values to transaction values.
				const date = `${DTTRADE.substring(0, 4)}-${DTTRADE.substring(4, 6)}-${DTTRADE.substring(6, 8)}`;
				const amount = Number.parseFloat(TOTAL);
				const amount2 = Number.parseFloat(UNITS);
				const securityName = securityIdsToNames[UNIQUEID];
				if (securityName === undefined) {
					throw new Error(`Malformed OFX: Security with id ${UNIQUEID} does not have a corresponding name.`);
				}

				// Make the transaction and push it.
				const transaction: Transaction = {
					id: '',
					accountIdFrom: '',
					accountIdTo: accountId,
					dateFrom: date,
					dateTo: date,
					amountFrom: -amount2,
					amountTo: amount,
					description: `SELLMF ${securityName}`,
					institutionIdFrom: '',
					institutionIdTo: FITID,
					categoryId: '',
					reviewed: false,
					note: ''
				};
				transactions.push(transaction);
			}
		}

		return transactions;
	}
}

/** Returns true if the ofx has the key. */
function hasKey(key: string, ofx: OFX): boolean {
	return ofx.find((value) => typeof value !== 'string' && value[0] === key) !== undefined;
}

/** Gets the value of the key in the ofx as a string, including verifying type. */
function getValueString(key: string, ofx: OFX): string {
	const value = ofx.find((value) => typeof value !== 'string' && value[0] === key)?.[1];
	if (typeof value !== 'string') {
		throw new Error(`Malformed OFX: The key ${key} should be a string.`);
	}
	return value;
}

/** Gets the value of the keyin the ofx as an object, including verifying type. */
function getValueObject(key: string, ofx: OFX): OFX {
	const value = ofx.find((value) => typeof value !== 'string' && value[0] === key)?.[1];
	if (typeof value !== 'object') {
		throw new Error(`Malformed OFX: The key ${key} should be an object.`);
	}
	return value;
}

/** Gets the value of the key in the ofx as an object array, including verifying type. */
function getValueObjectArray(key: string, ofx: OFX): OFX[] {
	return ofx
		.filter((value) => typeof value !== 'string' && value[0] === key)
		.map((value, i) => {
			if (typeof (value as [string, string | OFX])[1] !== 'object') {
				throw new Error(`Malformed OFX: The key ${key}[${i}] should be an object.`);
			}
			return (value as [string, OFX])[1];
		});
}

/** Turns the OFX, which is SGML, to XML. */
function convertToValidJsObject(text: string): OFX {

	// The nodes to be returned.
	const nodes: OFX = [];

	let index = 0;
	while (true) {

		// Get any content up to the first tag (or the rest).
		const startOfTag = text.indexOf('<', index);
		if (startOfTag === -1) {
			if (index < text.length) {
				nodes.push(text.substring(index));
			}
			break;
		}
		const innerText = text.substring(index, startOfTag).trim();
		if (innerText.length > 0) {
			nodes.push(innerText);
		}

		// Get the tag name.
		const endOfTag = text.indexOf('>', startOfTag);
		if (endOfTag === -1) {
			throw new Error('Malformed OFX, with < not having a matching >.');
		}
		const tagName = text.substring(startOfTag + 1, endOfTag).trim();

		// Find a closing tag and get the inner content. If there's no closing tag, get content until the next tag.
		const startOfContent = endOfTag + 1;
		let endOfContent = text.indexOf(`</${tagName}>`, startOfContent);
		const foundClosingTag = endOfContent !== -1;
		if (!foundClosingTag) {

			// No closing tag, get content until next tag start.
			endOfContent = text.indexOf('<', startOfContent);
			if (endOfContent === -1) {
				endOfContent = text.length;
			}
			nodes.push([tagName, text.substring(startOfContent, endOfContent).trim()]);
			index = endOfContent;
		}
		else {

			// Found closing tag. Recursively parse children.
			const children = convertToValidJsObject(text.substring(startOfContent, endOfContent).trim());
			if (children.length > 0) {
				if (children.length > 1 || typeof children[0] === 'object') {
					nodes.push([tagName, children]);
				}
				else {
					nodes.push([tagName, children[0]!]);
				}
			}
			index = endOfContent + `</${tagName}>`.length;
		}
	}

	return nodes;
}
