import { SimpleApp } from '@orourley/elm-app';
import { FinancioApp } from './internal';

export abstract class FinancioPage extends SimpleApp.Page {
	override get app(): FinancioApp {
		return super.parent as FinancioApp;
	}
}
