import js from '@eslint/js';
import stylistic from '@stylistic/eslint-plugin';
import stylisticTs from '@stylistic/eslint-plugin-ts';
import ts from 'typescript-eslint';
import tsParser from '@typescript-eslint/parser';
import tsPlugin from '@typescript-eslint/eslint-plugin';

export default [

	// Parse these files in addition to the default .js files.
	{
		files: ['src/**/*.ts', 'eslint.config.mjs']
	},

	// Don't parse these files.
	{
		ignores: ['docs/**/*', 'dist/**/*']
	},

	// Start with the ALL of the ESLint settings. We'll want most of them and we can turn off what we don't want.
	js.configs.all,

	// ESLint Styles, which were separated out from ESLint proper.
	// We start with the recommended and then add our own.
	stylistic.configs.customize({
		arrowParens: true,
		indent: 'tab',
		jsx: false,
		semi: true,
		commaDangle: 'never'
	}),
	{
		plugins: {
			'@stylistic': stylistic,
			'@stylistic/ts': stylisticTs
		},
		rules: {
			// We sometimes like a blank line when starting a new block for clarity.
			'@stylistic/padded-blocks': 'off',
			// This makes the second line of binary ops be on the same indent as the first line, which is weird.
			'@stylistic/indent-binary-ops': 'off',
			// If we are using semicolons, we need to enforce them to be at the end of every member.
			'@stylistic/member-delimiter-style': 'off',
			'@stylistic/ts/member-delimiter-style': ['error', { multiline: { delimiter: 'semi', requireLast: true }, singleline: { delimiter: 'semi', requireLast: true } }]
		}
	},

	// ESLint
	{
		rules: {
			// Enforce capitalized first letter in comments, but the second line or inline comments are okay.
			'capitalized-comments': ['error', 'always', { ignoreConsecutiveComments: true, ignoreInlineComments: true }],
			// This gives false positives for methods that are meant to be overridden and are empty.
			'class-methods-use-this': 'off',
			// Don't limit the complexity of functions.
			'complexity': 'off',
			// Sometimes we want to assign a variable to this, for example when walking a tree or graph.
			'consistent-this': 'off',
			// Many times we don't need a default case, since we have already have a default or have an exhaustive list.
			'default-case': 'off',
			// This is better supported in TS ESLint.
			'default-param-last': 'off',
			// We turn this off because TS ESLint dot-notation can handle cases where the type is Record<string, any>.
			'dot-notation': 'off',
			// Enforce no function expressions.
			'func-style': ['error', 'declaration'],
			// We need short variables for iterators and math.
			'id-length': 'off',
			// We don't always want to initialize a declared variable if it has conditional initialization.
			'init-declarations': 'off',
			// Don't limit the number of classes per file.
			'max-classes-per-file': 'off',
			// Don't limit the maximum depth of statements.
			'max-depth': 'off',
			// Don't limit the number of max lines per file.
			'max-lines': 'off',
			// Don't limit the number of lines per function.
			'max-lines-per-function': 'off',
			// Dont' limit the number of params of a function.
			'max-params': 'off',
			// Don't limit the number of statements per function.
			'max-statements': 'off',
			// We sometimes need awaits in loops, if we are doing serial asynchronity.
			'no-await-in-loop': 'off',
			// We like continue statements.
			'no-continue': 'off',
			// We sometimes like else statements after the last return to make things look more symmetrical.
			'no-else-return': 'off',
			// Overridden type TS ESLint.
			'no-empty-function': 'off',
			// Sometimes we just like inline comments.
			'no-inline-comments': 'off',
			// This causes problems with things like `1 * x`, even though x is already a number.
			// There isn't a TS ESLint rule to take into account type, so we're just turning this off.
			'no-implicit-coercion': 'off',
			// Don't allow function declarations anywhere but the top level.
			'no-inner-declarations': ['error', 'both', { blockScopedFunctions: 'disallow' }],
			// We sometimes like to have an if as the first statment inside an else to make things clearer.
			'no-lonely-if': 'off',
			// We like magic numbers everywhere! Make sure not to hard-code too many things in.
			'no-magic-numbers': 'off',
			// We like doing `something !== undefined`, but this gives a false positive.
			'no-negated-condition': 'off',
			// Sometimes we like to nest simple ternary operations. But we still have to be careful that it doesn't get too complicated.
			'no-nested-ternary': 'off',
			// Sometimes reassigning the params is good when we want defaults or for sanitizing.
			'no-param-reassign': 'off',
			// We like using i++.
			'no-plusplus': 'off',
			// This is handled by Typescript.
			'no-redeclare': 'off',
			// This can cause issues with code like: `const entity = ...; setUpdateFunction((entity) => { ... });`.
			'no-shadow': 'off',
			// We like ternary operations when appopriate.
			'no-ternary': 'off',
			// Typescript takes care of this one.
			'no-undef': 'off',
			// We like using undefined sometimes.
			'no-undefined': 'off',
			// We like prefixing private variables and unused params with underscores.
			'no-underscore-dangle': 'off',
			// This gives false positives if the properties of an object are changed but not the object.
			'no-unmodified-loop-condition': 'off',
			// Turning this off in favor of the TS ESLint version.
			'no-unused-vars': 'off',
			// Typescript takes care of this one.
			'no-use-before-define': 'off',
			// To comply with TS ESLint no-floating-promises, we need to use void in promise statements.
			'no-void': ['error', { allowAsStatement: true }],
			// We prefer that within an object all properties are either short-hand or long-hand, prefering shorthand when possible.
			'object-shorthand': 'off',
			// Enforce separate declarations for each variable.
			'one-var': ['error', 'never'],
			// This one enforces destructuring, which can be a bit confusing.
			'prefer-destructuring': ['error', { array: false }],
			// We're fine not using named capture groups in our regexes (we'll just use $1, $2, etc).
			'prefer-named-capture-group': 'off',
			// Don't enforce a radix for parseInt.
			'radix': 'off',
			// This gives false positives on property access, so we're adjusting it.
			'require-atomic-updates': ['error', { allowProperties: true }],
			// Sometimes we use async to denote a return of a promise and have no awaits.
			'require-await': 'off',
			// We sometimes like to sort our imports in a more logical way.
			'sort-imports': ['off'],
			// We sometimes like to sort our keys in a more logical way.
			'sort-keys': 'off',
			// We don't allow multiple variables in the same declaration, except in for loops, and they may have a non-sorted order.
			'sort-vars': 'off',
			// We sometimes like things like 0 <= index && index < 10.
			'yoda': ['error', 'never', { exceptRange: true }]
		}
	},

	// TS ESLint
	// To get typscript-eslint working, we need to specify the TypeScript parser config.
	{
		ignores: ['eslint.config.mjs'],
		files: ['src/**/*.ts'],
		languageOptions: {
			parser: tsParser,
			parserOptions: {
				ecmaFeatures: { modules: true },
				ecmaVersion: 'latest',
				project: './tsconfig.json'
			}
		},
		// This adds the plugin so we can use the TS ESLint rules.
		plugins: {
			'@typescript-eslint': tsPlugin
		},
		rules: {
			// Check add the recommended strict TS ESLint settings.
			...ts.configs.strictTypeChecked.rules,
			...ts.configs.stylisticTypeChecked.rules,

			// Although this is a good rule, it only works on TS files.
			'@typescript-eslint/class-literal-property-style': 'off',
			// Make sure default parameters are after any regular params.
			'@typescript-eslint/default-param-last': 'error',
			// This one is used because the ESLint version doesn't handle Record<string, any> type objects.
			'@typescript-eslint/dot-notation': 'error',
			// We often have custom toString methods that this would give false positives on.
			'@typescript-eslint/no-base-to-string': 'off',
			// We sometimes want to delete dynamic keys. This rule is overly strict.
			'@typescript-eslint/no-dynamic-delete': 'off',
			// We're okay with empty functions.
			'@typescript-eslint/no-empty-function': 'off',
			// Sometimes we like classes with all static functions to encapsulate them.
			'@typescript-eslint/no-extraneous-class': 'off',
			// Sometimes when walking trees or graphs we need to assign a variable to `this`.
			'@typescript-eslint/no-this-alias': 'off',
			// Normally we'd want this, but it causes issues with Record<string, type> access,
			// which returns Type, but not (Type | undefined) because we aren't enabling TypeScript's noUncheckedIndexedAccess.
			// We still want to check for undefined, but this causes a false positive because Type can never be undefined.
			'@typescript-eslint/no-unnecessary-condition': 'off',
			// When dealing with libraries that aren't perfectly typed, we get anys and it's okay.
			'@typescript-eslint/no-unsafe-assignment': 'off',
			// Underscore prefixes and things that start with a capital letter so that
			// imported classes that are only used in JSDoc work.
			'@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '^([A-Z]|_)', varsIgnorePattern: '^([A-Z]|_)' }],
			// We prefer `for(let i = 0...)` loops because they are faster and reduce garbage generation.
			'@typescript-eslint/prefer-for-of': 'off',
			// This works great except when we have `if (!object: {} | null || object.text: string | null === null)`, which would turn into `if (object?.text === null)`.
			// And that fails because expression changed from string | null to string | undefined | null, which isn't good.
			// And the checkString option doesn't help.
			'@typescript-eslint/prefer-optional-chain': 'off',
			// We like to use async functions, but they don't always use await explicitly, as
			// some just return a promise implicitly, which is okay.
			'@typescript-eslint/require-await': 'off',
			// We're okay automatically turning non-strings to strings in string templates.
			'@typescript-eslint/restrict-template-expressions': 'off',
			// Make sure we don't have things like `if (string | undefined)`, which can cause subtle errors.
			'@typescript-eslint/strict-boolean-expressions': ['error', { allowString: false, allowNumber: false }],
			// We often bind this to member functions that are callbacks, such as `this.foo = this.foo.bind(this)`,
			// so this gives a false positive.
			'@typescript-eslint/unbound-method': 'off',
			// In `catch (error)`, the error is `any`, but we can't easily make it `unknown`, since we're in JS not TS.
			'@typescript-eslint/use-unknown-in-catch-callback-variable': 'off'
		}
	}
];
