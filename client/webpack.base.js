const CopyWebpackPlugin = require('copy-webpack-plugin');
const TSConfigPathsWebpackPlugin = require('tsconfig-paths-webpack-plugin');
const path = require('path');

module.exports = {
	entry: './src/index.ts',
	output: {
		filename: 'script.js',
		clean: true
	},
	stats: 'minimal',
	resolve: {
		extensions: ['.ts', '.js']
	},
	module: {
		rules: [{
			test: /\.ts$/u,
			loader: 'ts-loader'
		}, {
			test: /\.(css|svg|html)$/u,
			use: 'raw-loader'
		}]
	},
	devServer: {
		hot: false
	},
	plugins: [
		new CopyWebpackPlugin({
			patterns: [{
				from: 'src/index.html'
			}, {
				from: 'src/config.json',
				noErrorOnMissing: false
			}, {
				from: 'src/manifest.json',
				noErrorOnMissing: true
			}, {
				from: 'src/assets',
				to: 'assets',
				noErrorOnMissing: true
			}]
		})
	]
};
