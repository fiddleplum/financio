import { Items } from './items';

/** The member roles. */
export enum Role {

	/** Can only view a dash board with accounts and categories. */
	DashBoard = 1,

	/** Can view anything, but not modify anything. */
	Viewer = 2,

	/** Can only modify transactions and rules. */
	Bookkeeper = 3,

	/** Can modify accounts and categories. */
	Accountant = 4,

	/** Can modify the project. */
	Owner = 5
}

/** A single project information. */
export type Project = Items.Item & {

	/** The default unit id when creating a new account. */
	defaultUnitId: string;

	/** The roles for each user in the project. */
	roles: Record<string, Role>;
};

/** A group of projects. */
export type ProjectGroup = Items.Group;

/** A list of project ids and member roles. */
export type ProjectFile = Items.File<Project, ProjectGroup>;
