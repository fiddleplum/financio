import { Sort } from '@orourley/pine-lib';
import { Items } from './items';

/** The conversion rates. */
export type ConversionRates = Record<string, [string, number][]>;

/** A unit. */
export type Unit = Items.Item & {

	/** The number of decimals to be displayed. */
	decimals: number;

	/** Conversion rates with other units, mapping from other unit id to tuple [date, X], where 1 of this unit equals X of the other unit. */
	conversionRates: ConversionRates;
};

export namespace Unit {

	/** Gets the rate at the date. Returns undefined if there are no rates at all. */
	export function getConversionRateAtDate(unitId: string, otherUnitId: string, date: string, conversionRates: ConversionRates): number | undefined {

		if (unitId === otherUnitId) {
			return 1;
		}

		const rates = conversionRates[otherUnitId];
		if (!rates) {
			return undefined;
		}

		const index = Sort.getIndex(date, rates, (a, b) => a[0] < b);
		if (index === rates.length) {
			return rates[index - 1]?.[1];
		}
		const rate = rates[index]!;
		if (index === 0 || rate[0] === date) {
			return rate[1];
		}

		// Interpolate between the dates.
		const prevRate = rates[index - 1]!;
		const d0 = new Date(prevRate[0]).getTime();
		const d1 = new Date(rate[0]).getTime();
		const dU = new Date(date).getTime();
		const u = (dU - d0) / (d1 - d0);
		return prevRate[1] * (1 - u) + u * rate[1];
	}
}

export type UnitGroup = Items.Group;
export type UnitFile = Items.File<Unit, UnitGroup>;
