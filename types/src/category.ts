import { Items } from './items';

/** A category. */
export type Category = Items.Item;

export type CategoryGroup = Items.Group;
export type CategoryFile = Items.File<Category, CategoryGroup>;
