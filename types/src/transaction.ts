import { JsonHelper, JsonType } from '@orourley/pine-lib';

export type Transaction = {

	/** The unique generated id. */
	id: string;

	/** The account id the transaction is going from. */
	accountIdFrom: string;

	/** The account id the transaction is going to. */
	accountIdTo: string;

	/** The date when the accountFrom transaction was processed, formatted as 'yyyy-mm-dd hh:mm:ss.sss'. */
	dateFrom: string;

	/** The date when the accountTo transaction was processed, formatted as 'yyyy-mm-dd hh:mm:ss.sss'. */
	dateTo: string;

	/** The amount in the unit of the accountFrom. It will always be positive. */
	amountFrom: number;

	/** The amount in the unit of the accountTo. It will always be positive. */
	amountTo: number;

	/** The description from the institution. */
	description: string;

	/** The unique id from the from institution. */
	institutionIdFrom: string;

	/** The unique id from the to institution. */
	institutionIdTo: string;

	/** The category id to which the transaction belongs. */
	categoryId: string;

	/** Whether or not the transaction has been reviewed. */
	reviewed: boolean;

	/** Additional note. */
	note: string;
};

/** Checks if the JSON is a valid transaction. */
export function isTransaction(json: JsonType | undefined): json is Transaction {
	if (!JsonHelper.isObject(json)
		|| !JsonHelper.isString(json['id'])
		|| !JsonHelper.isString(json['accountIdFrom'])
		|| !JsonHelper.isString(json['accountIdTo'])
		|| !JsonHelper.isString(json['dateFrom'])
		|| !JsonHelper.isString(json['dateTo'])
		|| !JsonHelper.isNumber(json['amountFrom'])
		|| !JsonHelper.isNumber(json['amountTo'])
		|| !JsonHelper.isString(json['description'])
		|| !JsonHelper.isString(json['institutionIdFrom'])
		|| !JsonHelper.isString(json['institutionIdTo'])
		|| !JsonHelper.isString(json['categoryId'])
		|| !JsonHelper.isBoolean(json['reviewed'])
		|| !JsonHelper.isString(json['note'])) {
		return false;
	}
	return true;
}
