import { Sort } from '@orourley/pine-lib';
import { Items } from './items';

/** A report. */
export type Account = Items.Item & {

	/** The unit id. */
	unitId: string;

	/** Whether or not the account is internal to the project or external (customer, donor, vendor).
	 *  For internal accounts we have transactions and values, but not external accounts. */
	internal: boolean;

	/** A value of the account at the start of a month in the form YYYY-MM. All transactions before and after
	 *  will be used to calculate the valuesAtMonthStart with this as an anchor point. */
	anchorPoint: { month: string; value: number; };

	/** The values of the account at the start of the month. Date is of the form 'YYYY-MM'.
	 *  There will always be at least one entry, the anchor month & value.
	 *  There will be one entry for each month after the anchor month that has transactions in the prior month,
	 *  and one entry for each month before the anchor month that has transactions in that month. */
	valuesAtMonthStart: { month: string; value: number; count: number; }[];

	/** The defaults for importing OFX files. */
	ofxImportDefaults?: {
		reverseDirection: boolean;
	};

	/** The defaults for importing CSV files. */
	csvImportDefaults?: {
		delim: string;
		trimFirstLines: number;
		trimLastLines: number;
		hasHeaders: boolean;
		dateFormat: string;
		headersToCols: Record<string, number>;
	};
};

export type AccountGroup = Items.Group;
export type AccountFile = Items.File<Account, AccountGroup>;

export namespace Account {

	/** Gets the value at month start, given the date in YYYY-MM form. Will return undefined if there are no valuesAtMonthStart. */
	export function getValueAtMonthStart(date: string, accountOrGroup: Account | AccountGroup, accountFile?: AccountFile, onlyIncludeAccountIds?: Set<string>): number {

		// Ignore anything that we should exclude.
		if (onlyIncludeAccountIds && !onlyIncludeAccountIds.has(accountOrGroup.id)) {
			return 0;
		}

		if (Items.isGroup(accountOrGroup)) {

			if (!accountFile) {
				throw new Error('Group given, but not accountFile given.');
			}

			// Total up all of the children.
			let valueAtMonthStart = 0;
			for (const childId of accountOrGroup.childIds) {

				const child = accountFile.list[childId];
				if (child) {
					valueAtMonthStart += getValueAtMonthStart(date, child, accountFile, onlyIncludeAccountIds);
				}
			}

			// Return the total.
			return valueAtMonthStart;
		}
		else {

			// If there no values at month start, just return the anchor value.
			if (accountOrGroup.valuesAtMonthStart.length === 0) {
				return accountOrGroup.anchorPoint.value;
			}

			// Get the index.
			const index = Sort.getIndex(date, accountOrGroup.valuesAtMonthStart, (lhs, rhs) => lhs.month < rhs);
			const valuesAtIndex = accountOrGroup.valuesAtMonthStart[index];
			const valuesAtPrevIndex = accountOrGroup.valuesAtMonthStart[index - 1];

			// If it is the length (undefined), then the date is after the last entry, so return the last entry.
			if (!valuesAtIndex) {
				if (valuesAtPrevIndex!.month < accountOrGroup.anchorPoint.month) {
					return accountOrGroup.anchorPoint.value;
				}
				else {
					return valuesAtPrevIndex!.value;
				}
			}
			// Check for an exact match.
			else if (date === valuesAtIndex.month) {
				return valuesAtIndex.value;
			}
			// Otherwise, return the given entry. If it was before the first entry, it will get the first entry.
			else if (index === 0) {
				if (valuesAtIndex.month > accountOrGroup.anchorPoint.month) {
					return accountOrGroup.anchorPoint.value;
				}
				else {
					return valuesAtIndex.value;
				}
			}
			else {
				if (valuesAtPrevIndex!.month < accountOrGroup.anchorPoint.month && accountOrGroup.anchorPoint.month < valuesAtIndex.month) {
					return accountOrGroup.anchorPoint.value;
				}
				else if (valuesAtIndex.month < accountOrGroup.anchorPoint.month) {
					return valuesAtIndex.value;
				}
				else {
					return valuesAtPrevIndex!.value;
				}
			}
		}
	}
}
